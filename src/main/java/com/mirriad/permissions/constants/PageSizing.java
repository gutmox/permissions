package com.mirriad.permissions.constants;

public interface PageSizing {

	Integer MAX_PAGE_SIZE = 100;
	Integer FIRST_PAGE = 0;
}
