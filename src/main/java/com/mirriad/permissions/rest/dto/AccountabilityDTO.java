package com.mirriad.permissions.rest.dto;

import javax.xml.bind.annotation.XmlRootElement;

import com.sun.istack.NotNull;


@XmlRootElement(name="relates")
public class AccountabilityDTO {

	@NotNull
	private RelatedPartyDTO childParty;

	@NotNull
	private RelatedPartyDTO parentParty;

	@NotNull
	private String accountabilityType;

	public String getAccountabilityType() {
		return accountabilityType;
	}

	public void setAccountabilityType(String accountabilityTypeId) {
		this.accountabilityType = accountabilityTypeId;
	}

	public RelatedPartyDTO getChildParty() {
		return childParty;
	}

	public void setChildParty(RelatedPartyDTO childParty) {
		this.childParty = childParty;
	}

	public RelatedPartyDTO getParentParty() {
		return parentParty;
	}

	public void setParentParty(RelatedPartyDTO parentParty) {
		this.parentParty = parentParty;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((accountabilityType == null) ? 0 : accountabilityType.hashCode());
		result = prime * result + ((childParty == null) ? 0 : childParty.hashCode());
		result = prime * result + ((parentParty == null) ? 0 : parentParty.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountabilityDTO other = (AccountabilityDTO) obj;
		if (accountabilityType == null) {
			if (other.accountabilityType != null)
				return false;
		} else if (!accountabilityType.equals(other.accountabilityType))
			return false;
		if (childParty == null) {
			if (other.childParty != null)
				return false;
		} else if (!childParty.equals(other.childParty))
			return false;
		if (parentParty == null) {
			if (other.parentParty != null)
				return false;
		} else if (!parentParty.equals(other.parentParty))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Accountability [childParty=" + childParty + ", parentParty=" + parentParty
				+ ", accountabilityType=" + accountabilityType + "]";
	}
}
