package com.mirriad.permissions.rest.dto;

import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="entity")
public class EntityDTO {

	private String uuid;

	private String name;

	private Collection<EntireEntityDTO> parents;

	private Collection<EntireEntityDTO> children;

	public EntityDTO(){
		super();
	}
	public EntityDTO(String uuid, String name) {
		this();
		this.uuid = uuid;
		this.name = name;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<EntireEntityDTO> getParents() {
		return parents;
	}
	public void setParents(Collection<EntireEntityDTO> parents) {
		this.parents = parents;
	}
	public Collection<EntireEntityDTO> getChildren() {
		return children;
	}
	public void setChildren(Collection<EntireEntityDTO> children) {
		this.children = children;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntityDTO other = (EntityDTO) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Entity [uuid=" + uuid + ", name=" + name + ", parents=" + parents
				+ ", children=" + children + "]";
	}
}
