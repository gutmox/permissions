package com.mirriad.permissions.rest.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="entity")
public class SimpleEntityDTO {

	private String uuid;

	private String name;


	public SimpleEntityDTO(){
		super();
	}
	public SimpleEntityDTO(String uuid, String name) {
		this();
		this.uuid = uuid;
		this.name = name;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleEntityDTO other = (SimpleEntityDTO) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Entity [uuid=" + uuid + ", name=" + name + "]";
	}
}
