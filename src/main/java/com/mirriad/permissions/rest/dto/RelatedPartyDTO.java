package com.mirriad.permissions.rest.dto;

import javax.xml.bind.annotation.XmlRootElement;

import com.mirriad.permissions.domain.PartyType;

@XmlRootElement(name="party")
public class RelatedPartyDTO extends PartyDTO{

	private PartyType partyType;

	public RelatedPartyDTO(){
		super();
	}

	public RelatedPartyDTO(String childPartyType, String uuid) {
		partyType = new PartyType(childPartyType);
		setUuid(uuid);
	}

	public PartyType getPartyType() {
		return partyType;
	}

	public void setPartyType(PartyType partyType) {
		this.partyType = partyType;
	}

	@Override
	public String toString() {
		return "RelatedParty [partyType=" + partyType + "]";
	}
}
