package com.mirriad.permissions.rest.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="party")
public class SimplePartyDTO {

	private String uuid;

	public String getUuid() {
		return uuid;
	}
	public SimplePartyDTO() {
		super();
	}

	public SimplePartyDTO(String partyUuid) {
		super();
		this.uuid = partyUuid;
	}

	public void setUuid(String partyUuid) {
		this.uuid = partyUuid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimplePartyDTO other = (SimplePartyDTO) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Party [uuid=" + uuid + "]";
	}
}