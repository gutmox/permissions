package com.mirriad.permissions.rest.dto;

import javax.xml.bind.annotation.XmlRootElement;

import com.sun.istack.NotNull;

@XmlRootElement(name="party")
public class PartyDTO {

	private String name;

	@NotNull
	private String uuid;

	public PartyDTO() {
		super();
	}
	public PartyDTO(String uuid) {
		super();
		this.uuid = uuid;
	}

	public PartyDTO(String uuid, String name) {
		super();
		this.name = name;
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String partyName) {
		this.name = partyName;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String partyUuid) {
		this.uuid = partyUuid;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PartyDTO other = (PartyDTO) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Party [name=" + name + ", uuid=" + uuid + "]";
	}
}
