package com.mirriad.permissions.rest;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.transaction.annotation.Transactional;

import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.dto.RoleDTO;
import com.mirriad.permissions.services.api.PartyRolesService;
import com.sun.istack.NotNull;

/**
 *  Party Roles
 *
 *  /parties/{partyType}/{partyUuid}/roles
 *  GET: return all the roles for partyUuid
 *  DELETE: clear all roles for partyUuid
 *
 *  /parties/{partyType}/{partyUuid}/roles/{roleName}
 *  GET: return the roleName role for partyUuid
 *  DELETE: clear roleName role for partyUuid
 *  PUT:assign partyUuid to roleName
 *
 */
@Named
@Transactional(rollbackFor = Exception.class)
@Consumes({ MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON})
@Path("/parties/{partyType}/{partyUuid}/roles")
public class PartyRolesResource {

	@Inject
	private PartyRolesService partyRolesService;

	@GET
	public Response getRoles(
			@NotNull @PathParam("partyType") String partyType,
			@NotNull @PathParam("partyUuid") String partyUuid) {
		Response validatedParams = validateParams(partyType, partyUuid);
		if (validatedParams  != null){
			return validatedParams;
		}
		Collection<RoleDTO> result;
		try {
			result = partyRolesService.getRoles(partyType, partyUuid);
		} catch (PartyNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}
		if (result == null || result.size() == 0){
			return Response.status(Status.NOT_FOUND).entity("RoleNotFound").build();
		}
		return Response.ok(result).build();
	}

	@GET
	@Path("/{roleName}")
	public Response getRole(@NotNull @PathParam("partyType") String partyType,
			@NotNull @PathParam("partyUuid") String partyUuid,
			@NotNull @PathParam("roleName") String roleName) {
		Response validatedParams = validateParams(partyType, partyUuid, roleName);
		if (validatedParams  != null){
			return validatedParams;
		}
		RoleDTO role;
		try {
			role = partyRolesService.getRole(partyType, partyUuid, roleName);
		} catch (RoleNotFoundException e) {
			return Response.status(Status.NOT_FOUND).entity(e.toString()).build();
		} catch (PartyNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}
		return Response.ok(role).build();
	}

	@DELETE
	public Response deleteRoles(@NotNull @PathParam("partyType") String partyType,
			@NotNull @PathParam("partyUuid") String partyUuid) {
		Response validatedParams = validateParams(partyType, partyUuid);
		if (validatedParams  != null){
			return validatedParams;
		}
		try {
			partyRolesService.deassignAllRole2Party(partyType, partyUuid);
		} catch (PartyNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}
		return Response.ok().build();
	}

	@DELETE
	@Path("/{roleName}")
	public Response deleteRole(@NotNull @PathParam("partyType") String partyType,
			@NotNull @PathParam("partyUuid") String partyUuid,
			@NotNull @PathParam("roleName") String roleName) {
		Response validatedParams = validateParams(partyType, partyUuid, roleName);
		if (validatedParams  != null){
			return validatedParams;
		}
		try {
			partyRolesService.deassignRole2Party(partyType, partyUuid, roleName);
		} catch (PartyNotFoundException | RoleNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}
		return Response.ok().build();
	}

	@PUT
	@Path("/{roleName}")
	public Response assignRole(@NotNull @PathParam("partyType") String partyType,
			@NotNull @PathParam("partyUuid") String partyUuid,
			@NotNull @PathParam("roleName") String roleName) {
		Response validatedParams = validateParams(partyType, partyUuid, roleName);
		if (validatedParams  != null){
			return validatedParams;
		}
		try {
			partyRolesService.assignRole2Party(partyType, partyUuid, roleName);
		} catch (PartyNotFoundException | RoleNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}
		return Response.ok().build();
	}

	private Response validateParams(String partyType, String partyUuid, String roleName) {
		Response validatedParams = validateParams(partyType, partyUuid);
		if (validatedParams  != null){
			return validatedParams;
		}
		if (roleName == null || roleName.trim().equals("")){
			return Response.status(Status.BAD_REQUEST).entity("RoleNameMandatory").build();
		}
		return null;
	}

	private Response validateParams(String partyType, String partyUuid) {
		if (partyType == null || partyType.trim().equals("")){
			return Response.status(Status.BAD_REQUEST).entity("PartyTypeMandatory").build();
		}
		if (partyUuid == null || partyUuid.trim().equals("")){
			return Response.status(Status.BAD_REQUEST).entity("PartyUuidMandatory").build();
		}
		return null;
	}
}
