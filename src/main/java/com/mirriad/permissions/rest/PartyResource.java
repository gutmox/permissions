package com.mirriad.permissions.rest;

import java.util.Collection;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.transaction.annotation.Transactional;

import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.rest.dto.PartyDTO;
import com.mirriad.permissions.rest.dto.PartyTypeDTO;
import com.mirriad.permissions.rest.dto.SimplePartyDTO;
import com.mirriad.permissions.services.api.PartyService;
import com.sun.istack.NotNull;

/**
 * /parties
 *  GET: party Type list, filtered by name
 *
 * /parties/{partyType}
 *	GET:  party list, filtered by name
 *	POST: create a Party
 *  PUT:  update a Party
 *
 * /parties/{partyType}/{partyIdentifier}
 *	GET:    return the party
 *  DELETE: delete party
 *
 */
@Named
@Transactional(rollbackFor = Exception.class)
@Consumes({ MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON})
@Path("/parties")
public class PartyResource {

	@Inject
	private PartyService partyService;

	@GET
	public Response getPartyTypes(
			@DefaultValue("100") @MatrixParam("pageSize") Integer pageSize,
			@DefaultValue("0") @MatrixParam("pageIndex") Integer pageIndex,
			@QueryParam("partyType") String partyTypeName) {
		Set<PartyTypeDTO> found = null;
		if (partyTypeName != null){
			found = partyService.findPartyTypeByName(partyTypeName);
		}else{
			found = partyService.findAllPartyTypes(pageSize, pageIndex);
		}
		if (found == null || found.size() == 0){
			return Response.status(Status.NOT_FOUND).entity("PartyTypeNotFound").build();
		}
		return Response.ok(found).build();
	}

	@GET
	@Path("/{partyType}")
	public Response getPartiesByType(
			@NotNull @PathParam("partyType") String partyType,
			@DefaultValue("100") @MatrixParam("pageSize") Integer pageSize,
			@DefaultValue("0") @MatrixParam("pageIndex") Integer pageIndex,
			@QueryParam("partyName") String partyName) {
		Collection<PartyDTO> found = null;
		if (partyName == null){
			found = partyService.findByPartyType(partyType, pageSize, pageIndex);
		}else{
			found = partyService.findByTypeAndName(partyType, partyName);
		}
		if (found == null || found.size() == 0){
			return Response.status(Status.NOT_FOUND).entity("PartyNotFound").build();
		}
		return Response.ok(found).build();
	}

	@Path("/{partyType}/{partyUuid}")
	@GET
	public Response getParty(
			@NotNull @PathParam("partyType") String partyType,
			@NotNull @PathParam("partyUuid") String partyUuid){
		PartyDTO found;
		try {
			found = partyService.findByUuid(partyType,partyUuid);
			return Response.ok(found).build();
		} catch (PartyNotFoundException e) {
			return Response.status(Status.NOT_FOUND).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/{partyType}")
	public Response createParty(
			@NotNull @PathParam("partyType") String partyType,
			PartyDTO party) {
		String savedUuid = partyService.saveIfNeccesary(partyType, party);
		return Response.ok(new SimplePartyDTO(savedUuid)).build();
	}

	@PUT
	@Path("/{partyType}")
	public Response updateParty(
			@NotNull @PathParam("partyType") String partyType,
			PartyDTO party) {
		try {
			if (party.getUuid() == null){
				return Response.status(Status.BAD_REQUEST).entity("Uuid could shoudn't be null").build();
			}
			partyService.update(partyType, party);
			return Response.ok().build();
		} catch (PartyNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}

	}

	 @Path("/{partyType}/{partyUuid}")
	 @DELETE
	 public Response deleteParty(
			 @NotNull @PathParam("partyType") String partyType,
			 @NotNull @PathParam("partyUuid") String partyUuid){
			if (partyUuid == null){
				return Response.status(Status.BAD_REQUEST).entity("Uuid could shoudn't be null").build();
			}
		 try {
			partyService.delete(partyType, partyUuid);
			return Response.ok().build();
		} catch (PartyNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}
	 }
}
