package com.mirriad.permissions.rest;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.transaction.annotation.Transactional;

import com.mirriad.permissions.exceptions.EntityNotFound;
import com.mirriad.permissions.exceptions.EntityTypeNotFound;
import com.mirriad.permissions.rest.dto.EntityDTO;
import com.mirriad.permissions.rest.dto.EntityTypeDTO;
import com.mirriad.permissions.rest.dto.EntityUuidDTO;
import com.mirriad.permissions.rest.dto.SimpleEntityDTO;
import com.mirriad.permissions.services.api.EntityService;

/**
 * /entities GET: entity type list, filtered by name
 *
 * /entities/{entityType} GET: entities list for entityType, filtered by name
 *
 * POST: create an Entity {"uuid":"1","name":"Californication, season 1",
 * "parents":{"identifier":"1", "entityType":"Campaign"},
 * "children":{"identifier":"1", "entityType":"Episode"}}
 *
 * DELETE: Delete all entities and entity type
 *
 * /entities/{entityType}/{uuid} GET: return the entity DELETE: delete the
 * entity
 *
 */
@Named
@Transactional(rollbackFor = Exception.class)
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@Path("/entities")
public class EntityResource {

	@Inject
	private EntityService entityService;

	@GET
	public Response getEntityTypes(
			@DefaultValue("100") @MatrixParam("pageSize") Integer pageSize,
			@DefaultValue("0") @MatrixParam("pageIndex") Integer pageIndex,
			@QueryParam("name") String entityType) {
		Collection<EntityTypeDTO> result;
		try {
			result = entityService.getAllEntityTypes(entityType, pageSize, pageIndex);
		} catch (EntityTypeNotFound e) {
			return Response.status(Status.NOT_FOUND).entity(e.toString()).build();
		}
		return Response.ok(result).build();
	}

	@GET
	@Path("/{entityType}")
	public Response getEntities(
			@DefaultValue("100") @MatrixParam("pageSize") Integer pageSize,
			@DefaultValue("0") @MatrixParam("pageIndex") Integer pageIndex,
			@PathParam("entityType") String entityType,
			@QueryParam("name") String entityName) {
		Collection<SimpleEntityDTO> allEntities;
		try {
			allEntities = entityService.getAllEntities(entityType, entityName, pageSize, pageIndex);
		} catch (EntityNotFound e) {
			return Response.status(Status.NOT_FOUND).entity(e.toString()).build();
		}
		return Response.ok(allEntities).build();
	}

	@GET
	@Path("/{entityType}/{uuid}")
	public Response getEntity(@PathParam("entityType") String entityType,
			@PathParam("uuid") String uuid) {
		SimpleEntityDTO entity;
		try {
			entity = entityService.getEntity(entityType, uuid);
		} catch (EntityNotFound e) {
			return Response.status(Status.NOT_FOUND).entity(e.toString()).build();
		}
		return Response.ok(entity).build();
	}

	@POST
	@Path("/{entityType}")
	public Response createEntity(@PathParam("entityType") String entityType,
			@NotNull EntityDTO entity) {
		if (entityType == null || entityType.trim().equals("")){
			return Response.status(Status.BAD_REQUEST).entity("entity Type shoudn't be null").build();
		}
		String uuid = entityService.createEntity(entityType, entity);
		return Response.ok(new EntityUuidDTO(uuid)).build();
	}

	@DELETE
	@Path("/{entityType}")
	public Response deleteEntityType(@PathParam("entityType") String entityType) {
		if (entityType == null || entityType.trim().equals("")){
			return Response.status(Status.BAD_REQUEST).entity("entity Type shoudn't be null").build();
		}
		try {
			entityService.deleteEntityType(entityType);
		} catch (EntityTypeNotFound e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}
		return Response.ok().build();
	}

	@DELETE
	@Path("/{entityType}/{uuid}")
	public Response deleteEntity(@PathParam("entityType") String entityType,
			@PathParam("uuid") String uuid) {
		if (entityType == null || entityType.trim().equals("")){
			return Response.status(Status.BAD_REQUEST).entity("entity Type shoudn't be null").build();
		}
		if (uuid == null || uuid.trim().equals("")){
			return Response.status(Status.BAD_REQUEST).entity("uuid shoudn't be null").build();
		}
		try {
			entityService.deleteEntity(entityType, uuid);
		} catch (EntityNotFound e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}
		return Response.ok().build();
	}
}
