package com.mirriad.permissions.rest;

import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.transaction.annotation.Transactional;

import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.rest.dto.AccountabilityDTO;
import com.mirriad.permissions.services.api.AccountabilityService;
import com.sun.istack.NotNull;

/**
 *  Accountabilities:
 *
 * /parties/{partyType}/{partyIdentifier}/relates
 *  GET: Return accountabilities
 *  POST: Create accountability with list other parties
 *  DELETE: Delete all accountabilities for this party
 *
 * /parties/{partyType}/{partyIdentifier}/relates/{accountabilityType}
 *  GET: Return list of this kind of accountability
 *  DELETE: Delete all of this type of accountabilities
 *
 * /parties/{partyType}/{partyIdentifier}/relates/{accountabilityType}/{partyType}/{partyIdentifier}
 *  GET: Detail of accountability
 *  DELETE: Delete accountability
 *
 */
@Named
@Transactional(rollbackFor = Exception.class)
@Consumes({ MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON})
@Path("/parties/{partyType}/{partyUuid}/relates")
public class AccountabilityResource {

	@Inject
	private AccountabilityService accountabilityService;

	@GET
	public Response getAccountabilities(
			@NotNull @PathParam("partyType") String partyType,
			@NotNull @PathParam("partyUuid") String partyUuid) {
		Set<AccountabilityDTO> accountabilityList = accountabilityService.findAccountabilities(partyType, partyUuid);
		if (accountabilityList == null || accountabilityList.size() == 0){
			return Response.status(Status.NOT_FOUND).entity("AccountabilityNotFound").build();
		}
		return Response.ok(accountabilityList).build();
	}

	@GET
	@Path("/{accountabilityType}")
	public Response getAccountabilities(
			@NotNull @PathParam("partyType") String partyType,
			@NotNull @PathParam("partyUuid") String partyUuid,
			@NotNull @PathParam("accountabilityType") String accountabilityType) {
		Set<AccountabilityDTO> accountabilityList = accountabilityService.findAccountabilitiesByType(partyType, partyUuid, accountabilityType);
		if (accountabilityList == null || accountabilityList.size() == 0){
			return Response.status(Status.NOT_FOUND).entity("AccountabilityNotFound").build();
		}
		return Response.ok(accountabilityList).build();
	}

	@GET
	@Path("/{accountabilityType}/{otherLegPartyType}/{otherLegPartyIdentifier}")
	public Response getAccountability(
			@NotNull @PathParam("partyType") String partyType,
			@NotNull @PathParam("partyUuid") String partyUuid,
			@NotNull @PathParam("accountabilityType") String accountabilityType,
			@NotNull @PathParam("otherLegPartyIdentifier") String otherLegPartyIdentifier) {
		Set<AccountabilityDTO> accountabilityList = accountabilityService.findAccountabilitiesByTypeAndChildParty(partyType, partyUuid, accountabilityType,otherLegPartyIdentifier);
		if (accountabilityList == null || accountabilityList.size() == 0){
			return Response.status(Status.NOT_FOUND).entity("AccountabilityNotFound").build();
		}
		return Response.ok(accountabilityList).build();
	}

	@POST
	public Response createAccountability(
			@NotNull @PathParam("partyType") String partyType,
			@NotNull @PathParam("partyUuid") String partyUuid,
			@NotNull AccountabilityDTO accountabilityDTO) {
		try {
			accountabilityService.create(partyType, partyUuid, accountabilityDTO);
		} catch (PartyNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity("PartyNotFound").build();
		}
		return Response.ok().build();
	}

	@DELETE
	public Response deleteAllAccountabilities(
			@NotNull @PathParam("partyType") String partyType,
			@NotNull @PathParam("partyUuid") String partyUuid){
		try {
			accountabilityService.deleteAllAccountabilities(partyType, partyUuid);
		} catch (PartyNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}
		return Response.ok().build();
	}

	@DELETE
	@Path("/{accountabilityType}")
	public Response deleteAccountability(
			@NotNull @PathParam("partyType") String partyType,
			@NotNull @PathParam("partyUuid") String partyUuid,
			@NotNull @PathParam("accountabilityType") String accountabilityType){
		try {
			accountabilityService.deleteAllAccountabilities(partyType, partyUuid,accountabilityType);
		} catch (PartyNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}
		return Response.ok().build();
	}

	@DELETE
	@Path("/{accountabilityType}/{otherLegPartyType}/{otherLegPartyIdentifier}")
	public Response deleteAccountability(
			@NotNull @PathParam("partyType") String partyType,
			@NotNull @PathParam("partyUuid") String partyUuid,
			@NotNull @PathParam("accountabilityType") String accountabilityType,
			@NotNull @PathParam("otherLegPartyType") String otherLegPartyType,
			@NotNull @PathParam("otherLegPartyIdentifier") String otherLegPartyIdentifier){
		try {
			accountabilityService.deleteAllAccountabilities(partyType, partyUuid,accountabilityType,otherLegPartyIdentifier);
		} catch (PartyNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}
		return Response.ok().build();
	}
}
