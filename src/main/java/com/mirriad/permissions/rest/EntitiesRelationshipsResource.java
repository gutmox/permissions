package com.mirriad.permissions.rest;

import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.transaction.annotation.Transactional;

/**
 * /entities/{entityType}/{uuid}/children
 * GET: Return all the children entities
 * DELETE: Delete all the children relationship for uuid
 *
 * /entities/{entityType}/{uuid}/parents
 * GET: Return all the parent entities
 * DELETE: Delete all the parent relationship for uuid
 *
 * /entities/{entityType}/{uuid}/child_of/{entityType}/{uuid2}
 * PUT: Add relationship as child between uuid and uuid
 * DELETE: Delete relationship as child between uuid and uuid
 *
 * /entities/{entityType}/{uuid}/parent_of/{entityType}/{uuid2}
 * PUT: Add relationship as parent between uuid and uuid
 * DELETE: Delete relationship as parent between uuid and uuid
 *
 */
@Named
@Transactional(rollbackFor = Exception.class)
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@Path("/children/{entityType}/{uuid}/")
public class EntitiesRelationshipsResource {

	@GET
	@Path("/children")
	public Response getChildren(
			@DefaultValue("50") @MatrixParam("pageSize") Integer pageSize,
			@DefaultValue("0") @MatrixParam("pageIndex") Integer pageIndex,
			@PathParam("entityType") String entityType,
			@PathParam("uuid") String uuid) {
		return Response.ok().build();
	}

	@GET
	@Path("/parents")
	public Response getParents(
			@DefaultValue("50") @MatrixParam("pageSize") Integer pageSize,
			@DefaultValue("0") @MatrixParam("pageIndex") Integer pageIndex,
			@PathParam("entityType") String entityType,
			@PathParam("uuid") String uuid) {
		return Response.ok().build();
	}

	@DELETE
	@Path("/children")
	public Response deleteChildren(
			@PathParam("entityType") String entityType,
			@PathParam("uuid") String uuid) {
		return Response.ok().build();
	}

	@DELETE
	@Path("/parents")
	public Response deleteParents(
			@PathParam("entityType") String entityType,
			@PathParam("uuid") String uuid) {
		return Response.ok().build();
	}

	@DELETE
	@Path("/child_of/{childEntityType}/{childUuid}")
	public Response deleteChild(
			@PathParam("entityType") String entityType,
			@PathParam("uuid") String uuid,
			@PathParam("childEntityType") String childEntityType,
			@PathParam("childUuid") String childUuid) {
		return Response.ok().build();
	}

	@DELETE
	@Path("/parent_of/{childEntityType}/{childUuid}")
	public Response deleteParent(
			@PathParam("entityType") String entityType,
			@PathParam("uuid") String uuid,
			@PathParam("childEntityType") String childEntityType,
			@PathParam("childUuid") String childUuid) {
		return Response.ok().build();
	}

	@PUT
	@Path("/child_of/{childEntityType}/{childUuid}")
	public Response relateAsChild(
			@PathParam("entityType") String entityType,
			@PathParam("uuid") String uuid,
			@PathParam("childEntityType") String childEntityType,
			@PathParam("childUuid") String childUuid) {
		return Response.ok().build();
	}

	@PUT
	@Path("/parent_of/{childEntityType}/{childUuid}")
	public Response relateAsParent(
			@PathParam("entityType") String entityType,
			@PathParam("uuid") String uuid,
			@PathParam("childEntityType") String childEntityType,
			@PathParam("childUuid") String childUuid) {
		return Response.ok().build();
	}
}