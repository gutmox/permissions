package com.mirriad.permissions.rest;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.transaction.annotation.Transactional;

import com.mirriad.permissions.exceptions.PermissionNotFound;
import com.mirriad.permissions.exceptions.ResourceNotFound;
import com.mirriad.permissions.rest.dto.PermissionDTO;
import com.mirriad.permissions.services.api.PermissionsService;

/**
 *  Permissions
 *
 *  /permissions
 *  GET:  Return all the permissions
 *  POST: Create a new Permission (Resource + Action)
 *
 *  /permissions/{resourceName}
 *  GET:    Return the resource actions
 *  DELETE: Delete the resource
 *
 *  /permissions/{resourceName}/{actionName}
 *  GET: Return the permission (Resource + Action)
 *  DELETE: Delete the Permission (Resource + Action)
 *
 */
@Named
@Transactional(rollbackFor = Exception.class)
@Consumes({ MediaType.APPLICATION_JSON})
@Produces({ MediaType.APPLICATION_JSON})
@Path("/permissions")
public class PermissionResource {

	@Inject
	private PermissionsService permissionsService;


	@GET
	public Response getPermissions(
			@DefaultValue("100") @MatrixParam("pageSize") Integer pageSize,
			@DefaultValue("0") @MatrixParam("pageIndex") Integer pageIndex) {
		Collection<PermissionDTO> result = permissionsService.getPermissions(pageSize, pageIndex);
		if (result == null || result.size() == 0){
			return Response.status(Status.NOT_FOUND).entity("PermissionsNotFound").build();
		}
		return Response.ok(result).build();
	}

	@GET
	@Path("/{resourceName}")
	public Response getPermissions(
			@DefaultValue("100") @MatrixParam("pageSize") Integer pageSize,
			@DefaultValue("0") @MatrixParam("pageIndex") Integer pageIndex,
			@NotNull @PathParam("resourceName") String resourceName) {
		if (resourceName == null || resourceName.trim().equals("")){
			return Response.status(Status.BAD_REQUEST).entity("ResourceMandatory").build();
		}
		Collection<PermissionDTO> result = permissionsService.getPermissions(resourceName, pageSize, pageIndex);
		if (result == null || result.size() == 0){
			return Response.status(Status.NOT_FOUND).entity("PermissionsNotFound").build();
		}
		return Response.ok(result).build();
	}

	@GET
	@Path("/{resourceName}/{actionName}")
	public Response getPermissions(@NotNull @PathParam("resourceName") String resourceName,
			@NotNull @PathParam("actionName") String actionName) {
		PermissionDTO result = permissionsService.getPermission(resourceName, actionName);
		if (result == null){
			return Response.status(Status.NOT_FOUND).entity("PermissionsNotFound").build();
		}
		return Response.ok(result).build();
	}

	@POST
	public Response createPermission(@NotNull PermissionDTO permission) {
		Response response = validate(permission);
		if (response != null)
			return response;
		permissionsService.createPermission(permission);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{resourceName}")
	public Response deleteResource(@NotNull @PathParam("resourceName") String resourceName) {
		if (resourceName == null || resourceName.trim().equals(""))
			return Response.status(Status.BAD_REQUEST).entity("Resource mandatory").build();
		try {
			permissionsService.deleteResource(resourceName);
		} catch (ResourceNotFound e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}
		return Response.ok().build();
	}

	@DELETE
	@Path("/{resourceName}/{actionName}")
	public Response deletePermission(@NotNull @PathParam("resourceName") String resourceName,
			@NotNull @PathParam("actionName") String actionName) {
		if (resourceName == null || resourceName.trim().equals(""))
			return Response.status(Status.BAD_REQUEST).entity("Resource mandatory").build();
		if (actionName == null || actionName.trim().equals(""))
			return Response.status(Status.BAD_REQUEST).entity("Action mandatory").build();
		try {
			permissionsService.deletePermission(resourceName, actionName);
		} catch (PermissionNotFound e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}
		return Response.ok().build();
	}

	private Response validate(PermissionDTO permission) {
		if (permission.getResource() == null || permission.getResource().trim().equals("")){
			return Response.status(Status.BAD_REQUEST).entity("ResourceMandatory").build();
		}
		if (permission.getAction() == null || permission.getAction().trim().equals("")){
			return Response.status(Status.BAD_REQUEST).entity("ResourceMandatory").build();
		}
		return null;
	}
}
