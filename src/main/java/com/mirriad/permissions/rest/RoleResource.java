package com.mirriad.permissions.rest;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.transaction.annotation.Transactional;

import com.mirriad.permissions.exceptions.PartyTypeNotFoundException;
import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.dto.PartyDTO;
import com.mirriad.permissions.rest.dto.RoleDTO;
import com.mirriad.permissions.services.api.RolePartiesService;
import com.mirriad.permissions.services.api.RoleService;
import com.sun.istack.NotNull;

/**
 * Roles
 *
 * /roles GET: return all the roles in the system POST: Create a new Role
 *
 * /roles/{roleName} GET: Return a role DELETE: Delete the role
 *
 * /roles/{roleName}/parties GET: Return parties related to a role
 *
 * /roles/{roleName}/parties/{partyType} GET: Return parties related to a role
 * with a specific partyType
 *
 */
@Named
@Transactional(rollbackFor = Exception.class)
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@Path("/roles")
public class RoleResource {

	@Inject
	private RoleService roleService;

	@Inject
	private RolePartiesService rolePartiesService;

	@GET
	public Response getRoles(
			@DefaultValue("100") @MatrixParam("pageSize") Integer pageSize,
			@DefaultValue("0") @MatrixParam("pageIndex") Integer pageIndex) {
		Collection<RoleDTO> roles = roleService.getRoles(pageIndex, pageSize);
		if (roles == null || roles.size() == 0) {
			return Response.status(Status.NOT_FOUND).entity("RoleNotFound").build();
		}
		return Response.ok(roles).build();
	}

	@GET
	@Path("/{roleName}")
	public Response getRole(@NotNull @PathParam("roleName") String roleName) {
		if (roleName == null || roleName.trim().equals("")) {
			return Response.status(Status.BAD_REQUEST).entity("NameMandatory").build();
		}
		RoleDTO role = roleService.getRole(roleName);
		if (role == null) {
			return Response.status(Status.NOT_FOUND).entity("RoleNotFound").build();
		}
		return Response.ok(role).build();
	}

	@GET
	@Path("/{roleName}/parties")
	public Response getPartiesAssociated2Role(
			@DefaultValue("100") @MatrixParam("pageSize") Integer pageSize,
			@DefaultValue("0") @MatrixParam("pageIndex") Integer pageIndex,
			@NotNull @PathParam("roleName") String roleName) {
		if (roleName == null || roleName.trim().equals("")) {
			return Response.status(Status.BAD_REQUEST).entity("RoleNameMandatory").build();
		}
		try {
			Collection<PartyDTO> parties = rolePartiesService.getParties(roleName, pageSize, pageIndex);
			return Response.ok(parties).build();
		} catch (RoleNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/{roleName}/parties/{partyType}")
	public Response getPartiesAssociated2Role(
			@DefaultValue("100") @MatrixParam("pageSize") Integer pageSize,
			@DefaultValue("0") @MatrixParam("pageIndex") Integer pageIndex,
			@NotNull @PathParam("roleName") String roleName,
			@NotNull @PathParam("partyType") String partyType) {
		if (roleName == null || roleName.trim().equals("")) {
			return Response.status(Status.BAD_REQUEST).entity("NameMandatory").build();
		}
		if (partyType == null || partyType.trim().equals("")) {
			return Response.status(Status.BAD_REQUEST).entity("PartyTypeMandatory").build();
		}
		try {
			Collection<PartyDTO> parties = rolePartiesService.getParties(roleName, partyType, pageSize, pageIndex);
			return Response.ok(parties).build();
		} catch (RoleNotFoundException | PartyTypeNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}

	}

	@POST
	public Response createRole(@NotNull RoleDTO roleDTO) {
		if (roleDTO.getName() == null || roleDTO.getName().trim().equals("")) {
			return Response.status(Status.BAD_REQUEST).entity("NameMandatory").build();
		}
		roleService.createRoleIfNeccesary(roleDTO);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{roleName}")
	public Response deleteRole(@NotNull @PathParam("roleName") String roleName) {
		if (roleName == null || roleName.trim().equals("")) {
			return Response.status(Status.BAD_REQUEST).entity("NameMandatory").build();
		}
		try {
			roleService.deleteRole(roleName);
		} catch (RoleNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).entity(e.toString()).build();
		}
		return Response.ok().build();
	}
}
