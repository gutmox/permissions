package com.mirriad.permissions.services.api;

import java.util.Collection;

import com.mirriad.permissions.exceptions.PartyTypeNotFoundException;
import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.dto.PartyDTO;

public interface RolePartiesService {

	Collection<PartyDTO> getParties(String roleName, Integer pageSize, Integer pageIndex)
			throws RoleNotFoundException;

	Collection<PartyDTO> getParties(String roleName, String partyType, Integer pageSize,
			Integer pageIndex) throws RoleNotFoundException, PartyTypeNotFoundException;
}
