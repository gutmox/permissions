package com.mirriad.permissions.services.api;

import java.util.Collection;

import com.mirriad.permissions.exceptions.PermissionNotFound;
import com.mirriad.permissions.exceptions.ResourceNotFound;
import com.mirriad.permissions.rest.dto.PermissionDTO;

public interface PermissionsService {

	void createPermission(PermissionDTO permission);

	Collection<PermissionDTO> getPermissions(Integer pageSize, Integer pageIndex);

	Collection<PermissionDTO> getPermissions(String resourceName, Integer pageSize, Integer pageIndex);

	PermissionDTO getPermission(String resourceName, String actionName);

	void deleteResource(String resourceName) throws ResourceNotFound;

	void deletePermission(String resourceName, String actionName) throws PermissionNotFound;

}
