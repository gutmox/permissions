package com.mirriad.permissions.services.api;

import java.util.Collection;

import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.dto.RoleDTO;

public interface RoleService {

	void createRoleIfNeccesary(RoleDTO roleDTO);

	Collection<RoleDTO> getRoles(Integer pageIndex, Integer pageSize);

	RoleDTO getRole(String roleName);

	void deleteRole(String roleName) throws RoleNotFoundException;

}
