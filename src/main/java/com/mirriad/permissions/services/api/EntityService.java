package com.mirriad.permissions.services.api;

import java.util.Collection;

import com.mirriad.permissions.exceptions.EntityNotFound;
import com.mirriad.permissions.exceptions.EntityTypeNotFound;
import com.mirriad.permissions.rest.dto.EntityDTO;
import com.mirriad.permissions.rest.dto.EntityTypeDTO;
import com.mirriad.permissions.rest.dto.SimpleEntityDTO;

public interface EntityService {

	String createEntity(String entityType, EntityDTO entity);

	Collection<SimpleEntityDTO> getAllEntities(String entityType, String entityName, Integer pageSize, Integer pageIndex) throws EntityNotFound;

	Collection<EntityTypeDTO> getAllEntityTypes(String entityType, Integer pageSize, Integer pageIndex) throws EntityTypeNotFound;

	SimpleEntityDTO getEntity(String entityType, String uuid) throws EntityNotFound;

	void deleteEntityType(String entityType) throws EntityTypeNotFound;

	void deleteEntity(String entityType, String uuid) throws EntityNotFound;

}
