package com.mirriad.permissions.services.api;

import java.util.Set;

import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.rest.dto.AccountabilityDTO;

public interface AccountabilityService {

	void create(String partyType, String partyUuid, AccountabilityDTO accountabilityDTO) throws PartyNotFoundException;

	Set<AccountabilityDTO> findAccountabilities(String partyType, String partyUuid);

	Set<AccountabilityDTO> findAccountabilitiesByType(String partyType, String partyUuid,
			String type);

	void deleteAllAccountabilities(String partyType, String partyUuid) throws PartyNotFoundException;

	void deleteAllAccountabilities(String partyType, String partyUuid,
			String accountabilityType) throws PartyNotFoundException;

	Set<AccountabilityDTO> findAccountabilitiesByTypeAndChildParty(String partyType,
			String partyUuid, String accountabilityType, String otherLegPartyIdentifier);

	void deleteAllAccountabilities(String partyType, String partyUuid,
			String accountabilityType, String otherLegPartyIdentifier) throws PartyNotFoundException;

}
