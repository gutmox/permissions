package com.mirriad.permissions.services.api;

import java.util.Collection;

import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.dto.RoleDTO;

public interface PartyRolesService {

	void assignRole2Party(String partyType, String partyUuid, String roleName) throws PartyNotFoundException, RoleNotFoundException;

	Collection<RoleDTO> getRoles(String partyType, String partyUuid) throws PartyNotFoundException;

	RoleDTO getRole(String partyType, String partyUuid, String roleName) throws RoleNotFoundException, PartyNotFoundException;

	void deassignAllRole2Party(String partyType, String partyUuid) throws PartyNotFoundException;

	void deassignRole2Party(String partyType, String partyUuid, String roleName) throws PartyNotFoundException, RoleNotFoundException;

}
