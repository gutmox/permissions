package com.mirriad.permissions.services.api;

import java.util.Collection;
import java.util.Set;

import com.mirriad.permissions.domain.Party;
import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.rest.dto.PartyDTO;
import com.mirriad.permissions.rest.dto.PartyTypeDTO;

public interface PartyService {

	Set<PartyDTO> findAll();

	String saveIfNeccesary(String partyType, PartyDTO party);

	PartyDTO findByUuid(String partyType, String partyUuid) throws PartyNotFoundException;

	Collection<PartyDTO> findByTypeAndName(String partyType, String partyName);

	Set<PartyTypeDTO> findAllPartyTypes(Integer pageSize, Integer pageIndex);

	Set<PartyTypeDTO> findPartyTypeByName(String partyTypeName);

	void update(String partyType, PartyDTO party) throws PartyNotFoundException;

	void delete(String partyType, String partyUuid) throws PartyNotFoundException;

	Party findOne(String partyType, String partyUuid) throws PartyNotFoundException;

	Collection<PartyDTO> findByPartyType(String partyType, Integer pageSize, Integer pageIndex);

	Party findOneChecked(String partyType, String partyUuid) throws PartyNotFoundException;

	String save(String partyTypeName, PartyDTO party);
}
