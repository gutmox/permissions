package com.mirriad.permissions.services.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.conversion.EndResult;

import com.mirriad.permissions.dao.api.EntityRepository;
import com.mirriad.permissions.dao.api.EntityTypeRepository;
import com.mirriad.permissions.dao.paging.Paging;
import com.mirriad.permissions.domain.Entity;
import com.mirriad.permissions.domain.EntityType;
import com.mirriad.permissions.exceptions.EntityNotFound;
import com.mirriad.permissions.exceptions.EntityTypeNotFound;
import com.mirriad.permissions.rest.dto.EntireEntityDTO;
import com.mirriad.permissions.rest.dto.EntityDTO;
import com.mirriad.permissions.rest.dto.EntityTypeDTO;
import com.mirriad.permissions.rest.dto.SimpleEntityDTO;
import com.mirriad.permissions.services.api.EntityService;

@Named
public class EntityServiceImpl implements EntityService {

	@Inject
	private EntityTypeRepository entityTypeRepository;

	@Inject
	private EntityRepository entityRepository;

	@Inject
	private Paging paging;

	@Override
	public String createEntity(String entityType, EntityDTO entity) {
		saveRelatives(entity.getChildren());
		saveRelatives(entity.getParents());
		Entity domainEntity = saveEntityChecked(entityType, entity);
		return domainEntity.getUuid();
	}

	@Override
	public Collection<SimpleEntityDTO> getAllEntities(String entityType, String entityName,
			Integer pageSize, Integer pageIndex) throws EntityNotFound {
		Page<Entity> resultPage;
		if (entityName == null || entityName.toString().equals(""))
			resultPage = entityRepository.findByEntityTypeName(entityType, paging.paginateAndSort(pageSize, pageIndex));
		else
			resultPage = entityRepository.findByNameAndEntityTypeName(entityName, entityType,
					paging.paginateAndSort(pageSize, pageIndex));

		if (! resultPage.iterator().hasNext())
			throw new EntityNotFound(entityType, entityName);

		return map(resultPage);
	}

	@Override
	public Collection<EntityTypeDTO> getAllEntityTypes(String entityType, Integer pageSize,
			Integer pageIndex) throws EntityTypeNotFound {
		if (entityType == null || entityType.trim().equals("")) {
			return searchFullTypes(pageSize, pageIndex);
		} else {
			Set<EntityTypeDTO> result = new HashSet<EntityTypeDTO>();
			result.add(findTypeByName(entityType));
			return result;
		}
	}

	@Override
	public SimpleEntityDTO getEntity(String entityType, String uuid) throws EntityNotFound {
		Entity entity = entityRepository.findByEntityTypeNameAndUuid(entityType, uuid);
		if (entity == null)
			throw new EntityNotFound(entityType, uuid);
		return map(entity);
	}

	@Override
	public void deleteEntityType(String entityType) throws EntityTypeNotFound {
		EntityType entityTypeDomain = entityTypeRepository.findByName(entityType);
		validateEntityType(entityType, entityTypeDomain);
		deleteAllEntitiesOfAType(entityType);
		entityTypeRepository.delete(entityTypeDomain);
	}

	@Override
	public void deleteEntity(String entityType, String uuid) throws EntityNotFound {
		Entity entity = entityRepository.findByEntityTypeNameAndUuid(entityType, uuid);
		if (entity == null)
			throw new EntityNotFound(entityType, uuid);
		entityRepository.delete(entity);
	}

	private void validateEntityType(String entityType, EntityType entityTypeDomain)
			throws EntityTypeNotFound {
		if (entityTypeDomain == null)
			throw new EntityTypeNotFound(entityType);
	}

	private void deleteAllEntitiesOfAType(String entityType) {
		EndResult<Entity> entitiesByType = entityRepository.findByEntityTypeName(entityType);
		Iterator<Entity> iterator = entitiesByType.iterator();
		while (iterator.hasNext()) {
			Entity entity = iterator.next();
			entityRepository.delete(entity);
		}
	}

	private Collection<EntityTypeDTO> searchFullTypes(Integer pageSize, Integer pageIndex) throws EntityTypeNotFound {
		Pageable pageable = paging.paginateAndSort(pageSize, pageIndex);
		Page<EntityType> resultPage = entityTypeRepository.findAll(pageable);
		if (! resultPage.iterator().hasNext())
			throw new EntityTypeNotFound();
		return mapTypes(resultPage);
	}

	private EntityTypeDTO findTypeByName(String name) throws EntityTypeNotFound {
		EntityType entityType = entityTypeRepository.findByName(name);
		validateEntityType(name, entityType);
		return map(entityType);
	}

	private EntityTypeDTO map(EntityType entityType) {
		return new EntityTypeDTO(entityType.getName());
	}

	private Entity saveEntityChecked(String entityType, EntityDTO entity) {
		Entity storedEntity = null;
		storedEntity = lookupEntity(entityType, entity, storedEntity);
		if (storedEntity != null)
			return storedEntity;
		storedEntity = saveEntity(entityType, entity);
		return storedEntity;
	}

	private Entity lookupEntity(String entityType, EntityDTO entity, Entity storedEntity) {
		if (entityType != null && entity.getUuid() != null)
			storedEntity = entityRepository.findByEntityTypeNameAndUuid(entityType,
					entity.getUuid());
		return storedEntity;
	}

	private Entity saveEntity(String entityType, EntityDTO entity) {
		EntityType entityTypeDomain = attachEntityType(entityType);
		Entity storedEntity = new Entity(entityTypeDomain, entity.getUuid(), entity.getName());
		generateUuidIfNeccesary(storedEntity);
		entityRepository.save(storedEntity);
		return storedEntity;
	}

	private EntityType attachEntityType(String entityType) {
		EntityType entityTypeDomain = entityTypeRepository.findByName(entityType);
		if (entityTypeDomain == null) {
			entityTypeDomain = new EntityType(entityType);
			entityTypeRepository.save(entityTypeDomain);
		}
		return entityTypeDomain;
	}

	private void generateUuidIfNeccesary(Entity entity) {
		if (entity.getUuid() == null)
			entity.setUuid(UUID.randomUUID().toString());
	}

	private Collection<SimpleEntityDTO> map(Iterable<Entity> content) {
		Set<SimpleEntityDTO> result = new HashSet<SimpleEntityDTO>();
		Iterator<Entity> iterator = content.iterator();
		while (iterator.hasNext()) {
			Entity entity = iterator.next();
			result.add(map(entity));
		}
		return result;
	}

	private Collection<EntityTypeDTO> mapTypes(Iterable<EntityType> content) {
		Set<EntityTypeDTO> result = new HashSet<EntityTypeDTO>();
		Iterator<EntityType> iterator = content.iterator();
		while (iterator.hasNext()) {
			EntityType entityType = iterator.next();
			result.add(new EntityTypeDTO(entityType.getName()));
		}
		return result;
	}

	private SimpleEntityDTO map(Entity entity) {
		return new SimpleEntityDTO(entity.getUuid(), entity.getName());
	}

	private void saveRelatives(Collection<EntireEntityDTO> relative) {
		if (relative != null){
			for (EntireEntityDTO entityDTO : relative) {
				createEntity(entityDTO.getEntityType(), map(entityDTO));
			}
		}
	}

	private EntityDTO map(EntireEntityDTO entityDTO) {
		return new EntityDTO(entityDTO.getUuid(), entityDTO.getName());
	}
}
