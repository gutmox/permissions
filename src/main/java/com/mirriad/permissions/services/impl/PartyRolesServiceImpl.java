package com.mirriad.permissions.services.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.data.neo4j.conversion.EndResult;

import com.mirriad.permissions.dao.api.PartyRepository;
import com.mirriad.permissions.dao.api.PartyRolesRepository;
import com.mirriad.permissions.dao.api.RoleRepository;
import com.mirriad.permissions.domain.Party;
import com.mirriad.permissions.domain.PartyRole;
import com.mirriad.permissions.domain.Role;
import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.dto.RoleDTO;
import com.mirriad.permissions.services.api.PartyRolesService;

@Named
public class PartyRolesServiceImpl implements PartyRolesService{

	@Inject
	private RoleRepository roleRepository;

	@Inject
	private PartyRepository partyRepository;

	@Inject
	private PartyRolesRepository partyRolesRepository;

	private Mapper mapper = new DozerBeanMapper();

	@Override
	public Collection<RoleDTO> getRoles(String partyType, String partyUuid) throws PartyNotFoundException {
		checkParty(partyType, partyUuid);
		EndResult<PartyRole> partyroles = partyRolesRepository.findByPartyUuid(partyUuid);
		return map(partyroles);
	}

	@Override
	public RoleDTO getRole(String partyType, String partyUuid, String roleName) throws RoleNotFoundException, PartyNotFoundException {
		checkParty(partyType, partyUuid);
		PartyRole partyRole = partyRolesRepository.findByPartyUuidAndRoleName(partyUuid, roleName);
		if (partyRole == null)
			throw new RoleNotFoundException(roleName);
		return map(partyRole.getRole());
	}

	@Override
	public void assignRole2Party(String partyType, String partyUuid, String roleName) throws PartyNotFoundException, RoleNotFoundException {
		Party party = checkParty(partyType, partyUuid);
		if (partyRolesRepository.findByPartyUuidAndRoleName(partyUuid, roleName) == null){
			Role role = checkRole(roleName);
			partyRolesRepository.save(new PartyRole(party, role));
		}
	}

	@Override
	public void deassignAllRole2Party(String partyType, String partyUuid) throws PartyNotFoundException {
		checkParty(partyType, partyUuid);
		EndResult<PartyRole> partyroles = partyRolesRepository.findByPartyUuid(partyUuid);
		Iterator<PartyRole> iterator = partyroles.iterator();
		while (iterator.hasNext()) {
			PartyRole partyRole = iterator.next();
			partyRolesRepository.delete(partyRole);
		}

	}

	@Override
	public void deassignRole2Party(String partyType, String partyUuid, String roleName) throws PartyNotFoundException, RoleNotFoundException {
		checkParty(partyType, partyUuid);
		checkRole(roleName);
		PartyRole partyRole = partyRolesRepository.findByPartyUuidAndRoleName(partyUuid, roleName);
		if (partyRole != null){
			partyRolesRepository.delete(partyRole);
		}
	}

	private Party checkParty(String partyType, String partyUuid)
			throws PartyNotFoundException {
		Party party = partyRepository.findByPartyTypeNameAndUuid(partyType, partyUuid);
		if (party == null){
			throw new PartyNotFoundException(partyType, partyUuid);
		}
		return party;
	}

	private Role checkRole(String roleName) throws RoleNotFoundException {
		Role role  = roleRepository.findByName(roleName);
		if (role == null){
			throw new RoleNotFoundException(roleName);
		}
		return role;
	}

	private Collection<RoleDTO> map(EndResult<PartyRole> partyroles) {
		Set<RoleDTO> roles = new HashSet<RoleDTO>();
		Iterator<PartyRole> iterator = partyroles.iterator();
		while (iterator.hasNext()) {
			PartyRole partyRole = iterator.next();
			roles.add(map(partyRole.getRole()));
		}
		return roles;
	}

	private RoleDTO map(Role role) {
		if (role == null)
			return null;
		RoleDTO roleDTO = new RoleDTO();
		mapper.map(role, roleDTO);
		return roleDTO;
	}
}
