package com.mirriad.permissions.services.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.data.domain.Page;

import com.mirriad.permissions.dao.api.PartyRolesRepository;
import com.mirriad.permissions.dao.api.PartyTypeRepository;
import com.mirriad.permissions.dao.api.RoleRepository;
import com.mirriad.permissions.dao.paging.Paging;
import com.mirriad.permissions.domain.PartyRole;
import com.mirriad.permissions.domain.PartyType;
import com.mirriad.permissions.domain.Role;
import com.mirriad.permissions.exceptions.PartyTypeNotFoundException;
import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.dto.PartyDTO;
import com.mirriad.permissions.services.api.RolePartiesService;

@Named
public class RolePartiesServiceImpl implements RolePartiesService {

	@Inject
	private RoleRepository roleRepository;

	@Inject
	private PartyTypeRepository partyTypeRepository;

	@Inject
	private PartyRolesRepository partyRolesRepository;

	@Inject
	private Paging paging;

	private Mapper mapper = new DozerBeanMapper();

	@Override
	public Collection<PartyDTO> getParties(String roleName, Integer pageSize,
			Integer pageIndex) throws RoleNotFoundException {
		checkRole(roleName);
		Page<PartyRole> roles = partyRolesRepository.findByRoleName(roleName,
				paging.paginateAndSort(pageSize, pageIndex, "partyRole_role.name"));
		return map(roles);
	}

	@Override
	public Collection<PartyDTO> getParties(String roleName, String partyType,
			Integer pageSize, Integer pageIndex) throws RoleNotFoundException,
			PartyTypeNotFoundException {
		checkRole(roleName);
		checkPartyType(partyType);
		Page<PartyRole> roles = partyRolesRepository
				.findByRoleNameAndPartyPartyTypeName(roleName, partyType,
						paging.paginateAndSort(pageSize, pageIndex, "partyRole_party.name"));
		return map(roles);
	}

	private void checkRole(String roleName) throws RoleNotFoundException {
		Role storedRole = roleRepository.findByName(roleName);
		if (storedRole == null) {
			throw new RoleNotFoundException(roleName);
		}
	}

	private void checkPartyType(String partyType) throws PartyTypeNotFoundException {
		PartyType storedPartyType = partyTypeRepository.findByName(partyType);
		if (storedPartyType == null) {
			throw new PartyTypeNotFoundException(partyType);
		}
	}

	private Collection<PartyDTO> map(Iterable<PartyRole> roles) {
		Set<PartyDTO> result = new HashSet<PartyDTO>();
		Iterator<PartyRole> iterator = roles.iterator();
		while (iterator.hasNext()) {
			PartyRole partyRole = iterator.next();
			result.add(map(partyRole));
		}
		return result;
	}

	private PartyDTO map(PartyRole partyRole) {
		if (partyRole == null)
			return null;
		PartyDTO party = new PartyDTO();
		mapper.map(partyRole.getParty(), party);
		return party;
	}
}
