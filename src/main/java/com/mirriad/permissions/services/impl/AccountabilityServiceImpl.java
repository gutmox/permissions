package com.mirriad.permissions.services.impl;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.neo4j.conversion.EndResult;

import com.mirriad.permissions.dao.api.AccountabilityRepository;
import com.mirriad.permissions.domain.Accountability;
import com.mirriad.permissions.domain.Party;
import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.rest.dto.AccountabilityDTO;
import com.mirriad.permissions.rest.dto.RelatedPartyDTO;
import com.mirriad.permissions.services.api.AccountabilityService;
import com.mirriad.permissions.services.api.PartyService;

@Named
public class AccountabilityServiceImpl implements AccountabilityService {

	@Inject
	private PartyService partyService;

	@Inject
	private AccountabilityRepository accountabilityRepository;

	@Override
	public Set<AccountabilityDTO> findAccountabilities(String partyType, String partyUuid) {
		EndResult<Accountability> accountabilities = accountabilityRepository
				.getByTypeAndUuid(partyType, partyUuid);
		return map(accountabilities);
	}

	@Override
	public Set<AccountabilityDTO> findAccountabilitiesByType(String partyType,
			String partyUuid, String type) {
		EndResult<Accountability> accountabilities = accountabilityRepository
				.getByAccountabilityType(type, partyType, partyUuid);
		return map(accountabilities);
	}

	@Override
	public Set<AccountabilityDTO> findAccountabilitiesByTypeAndChildParty(String partyType,
			String partyUuid, String accountabilityType, String otherLegPartyIdentifier) {
		EndResult<Accountability> accountabilities = accountabilityRepository
				.getByAccountabilityType(accountabilityType, partyType, partyUuid,
						otherLegPartyIdentifier);
		return map(accountabilities);
	}

	@Override
	public void create(String partyType, String partyUuid,
			AccountabilityDTO accountabilityDTO) throws PartyNotFoundException {
		Accountability accountability = new Accountability();
		addParent2Accountability(partyType, partyUuid, accountability);
		addChild2Accountability(accountabilityDTO, accountability);
		accountability.setAccountabilityType(accountabilityDTO.getAccountabilityType());
		accountabilityRepository.save(accountability);
	}

	@Override
	public void deleteAllAccountabilities(String partyType, String partyUuid)
			throws PartyNotFoundException {
		checkParty(partyType, partyUuid);
		EndResult<Accountability> accountabilities = accountabilityRepository
				.getByTypeAndUuid(partyType, partyUuid);
		delete(accountabilities);
	}

	private void checkParty(String partyType, String partyUuid)
			throws PartyNotFoundException {
		partyService.findByUuid(partyType, partyUuid);
	}

	private void delete(EndResult<Accountability> accountabilities) {
		Iterator<Accountability> iterator = accountabilities.iterator();
		while (iterator.hasNext()) {
			Accountability accountability = iterator.next();
			accountabilityRepository.delete(accountability);
		}
	}

	@Override
	public void deleteAllAccountabilities(String partyType, String partyUuid,
			String accountabilityType) throws PartyNotFoundException {
		checkParty(partyType, partyUuid);
		EndResult<Accountability> accountabilities = accountabilityRepository
				.getByAccountabilityType(accountabilityType, partyType, partyUuid);
		delete(accountabilities);
	}

	@Override
	public void deleteAllAccountabilities(String partyType, String partyUuid,
			String accountabilityType, String otherLegPartyIdentifier)
			throws PartyNotFoundException {
		checkParty(partyType, partyUuid);
		EndResult<Accountability> accountabilities = accountabilityRepository
				.getByAccountabilityType(accountabilityType, partyType, partyUuid,
						otherLegPartyIdentifier);
		delete(accountabilities);
	}

	private void addParent2Accountability(String partyType, String partyUuid,
			Accountability accountability) throws PartyNotFoundException {
		Party parentParty = partyService.findOneChecked(partyType, partyUuid);
		accountability.setParentParty(parentParty);
	}

	private void addChild2Accountability(AccountabilityDTO accountabilityDTO,
			Accountability accountability) throws PartyNotFoundException {
		RelatedPartyDTO childPartyDTO = accountabilityDTO.getChildParty();
		Party childParty = partyService.findOneChecked(
				childPartyDTO.getPartyType().getName(), childPartyDTO.getUuid());
		accountability.setChildParty(childParty);
	}

	private Set<AccountabilityDTO> map(Iterable<Accountability> accountabilities) {
		Set<AccountabilityDTO> accountabilityDTOList = new HashSet<AccountabilityDTO>();
		Iterator<Accountability> iterator = accountabilities.iterator();
		while (iterator.hasNext()) {
			Accountability accountability = iterator.next();
			accountabilityDTOList.add(map(accountability));
		}
		return accountabilityDTOList;
	}

	private AccountabilityDTO map(Accountability accountability) {
		AccountabilityDTO accountabilityDTO = new AccountabilityDTO();
		accountabilityDTO.setAccountabilityType(accountability.getAccountabilityType());
		Party childParty = accountability.getChildParty();
		Party parentParty = accountability.getParentParty();
		RelatedPartyDTO childPartyDTO = new RelatedPartyDTO(childParty.getPartyType()
				.getName(), childParty.getUuid());
		RelatedPartyDTO parentPartyDTO = new RelatedPartyDTO(parentParty.getPartyType()
				.getName(), parentParty.getUuid());
		accountabilityDTO.setChildParty(childPartyDTO);
		accountabilityDTO.setParentParty(parentPartyDTO);
		return accountabilityDTO;
	}
}
