package com.mirriad.permissions.services.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.data.domain.Page;

import com.mirriad.permissions.dao.api.RoleRepository;
import com.mirriad.permissions.dao.paging.Paging;
import com.mirriad.permissions.domain.Role;
import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.dto.RoleDTO;
import com.mirriad.permissions.services.api.RoleService;

@Named
public class RoleServiceImpl implements RoleService {

	private Mapper mapper = new DozerBeanMapper();

	@Inject
	private RoleRepository roleRepository;

	@Inject
	private Paging paging;

	@Override
	public Collection<RoleDTO> getRoles(Integer pageIndex, Integer pageSize) {
		Page<Role> allRoles = roleRepository.findAll(paging.paginateAndSort(pageSize, pageIndex));
		Set<RoleDTO> rolesDTO = map(allRoles);
		return rolesDTO;
	}

	@Override
	public RoleDTO getRole(String roleName) {
		return map(roleRepository.findByName(roleName));
	}

	@Override
	public void deleteRole(String roleName) throws RoleNotFoundException {
		Role storedRole = roleRepository.findByName(roleName);
		if (storedRole == null){
			throw new RoleNotFoundException(roleName);
		}
		roleRepository.delete(storedRole);
	}

	@Override
	public void createRoleIfNeccesary(RoleDTO roleDTO) {
		Role storedRole = roleRepository.findByName(roleDTO.getName());
		if (storedRole == null){
			Role role = map(roleDTO);
			roleRepository.save(role);
		}
	}

	private Role map(RoleDTO roleDTO) {
		Role role = new Role();
		mapper.map(roleDTO, role);
		return role;
	}

	private Set<RoleDTO> map(Page<Role> allRoles) {
		Set<RoleDTO> rolesDTO = new HashSet<RoleDTO>();
		Iterator<Role> iterator = allRoles.iterator();
		while (iterator.hasNext()) {
			map(rolesDTO, iterator);
		}
		return rolesDTO;
	}

	private void map(Set<RoleDTO> rolesDTO, Iterator<Role> iterator) {
		Role role = iterator.next();
		RoleDTO roleDTO = map(role);
		rolesDTO.add(roleDTO);
	}

	private RoleDTO map(Role role) {
		if (role == null)
			return null;
		RoleDTO roleDTO = new RoleDTO();
		mapper.map(role, roleDTO);
		return roleDTO;
	}

}
