package com.mirriad.permissions.services.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Page;
import org.springframework.data.neo4j.conversion.EndResult;

import com.mirriad.permissions.dao.api.ActionRepository;
import com.mirriad.permissions.dao.api.PermissionsRepository;
import com.mirriad.permissions.dao.api.ResourceRepository;
import com.mirriad.permissions.dao.paging.Paging;
import com.mirriad.permissions.domain.Action;
import com.mirriad.permissions.domain.Permission;
import com.mirriad.permissions.domain.Resource;
import com.mirriad.permissions.exceptions.PermissionNotFound;
import com.mirriad.permissions.exceptions.ResourceNotFound;
import com.mirriad.permissions.rest.dto.PermissionDTO;
import com.mirriad.permissions.services.api.PermissionsService;
@Named
public class PermissionsServiceImpl implements PermissionsService {

	@Inject
	private PermissionsRepository permissionsRepository;

	@Inject
	private ResourceRepository resourceRepository;

	@Inject
	private ActionRepository actionRepository;

	@Inject
	private Paging paging;

	@Override
	public Collection<PermissionDTO> getPermissions(Integer pageSize, Integer pageIndex) {
		Page<Permission> allPermissions = permissionsRepository.findAllOrdered(
				paging.paginateAndSort(pageSize, pageIndex, "resource.name"));
		return map(allPermissions);
	}

	@Override
	public Collection<PermissionDTO> getPermissions(String resourceName, Integer pageSize, Integer pageIndex) {
		Page<Permission> allPermissions = permissionsRepository.findByResourceName(resourceName,
				paging.paginateAndSort(pageSize, pageIndex, "permission_resource.name"));
		return map(allPermissions);
	}

	@Override
	public PermissionDTO getPermission(String resourceName, String actionName) {
		Permission permission = permissionsRepository.findByResourceNameAndActionName(resourceName, actionName);
		return map(permission);
	}

	@Override
	public void createPermission(PermissionDTO permission) {
		Permission permissionDomain = permissionsRepository.findByResourceNameAndActionName(permission.getResource(), permission.getAction());
		if (permissionDomain == null){
			permissionDomain = attachResourceAction2Permission(permission);
			permissionsRepository.save(permissionDomain);
		}
	}

	private Permission attachResourceAction2Permission(PermissionDTO permission) {
		Resource resource = resourceRepository.findByName(permission.getResource());
		if (resource == null)
			resource = new Resource(permission.getResource());
		Action action = actionRepository.findByName(permission.getAction());
		if (action == null)
			action = new Action(permission.getAction());
		return new Permission(resource, action);
	}

	@Override
	public void deleteResource(String resourceName) throws ResourceNotFound {
		Resource resource = resourceRepository.findByName(resourceName);
		if (resource == null)
			throw new ResourceNotFound(resourceName);

		EndResult<Permission> allPermissions = permissionsRepository.findByResourceName(resourceName);
		deletePermissions(allPermissions);
		resourceRepository.delete(resource);
	}

	@Override
	public void deletePermission(String resourceName, String actionName) throws PermissionNotFound {
		Permission permission = permissionsRepository.findByResourceNameAndActionName(resourceName, actionName);
		if (permission == null)
			throw new PermissionNotFound(resourceName, actionName);

		permissionsRepository.delete(permission);
	}

	private void deletePermissions(EndResult<Permission> allPermissions) {
		Iterator<Permission> iterator = allPermissions.iterator();
		while (iterator.hasNext()) {
			Permission permission = iterator.next();
			permissionsRepository.delete(permission);
		}
	}

	private Collection<PermissionDTO> map(Iterable<Permission> allPermissions) {
		Iterator<Permission> iterator = allPermissions.iterator();
		Set<PermissionDTO>  result = new HashSet<PermissionDTO>();
		while (iterator.hasNext()) {
			Permission permission = iterator.next();
			result.add(map(permission));
		}
		return result;
	}

	private PermissionDTO map(Permission permission) {
		if (permission == null)
			return null;
		return new PermissionDTO(permission.getResource().getName(), permission.getAction().getName());
	}
}
