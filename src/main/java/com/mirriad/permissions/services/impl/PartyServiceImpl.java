package com.mirriad.permissions.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.transaction.annotation.Transactional;

import com.mirriad.permissions.dao.api.PartyRepository;
import com.mirriad.permissions.dao.api.PartyTypeRepository;
import com.mirriad.permissions.dao.paging.Paging;
import com.mirriad.permissions.domain.Party;
import com.mirriad.permissions.domain.PartyType;
import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.rest.dto.PartyDTO;
import com.mirriad.permissions.rest.dto.PartyTypeDTO;
import com.mirriad.permissions.services.api.PartyService;

@Named
public class PartyServiceImpl implements PartyService {

	private Mapper mapper = new DozerBeanMapper();

	@Inject
	private PartyRepository partyRepository;

	@Inject
	private PartyTypeRepository partyTypeRepository;

	@Inject
	private Paging paging;

	@Override
	public Set<PartyDTO> findAll() {
		EndResult<Party> allFound = partyRepository.findAll();
		Iterator<Party> iterator = allFound.iterator();
		Set<PartyDTO> result = new HashSet<PartyDTO>();
		while (iterator.hasNext()) {
			PartyDTO partyDTO = new PartyDTO();
			mapper.map(iterator.next(), partyDTO);
			result.add(partyDTO);
		}
		return result;
	}

	@Override
	public PartyDTO findByUuid(String partyType, String partyUuid) throws PartyNotFoundException {
		if (partyUuid == null){
			return null;
		}
		Party found = findOneChecked(partyType, partyUuid);
		PartyDTO partyDTO = new PartyDTO();
		if (found != null){
			mapper.map(found, partyDTO);
		}
		return partyDTO;
	}

	@Override
	public Party findOne(String partyType, String partyUuid) {
			if (partyUuid == null)
				return null;
			return partyRepository.findByPartyTypeNameAndUuid(partyType,partyUuid);
	}

	@Override
	public Party findOneChecked(String partyType, String partyUuid) throws PartyNotFoundException {
		Party found = findOne(partyType, partyUuid);
		if (found == null)
			throw new PartyNotFoundException(partyType, partyUuid);

		return found;
	}


	@Override
	public Collection<PartyDTO> findByTypeAndName(String partyType, String partyName) {
		EndResult<Party> found = partyRepository.findByPartyTypeNameAndName(partyType, partyName);
		Iterator<Party> iterator = found.iterator();
		return map(iterator);
	}

	@Override
	public Collection<PartyDTO> findByPartyType(String partyType, Integer pageSize, Integer pageIndex) {
		Page<Party> found = partyRepository.findByPartyTypeName(partyType,
				paging.paginateAndSort(pageSize, pageIndex, "uuid"));
		Iterator<Party> iterator = found.iterator();
		return map(iterator);
	}


	@Override
	public Set<PartyTypeDTO> findAllPartyTypes(Integer pageSize, Integer pageIndex) {
		EndResult<PartyType> found = partyTypeRepository.findAll();
		Iterator<PartyType> iterator = found.iterator();
		Set<PartyTypeDTO> result = mapPartyType(iterator);
		return result;
	}

	@Override
	public Set<PartyTypeDTO> findPartyTypeByName(String partyTypeName) {
		EndResult<PartyType> found = partyTypeRepository.findAllByPropertyValue("name", partyTypeName);
		return mapPartyType(found.iterator());
	}

	@Override
	@Transactional
	public String saveIfNeccesary(String partyTypeName, PartyDTO party) {
		Party partyStored = findOne(partyTypeName, party.getUuid());
		if (partyStored != null)
			return partyStored.getUuid();
		return save(partyTypeName, party);
	}

	@Override
	@Transactional
	public String save(String partyTypeName, PartyDTO party) {
		Party domainParty = map(party);
		attachToPartyType(partyTypeName, domainParty);
		generateUuidIfNeccesary(domainParty);
		partyRepository.save(domainParty);
		return domainParty.getUuid();
	}

	@Override
	public void delete(String partyType, String partyUuid) throws PartyNotFoundException {
		Party found = findOneChecked(partyType, partyUuid);
		partyRepository.delete(found);
	}


	@Override
	public void update(String partyType, PartyDTO party) throws PartyNotFoundException {
		Party found = findOne(partyType, party.getUuid());
		if (found == null){
			throw new PartyNotFoundException(partyType, party.getUuid());
		}else{
			found.setName(party.getName());
			partyRepository.save(found);
		}
	}

	private Party map(PartyDTO party) {
		Party domainParty = new Party();
		mapper.map(party, domainParty);
		return domainParty;
	}

	private void attachToPartyType(String partyTypeName, Party domainParty) {
		PartyType partyType = partyTypeRepository.findByName(partyTypeName);
		if (partyType == null)
			partyType = new PartyType(partyTypeName);
		partyTypeRepository.save(partyType);
		domainParty.setPartyType(partyType);
	}

	private void generateUuidIfNeccesary(Party domainParty) {
		if (domainParty.getUuid() == null){
			domainParty.setUuid(UUID.randomUUID().toString());
		}
	}

	private Collection<PartyDTO> map(Iterator<Party> iterator) {
		List<PartyDTO> result = new ArrayList<PartyDTO>();
		while (iterator.hasNext()) {
			Party party = iterator.next();
			PartyDTO partyDTO = map(party);
			result.add(partyDTO);
		}
		return result;
	}

	private PartyDTO map(Party party) {
		PartyDTO partyDTO = new PartyDTO();
		mapper.map(party, partyDTO);
		return partyDTO;
	}

	private Set<PartyTypeDTO> mapPartyType(Iterator<PartyType> iterator) {
		Set<PartyTypeDTO> result = new HashSet<PartyTypeDTO>();
		while (iterator.hasNext()) {
			PartyType partyType = iterator.next();
			result.add(mapPartyType(partyType));
		}
		return result;
	}

	private PartyTypeDTO mapPartyType(PartyType partyType) {
		PartyTypeDTO partyTypeDTO = new PartyTypeDTO();
		mapper.map(partyType, partyTypeDTO);
		return partyTypeDTO;
	}
}
