package com.mirriad.permissions.dao.api;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.mirriad.permissions.domain.PartyRole;

public interface PartyRolesRepository extends GraphRepository<PartyRole>{

	EndResult<PartyRole> findByPartyPartyTypeNameAndPartyUuid(String partyType, String partyUuid);

	PartyRole findByPartyPartyTypeNameAndPartyUuidAndRoleName(String partyType,
			String partyUuid, String roleName);

	EndResult<PartyRole> findByPartyUuid(String partyUuid);

	PartyRole findByPartyUuidAndRoleName(String partyUuid, String roleName);

	Page<PartyRole> findByRoleName(String roleName, Pageable pageable);

	Page<PartyRole> findByRoleNameAndPartyPartyTypeName(String roleName, String partyTypeName, Pageable pageable);
}
