package com.mirriad.permissions.dao.api;

import org.springframework.data.neo4j.repository.GraphRepository;

import com.mirriad.permissions.domain.PartyType;

public interface PartyTypeRepository extends GraphRepository<PartyType>{

	PartyType findByName(String partyType);

}