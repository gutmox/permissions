package com.mirriad.permissions.dao.api;

import org.springframework.data.neo4j.repository.GraphRepository;

import com.mirriad.permissions.domain.Resource;

public interface ResourceRepository extends GraphRepository<Resource>{

	Resource findByName(String resource);

}
