package com.mirriad.permissions.dao.api;

import org.springframework.data.neo4j.repository.GraphRepository;

import com.mirriad.permissions.domain.EntityType;

public interface EntityTypeRepository extends GraphRepository<EntityType>{

	EntityType findByName(String entityType);

}
