package com.mirriad.permissions.dao.api;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.mirriad.permissions.domain.Permission;

public interface PermissionsRepository extends GraphRepository<Permission>{

	Permission findByResourceNameAndActionName(String resource, String action);

	Page<Permission> findByResourceName(String resourceName, Pageable pageable);

	@Query("START permission=node:__types__(className=\"com.mirriad.permissions.domain.Permission\")  " +
			"MATCH permission -[:OVER]-resource " +
			"RETURN permission ")
	Page<Permission> findAllOrdered(Pageable pageable);

	EndResult<Permission> findByResourceName(String resourceName);

}
