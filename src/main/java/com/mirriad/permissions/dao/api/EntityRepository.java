package com.mirriad.permissions.dao.api;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.mirriad.permissions.domain.Entity;

public interface EntityRepository extends GraphRepository<Entity>{

	Entity findByEntityTypeNameAndUuid(String entityType, String uuid);

	Page<Entity> findByEntityTypeName(String entityType, Pageable pageable);

	Page<Entity> findByNameAndEntityTypeName(String entityName, String entityType,
			Pageable pageable);

	EndResult<Entity> findByEntityTypeName(String entityType);

}
