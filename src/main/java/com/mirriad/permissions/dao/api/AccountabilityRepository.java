package com.mirriad.permissions.dao.api;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import com.mirriad.permissions.domain.Accountability;

public interface AccountabilityRepository extends GraphRepository<Accountability>{

	@Query("start party=node:__types__(className=\"com.mirriad.permissions.domain.Party\") " +
			//" ,partyType=node:__types__(className=\"com.mirriad.permissions.accountability.domain.PartyType\") " +
			"MATCH party-[r:ACCOUNTABILITY]-child " +
			//", party-[t:PARTY_PARTYTYPE]-partyType " +
			"WHERE party.uuid={uuid} " +
			//"AND t.name! ={partyType}" +
			" return r")
	EndResult<Accountability> getByTypeAndUuid(@Param("partyType") String partyType, @Param("uuid") String uuid);

	@Query("start party=node:__types__(className=\"com.mirriad.permissions.domain.Party\") " +
			//" ,partyType=node:__types__(className=\"com.mirriad.permissions.accountability.domain.PartyType\") " +
			"MATCH party-[r:ACCOUNTABILITY]-child " +
			//", party-[t:PARTY_PARTYTYPE]-partyType " +
			"WHERE party.uuid={uuid} " +
			//"AND t.name! ={partyType}" +
			" return r")
	EndResult<Accountability> getByAccountabilityType(
			@Param("type") String type, @Param("partyType") String partyType, @Param("uuid") String partyUuid);

	@Query("start party=node:__types__(className=\"com.mirriad.permissions.domain.Party\") " +
			//" ,partyType=node:__types__(className=\"com.mirriad.permissions.accountability.domain.PartyType\") " +
			"MATCH party-[r:ACCOUNTABILITY]-child " +
			//", party-[t:PARTY_PARTYTYPE]-partyType " +
			"WHERE party.uuid={uuid} " +
			//"AND t.name! ={partyType}" +
			" return r")
	EndResult<Accountability> getByAccountabilityType(
			@Param("type") String type, @Param("partyType") String partyType, @Param("uuid") String partyUuid, @Param("childUuid") String childUuid);

}

