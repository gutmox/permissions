package com.mirriad.permissions.dao.api;

import org.springframework.data.neo4j.repository.GraphRepository;

import com.mirriad.permissions.domain.Role;

public interface RoleRepository extends GraphRepository<Role>{

	Role findByName(String roleName);
}
