package com.mirriad.permissions.dao.api;

import org.springframework.data.neo4j.repository.GraphRepository;

import com.mirriad.permissions.domain.Action;

public interface ActionRepository extends GraphRepository<Action>{

	Action findByName(String action);

}
