package com.mirriad.permissions.dao.api;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.mirriad.permissions.domain.Party;

public interface PartyRepository extends GraphRepository<Party>{

	EndResult<Party> findByPartyTypeNameAndName(String partyType, String partyName);

	Page<Party> findByPartyTypeName(String partyType, Pageable pageable);

	Party findByPartyTypeNameAndUuid(String partyType, String partyUuid);
}

