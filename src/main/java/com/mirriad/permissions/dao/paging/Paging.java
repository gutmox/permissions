package com.mirriad.permissions.dao.paging;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

public interface Paging {
	Pageable paginateAndSort(Integer pageSize, Integer pageIndex);

	Pageable paginateAndSort(Integer pageSize, Integer pageIndex, String... columnOrder);

	Pageable paginateAndSort(Integer pageSize, Integer pageIndex, Direction direction,
			String... columnOrder);

	Pageable paginate(Integer pageSize, Integer pageIndex);
}
