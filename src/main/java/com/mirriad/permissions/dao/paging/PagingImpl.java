package com.mirriad.permissions.dao.paging;

import javax.inject.Named;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

import com.mirriad.permissions.constants.PageSizing;

@Named
public class PagingImpl implements Paging{

	@Override
	public Pageable paginate(Integer pageSize, Integer pageIndex) {
		Pageable pageable = new PageRequest((pageIndex == null)?PageSizing.FIRST_PAGE:pageIndex
				, (pageSize == null)?PageSizing.MAX_PAGE_SIZE:pageSize);
		return pageable;
	}

	@Override
	public Pageable paginateAndSort(Integer pageSize, Integer pageIndex) {
		return paginateAndSort(pageSize, pageIndex, Direction.DESC, "name");
	}

	@Override
	public Pageable paginateAndSort(Integer pageSize, Integer pageIndex, String... columnOrder) {
		return paginateAndSort(pageSize, pageIndex, Direction.DESC, columnOrder);
	}

	@Override
	public Pageable paginateAndSort(Integer pageSize, Integer pageIndex, Direction direction, String... columnOrder) {
		Pageable pageable = new PageRequest((pageIndex == null)?PageSizing.FIRST_PAGE:pageIndex
				, (pageSize == null)?PageSizing.MAX_PAGE_SIZE:pageSize, direction, columnOrder);
		return pageable;
	}

}
