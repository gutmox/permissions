package com.mirriad.permissions.domain;

import java.io.Serializable;

import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

@NodeEntity
public class Party implements Serializable{

	private static final long serialVersionUID = 209898781307504501L;

	@GraphId
	private Long id;

	@Fetch @RelatedTo(type = "PARTY_PARTYTYPE")
	private PartyType partyType;

	@Indexed(indexName="partyUuid")
	@GraphProperty
	private String uuid;

	@Indexed(indexName="partyName")
	@GraphProperty
	private String name;


	public Party() {
		super();
	}

	public Party(String uuid, String name) {
		super();
		this.uuid = uuid;
		this.name = name;
	}

	public Party(PartyType partyType, String uuid, String name) {
		super();
		this.partyType = partyType;
		this.uuid = uuid;
		this.name = name;
	}

	public PartyType getPartyType() {
		return partyType;
	}

	public void setPartyType(PartyType partyType) {
		this.partyType = partyType;
	}

	public String getName() {
		return name;
	}

	public void setName(String partyName) {
		this.name = partyName;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String partyUuid) {
		this.uuid = partyUuid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((partyType == null) ? 0 : partyType.hashCode());
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Party other = (Party) obj;
		if (partyType == null) {
			if (other.partyType != null)
				return false;
		} else if (!partyType.equals(other.partyType))
			return false;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Party [partyType=" + partyType + ", uuid=" + uuid + ", name=" + name + "]";
	}

}
