package com.mirriad.permissions.domain;

import java.io.Serializable;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;


@RelationshipEntity(type="ACCOUNTABILITY")
public class Accountability implements Serializable{

	private static final long serialVersionUID = 6079392728020972132L;

	@GraphId
	private Long id;

	@Fetch @StartNode
	private Party parentParty;

	@Fetch @EndNode
	private Party childParty;

	@GraphProperty
	private String accountabilityType;

	public Party getParentParty() {
		return parentParty;
	}

	public void setParentParty(Party parentParty) {
		this.parentParty = parentParty;
	}

	public Party getChildParty() {
		return childParty;
	}

	public void setChildParty(Party childParty) {
		this.childParty = childParty;
	}

	public String getAccountabilityType() {
		return accountabilityType;
	}

	public void setAccountabilityType(String accountabilityType) {
		this.accountabilityType = accountabilityType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((accountabilityType == null) ? 0 : accountabilityType.hashCode());
		result = prime * result + ((childParty == null) ? 0 : childParty.hashCode());
		result = prime * result + ((parentParty == null) ? 0 : parentParty.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Accountability other = (Accountability) obj;
		if (accountabilityType == null) {
			if (other.accountabilityType != null)
				return false;
		} else if (!accountabilityType.equals(other.accountabilityType))
			return false;
		if (childParty == null) {
			if (other.childParty != null)
				return false;
		} else if (!childParty.equals(other.childParty))
			return false;
		if (parentParty == null) {
			if (other.parentParty != null)
				return false;
		} else if (!parentParty.equals(other.parentParty))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Accountability [parentParty=" + parentParty + ", childParty=" + childParty
				+ ", accountabilityType=" + accountabilityType + "]";
	}
}
