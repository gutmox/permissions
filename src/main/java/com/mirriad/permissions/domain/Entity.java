package com.mirriad.permissions.domain;

import java.io.Serializable;

import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

@NodeEntity
public class Entity implements Serializable{

	private static final long serialVersionUID = 3531190434925351083L;

	@GraphId
	private Long id;

	@Fetch @RelatedTo(type = "ENTITY_ENTITYTYPE")
	private EntityType entityType;

	@Indexed(indexName="entityUuid")
	@GraphProperty
	private String uuid;

	@Indexed(indexName="entityName")
	@GraphProperty
	private String name;

	public Entity() {
		super();
	}

	public Entity(EntityType entityType, String uuid, String name) {
		this();
		this.entityType = entityType;
		this.uuid = uuid;
		this.name = name;
	}

	public EntityType getEntityType() {
		return entityType;
	}

	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entityType == null) ? 0 : entityType.hashCode());
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entity other = (Entity) obj;
		if (entityType == null) {
			if (other.entityType != null)
				return false;
		} else if (!entityType.equals(other.entityType))
			return false;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Entity [entityType=" + entityType + ", uuid=" + uuid + ", name=" + name + "]";
	}
}
