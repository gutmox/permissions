package com.mirriad.permissions.config.neo4j;

import java.util.HashMap;
import java.util.Map;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.kernel.EmbeddedGraphDatabase;
import org.springframework.beans.factory.DisposableBean;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class Neo4jDatasourceConfiguration implements DisposableBean {

	//@Value( "${neo4j.embedded}")
	private boolean dbEmbedded = true;

	//@Value( "${neo4j.rest.url}")
	private String urlEndpointRest = "http://localhost:7474/db/data";

	//@Value( "${neo4j.store.path}")
	private String storePath = "target/neo4j-db";

	//@Value( "${neo4j.check.alive.timeout}")
	private int aliveTimeout = 100;

	private GraphDatabaseService neo4jService=null;

	public GraphDatabaseService getDataSource() {

		if (dbEmbedded) {
			neo4jService = getEbeddedDataSource();
		} else {
			neo4jService = neo4jOceanOS();
			if (neo4jService == null) {
				neo4jService = getEbeddedDataSource();
			}
		}
		return neo4jService;
	}

	private GraphDatabaseService getEbeddedDataSource() {

		System.out.println("** USING EMBEDDED NEO4J **");
		Map<String, String> params = new HashMap<String, String>();
		params.put("allow_store_upgrade", "true");
		GraphDatabaseService neo4jService=new EmbeddedGraphDatabase(storePath, params );

		return neo4jService;
	}

	private GraphDatabaseService neo4jOceanOS() {
		//return new SpringRestGraphDatabase(urlEndpointRest);
		return null;
	}

	private boolean isNeo4jAlive(String urlEndpointRest) {

		ClientConfig config = new DefaultClientConfig();

		Client httpClient=Client.create(config);
		httpClient.setConnectTimeout(aliveTimeout);

		WebResource resource = httpClient.resource(urlEndpointRest);

		int status=resource.get(ClientResponse.class).getStatus();

		return (status == 200);
	}

	@Override
	public void destroy() throws Exception {

		System.out.println("** DESTROYING NEO4J SERVICE **");

		if (dbEmbedded && (neo4jService != null)) {
			neo4jService.shutdown();
		}
	}

}