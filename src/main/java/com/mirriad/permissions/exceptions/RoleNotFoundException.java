package com.mirriad.permissions.exceptions;

public class RoleNotFoundException extends Exception {

	private static final long serialVersionUID = -8875639200280843822L;

	private String roleName;

	public RoleNotFoundException(String roleName) {
		super();
		this.roleName = roleName;
	}

	@Override
	public String toString() {
		return "RoleNotFound [roleName=" + roleName + "]";
	}

}
