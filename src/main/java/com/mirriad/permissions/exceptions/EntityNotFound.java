package com.mirriad.permissions.exceptions;

@SuppressWarnings("serial")
public class EntityNotFound extends Exception {

	private String entityType;
	private String uuid;

	public EntityNotFound(String entityType, String uuid) {
		this.entityType = entityType;
		this.uuid = uuid;
	}

	@Override
	public String toString() {
		return "EntityNotFound [entityType=" + entityType + ", uuid=" + uuid + "]";
	}
}
