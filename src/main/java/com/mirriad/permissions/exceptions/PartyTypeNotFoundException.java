package com.mirriad.permissions.exceptions;


public class PartyTypeNotFoundException extends Exception {

	private static final long serialVersionUID = 6539721182135652450L;

	private String partyType;

	public PartyTypeNotFoundException(String partyType) {
		this.partyType = partyType;
	}

	@Override
	public String toString() {
		return "PartyTypeNotFound [partyType=" + partyType + "]";
	}
}
