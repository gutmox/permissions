package com.mirriad.permissions.exceptions;

public class PermissionNotFound extends Exception {

	private static final long serialVersionUID = 8932344870217479771L;

	private String resource;
	private String action;
	public PermissionNotFound(String resource, String action) {
		super();
		this.resource = resource;
		this.action = action;
	}
	@Override
	public String toString() {
		return "PermissionNotFound [resource=" + resource + ", action=" + action + "]";
	}
}
