package com.mirriad.permissions.exceptions;

public class ResourceNotFound extends Exception {

	private static final long serialVersionUID = -8470568139497458751L;

	private String resource;

	public ResourceNotFound(String resource) {
		super();
		this.resource = resource;
	}

	@Override
	public String toString() {
		return "ResourceNotFound [resource=" + resource + "]";
	}
}
