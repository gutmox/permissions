package com.mirriad.permissions.exceptions;

public class PartyNotFoundException extends Exception {
	private static final long serialVersionUID = -4924384882765184405L;

	private String partyType;
	private String partyName;
	public PartyNotFoundException(String partyType, String partyName) {
		super();
		this.partyType = partyType;
		this.partyName = partyName;
	}
	@Override
	public String toString() {
		return "PartyNotFound [partyType=" + partyType + ", partyName=" + partyName	+ "]";
	}
}
