package com.mirriad.permissions.exceptions;

public class EntityTypeNotFound extends Exception {

	private static final long serialVersionUID = 7797411994071969815L;

	private String name;

	public EntityTypeNotFound() {
		super();
	}

	public EntityTypeNotFound(String name) {
		this();
		this.name = name;
	}

	@Override
	public String toString() {
		return "EntityTypeNotFound [name=" + name + "]";
	}
}
