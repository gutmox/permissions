@PartyRoles
Feature: Deassign roles of party
URI Rest: /parties/{partyType}/{partyUuid}/roles  DELETE

  As Business Manager
  In order to keep my permissions updated 
  I'd like to deassign those privileges to any person or group
  
Scenario: Successful all roles
	Given a partyType "user" named "Messi" with uuid "messi" assigned to roles "CatalogueEditor" and "CampaginEditor" 
	When I call to deassign all the roles for the "user" "messi" 
    Then I receive a HTTP "OK"
	And I don't get any rol for "user" "messi"
	    
Scenario: Successful for one role
	Given a partyType "user" named "Messi" with uuid "messi" assigned to roles "CatalogueEditor" and "CampaginEditor" 
	When I call to deassign all the role "CampaginEditor" for the "user" "messi" 
    Then I receive a HTTP "OK"
    And I get the rol "CatalogueEditor" for "user" "messi"
    
Scenario: Error not existing party
	When I call to deassign all the role "CampaginEditor" for the "user" "not_existing"
    Then I receive a HTTP "BAD_REQUEST"
    
Scenario: Error not existing partyType
	When I call to deassign all the role "CampaginEditor" for the "pear" "messi"
    Then I receive a HTTP "BAD_REQUEST"
    
 Scenario: Error not empty party
    When I call to deassign all the role "CampaginEditor" for the "pear" ""
    Then I receive a HTTP "NOT_FOUND"
    
Scenario: Error not empty party type
	When I call to deassign all the role "CampaginEditor" for the "" "messi"
    Then I receive a HTTP "NOT_FOUND"