@Permissions
Feature: Permissions creation
URI Rest: /permissions POST
{
	'resource' : 'Campaign',
	'action' : 'View',
}
  As Business Manager
  In order to define what a user can do or not   
  I'd like to create my security as resources to protect and actions over them   
  
Scenario: Successful
	When I call to create the  permission "Campaign" "View"
    Then I receive a HTTP "OK"
    
Scenario: Error empty resource
	When I call to create the  permission "" "View"
    Then I receive a HTTP "BAD_REQUEST"
    
Scenario: Error empty action
	When I call to create the  permission "Campaign" ""
    Then I receive a HTTP "BAD_REQUEST"
    
Scenario: Error not resource
	When I call to create the  permission "View" without Resource
    Then I receive a HTTP "BAD_REQUEST"
    
Scenario: Error not action
	When I call to create the  permission "Campaign" without Action
    Then I receive a HTTP "BAD_REQUEST"