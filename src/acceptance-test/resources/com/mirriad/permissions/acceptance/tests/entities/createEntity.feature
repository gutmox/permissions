@Entities
Feature: Store an entity
URI Rest: /entities/{entityType}  POST
{"uuid":"1","name":"Californication, season 1",
 "parents":{"identifier":"1", "entityType":"Campaign"},
 "children":{"identifier":"1", "entityType":"Episode"}}

  As Business Manager
  In order to be able to define permissions for a specific domain entity
  I'd like to store my domain entities in the permission system
  
Scenario: Successful all fields
	When I call to add new entity "Enchanted" with uuid "enchanted_uuid" and entity type "campaign"
    Then I receive a HTTP "OK"
    And  I get the same entity uuid "enchanted_uuid"
    
Scenario: Successful without uuid
    When I call to add new entity "Microsoft X-Box One" without uuid and entity type "campaign"
    Then I receive a HTTP "OK"
    And  I get a generated entity uuid
    
Scenario: Successful without neither name nor uuid
    When I call to add new entity without neither name nor uuid and entity type "campaign"
    Then I receive a HTTP "OK"
    And  I get a generated entity uuid
    
Scenario: Successful without name
    When I call to add new entity without name but with uuid "djarum_uuid" and entity type "campaign"
    Then I receive a HTTP "OK"
    And  I get the same entity uuid "djarum_uuid"
    
Scenario: Error without PartyType
    When I call to add new entity without any partyTppe
    Then I receive a HTTP "METHOD_NOT_ALLOWED"
