@Parties
Feature: Delete a party
URI Rest: /parties/{partyType}/{partyUuid}  DELETE

  As Business Manager
  In order to keep all my group, departments and staff updated when a member decide to work for google
  I'd like to erase groups, departments and users

Scenario: Successful Delete user by uuid
	Given a partyType "user" named "Ramon" with uuid "ramon_uuid"
	When I call to delete a party with uuid "ramon_uuid" and party type "user"
    Then I receive a HTTP "OK"
    
Scenario: Successful delete group by uuid 
   Given a partyType "group" named "Justin Bieber Managers" with uuid "justin_uuid"
	When I call to delete a party with uuid "justin_uuid" and party type "group"
    Then I receive a HTTP "OK"

Scenario: Error without Party UUid not existing
	When I call to delete a party with uuid "non_existing_uuid" and party type "user"
    Then I receive a HTTP "BAD_REQUEST"

        
Scenario: Error without PartyType
    When I call to delete a party without any partyTppe
    Then I receive a HTTP "METHOD_NOT_ALLOWED"