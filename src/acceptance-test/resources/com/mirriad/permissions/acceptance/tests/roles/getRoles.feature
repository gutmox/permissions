@Roles
Feature: Get a role
URI Rest: /roles  GET

  As Business Manager
  In order to have different data visibility layers 
  I'd like to get roles for users o groups
  
Scenario: Successful
	Given a role "PresentationEditor"
	When I call to get a role "PresentationEditor"
    Then I receive a HTTP "OK"
    And  I receive a single role "PresentationEditor"
    
Scenario: Successful list
	Given a role "PresentationEditor"
	When I call to get all roles
    Then I receive a HTTP "OK"
    And  I receive a role list

Scenario: Error role not found
	When I call to get a role "NotExistingRole"
    Then I receive a HTTP "NOT_FOUND"
    