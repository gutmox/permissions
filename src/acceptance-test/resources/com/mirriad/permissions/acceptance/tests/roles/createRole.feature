@Roles
Feature: Store a role
URI Rest: /roles  POST
{
   "name": "CampaignEditor",
   "description": "They can edit campaigns"
}

  As Business Manager
  In order to have different data visibility layers 
  I'd like to create roles to attach them to different groups or users and assign to permissions on domain entities
  
Scenario: Successful
	When I call to add new role "CampaignEditor" with description "They can edit campaigns"
    Then I receive a HTTP "OK"
    
Scenario: Successful without description
	When I call to add new role "CampaignViewer" without description
    Then I receive a HTTP "OK"

Scenario: Error without name
	When I call to add new role without name
    Then I receive a HTTP "BAD_REQUEST"