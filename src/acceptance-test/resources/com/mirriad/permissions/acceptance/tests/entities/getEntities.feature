@Entities
Feature: Get entities
  
  URI Rest: /entities/{entityType}/{uuid} GET
  
  As Business Manager
  In order to be able to define permissions for a specific domain entity
  I'd like to get my domain entities stored in the permission system

  Scenario: Successful all types
    When I call to add new entity "Enchanted" with uuid "enchanted_uuid" and entity type "campaignSimilar"
    When I call to get all entities types
    Then I receive a HTTP "OK"
    And I get a list of entities in which I one element "campaignSimilar" type;

  Scenario: Successful types filtered by name
    When I call to add new entity "Enchanted" with uuid "enchanted_uuid" and entity type "campaignSimilar"
    When I call to get all entities types filtered by "campaignSimilar"
    Then I receive a HTTP "OK"
    And I get a list of entities with only one element "campaignSimilar" type;

  Scenario: Successful type Not found
    When I call to get all entities types filtered by "not_existing"
    Then I receive a HTTP "NOT_FOUND"

  Scenario: Successful all entities of a type
    When I call to add new entity "Enchanted" with uuid "enchanted_uuid" and entity type "campaignLike"
    When I call to get all entities of type "campaignLike"
    Then I receive a HTTP "OK"
    And I get a list of entities in which I one element "enchanted_uuid";

  Scenario: Successful all entities filtered by name
    When I call to add new entity "Enchanted" with uuid "enchanted_uuid" and entity type "campaignLike"
    When I call to get all entities of type "campaignLike" filtered by name "Enchanted"
    Then I receive a HTTP "OK"
    And I get a list of entities in which I one element "enchanted_uuid";

  Scenario: Successful entity not found
    When I call to get all entities of type "campaignLike" filtered by name "Not_Existing"
    Then I receive a HTTP "NOT_FOUND"

  Scenario: Successful one
    When I call to add new entity "Enchanted" with uuid "enchanted_uuid" and entity type "campaign"
    When I call to get an entity of type "campaign" and uuid "enchanted_uuid"
    Then I receive a HTTP "OK"
    And I get an entity which of type "campaign" and uuid "enchanted_uuid"

  Scenario: Successful list not found
    When I call to get all entities of type "phones"
    Then I receive a HTTP "NOT_FOUND"

  Scenario: Successful not found
    When I call to get an entity of type "campaign" and uuid "not_existing_uuid"
    Then I receive a HTTP "NOT_FOUND"
