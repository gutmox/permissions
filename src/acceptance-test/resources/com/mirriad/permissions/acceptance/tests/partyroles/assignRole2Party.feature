@PartyRoles
Feature: Assign role 2 party
URI Rest: /parties/{partyType}/{partyUuid}/roles/{roleName}  PUT

  As Business Manager
  In order to grant people or groups to access to edit, view or create over Campaigns, Media Families or Presentaions 
  I'd like to assign those privileges to any person or group  
  
Scenario: Successful
	Given a partyType "user" named "thatDude" with uuid "that_dude"
	And a role "CatalogueEditor"
	When I call to assign the "user" "that_dude" to the role "CatalogueEditor"
    Then I receive a HTTP "OK"
    
Scenario: Error not existing party
	When I call to assign the "user" "not_existing" to the role "CatalogueEditor"
    Then I receive a HTTP "BAD_REQUEST"
    
Scenario: Error not existing partyType
	When I call to assign the "extrangePartyType" "paul" to the role "CatalogueEditor"
    Then I receive a HTTP "BAD_REQUEST"

Scenario: Error not existing role
	Given a partyType "user" named "thatBlock" with uuid "that_block"
	When I call to assign the "user" "that_block" to the role "NotExistingRole"
    Then I receive a HTTP "BAD_REQUEST"
    
 Scenario: Error not empty party
	When I call to assign the "user" "" to the role "CatalogueEditor"
    Then I receive a HTTP "NOT_FOUND"
    
Scenario: Error not empty party type
	When I call to assign the "" "johnny" to the role "CatalogueEditor"
    Then I receive a HTTP "NOT_FOUND"
    
Scenario: Error not empty role
	When I call to assign the "user" "johnny" to the role ""
    Then I receive a HTTP "METHOD_NOT_ALLOWED"