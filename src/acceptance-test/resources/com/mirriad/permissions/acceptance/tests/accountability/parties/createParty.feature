@Parties
Feature: Store a party
URI Rest: /parties/{partyType}  POST
{
   "partyName": "Valarie",
   "partyUuid": "valarieUuid"
}

  As Business Manager
  In order to see all the data than my staff can see 
  I'd like to create easely my company structure
  
Scenario: Successful all fields
	When I call to add new party "John" with uuid "john1" and party type "user"
    Then I receive a HTTP "OK"
    And  I get teh same uuid "john1"
    
Scenario: Successful without uuid
	When I call to add new party "Jenny" without uuid and party type "user"
    Then I receive a HTTP "OK"
    And  I get a generated uuid
    
Scenario: Successful group
	When I call to add new party "Beyonce Presentation Managers" without uuid and party type "group"
    Then I receive a HTTP "OK"
    And  I get a generated uuid
    
Scenario: Successful department
	When I call to add new party "UMG India Sales" without uuid and party type "department"
    Then I receive a HTTP "OK"
    And  I get a generated uuid
    
Scenario: Successful without neither name nor uuid
    When I call to add new party without any name nor uuid and party type "user"
    Then I receive a HTTP "OK"
    And  I get a generated uuid
    
Scenario: Successful without name
    When I call to add new party without any name, uuid "noname1" and party type "user"
    Then I receive a HTTP "OK"
    And  I get teh same uuid "noname1"
    
Scenario: Error without PartyType
    When I call to add new party without any partyTppe
    Then I receive a HTTP "METHOD_NOT_ALLOWED"
