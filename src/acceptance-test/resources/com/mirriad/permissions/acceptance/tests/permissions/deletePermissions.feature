@Permissions
Feature: Deleting Permissions
URI Rest: /permissions/{resourceName}/{actionName} DELETE

As Business Manager
  In order to keep my resources updated   
  I'd like to remove actions and resources when are also changed in the original system 
  
Scenario: Successful for resource
	Given a permission "Episode" "View"
	Given a permission "Hub" "View"
	When I call to delete the resource "Episode"
    Then I receive a HTTP "OK"
    When I call to get permissions for resource "Episode"
    Then I receive a HTTP "NOT_FOUND"
    
Scenario: Successful for resource and action
	Given a permission "EmbedProject" "Editor"
	Given a permission "EmbedProject" "View"
	When I call to delete the permission "EmbedProject" "Editor"
    Then I receive a HTTP "OK"
    When I call to get permissions for resource "EmbedProject" and action "Editor" 
    Then I receive a HTTP "NOT_FOUND"
    
Scenario: Error empty resource
	When I call to delete the resource "NotExisting"
    Then I receive a HTTP "BAD_REQUEST"
    