@Parties
Feature: Get parties
URI Rest: /parties/{partyType}  GET

  As Business Manager
  In order to see all the data than my staff can see 
  I'd like to navigate easely my company structure
	
Scenario: Successful Get by uuid
    Given a partyType "user" named "JohnSilver" with uuid "john_silver"
    When I call to get a party with partyType "user" by uuid "john_silver"
    Then I receive a HTTP "OK"
    And I receive the user "JohnSilver" with uuid "john_silver"
    
Scenario: Successful Get by Name
    Given a partyType "user" named "JohnSilver" with uuid "john_silver"
	When I call to get a party with partyType "user" by name "JohnSilver"
    Then I receive a HTTP "OK"
    And I receive a list of users with name "JohnSilver"
    
Scenario: Successful All
  Given a partyType "user" named "JohnSilver" with uuid "john_silver"
  When I call to get all parties with partyType "user" 
  Then I receive a HTTP "OK"
  And I receive a complete list

Scenario: Error not found
  When I call to get all parties with partyType "oranges" 
  Then I receive a HTTP "NOT_FOUND"

Scenario: Error user not found
  When I call to get a party with partyType "user" by name "NOTJohnSilver" 
  Then I receive a HTTP "NOT_FOUND"
  
Scenario: Error Not Found by uuid
    When I call to get a party with partyType "user" by uuid "NotJohn_silver"
    Then I receive a HTTP "NOT_FOUND"
          