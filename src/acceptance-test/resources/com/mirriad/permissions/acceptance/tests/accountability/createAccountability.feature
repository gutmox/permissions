@Accountability
Feature: Create an accountability
URI Rest: /parties/{partyType}/{partyIdentifier}/relates  POST

  As Business Manager
  In order to have my departments and groups organization updated
  I'd like to create relations users to groups, groups to groups, departments to users. Relations of type 'depends of', 'is part of', 'managed by'
	
Scenario: Successful between users
	Given a partyType "user" named "Robert" with uuid "robert"
	Given a partyType "user" named "Ramon" with uuid "ramon"
	When I call add a new relation "colleague" between "user" "robert" and "user" "ramon" 
	Then I receive a HTTP "OK"
	
Scenario: Successful between an user and a department
	Given a partyType "user" named "Ramon" with uuid "ramon"
	Given a partyType "department" named "General marketing" with uuid "general_marketing"
	When I call add a new relation "works in" between "department" "general_marketing" and "user" "ramon" 
	Then I receive a HTTP "OK"

Scenario: Successful between groups 	
	Given a partyType "department" named "General marketing" with uuid "general_marketing"
	Given a partyType "department" named "India marketing" with uuid "india_marketing"
	When I call add a new relation "depends of" between "department" "india_marketing" and "department" "general_marketing" 
	Then I receive a HTTP "OK"
	
Scenario: Error without PartyType
    Given a partyType "department" named "General marketing" with uuid "general_marketing"
    When I call add a new relation "depends of" between "" "india_marketing" and "department" "general_marketing"
    Then I receive a HTTP "NOT_FOUND"
    
Scenario: Error without party uuid 	
	Given a partyType "department" named "General marketing" with uuid "general_marketing"
	When I call add a new relation "depends of" between "department" "" and "department" "general_marketing" 
	Then I receive a HTTP "NOT_FOUND"
	
Scenario: Error with Party Uuid not existing
    Given a partyType "department" named "General marketing" with uuid "general_marketing"
	When I call add a new relation "bosses" between "user" "non_existing_uuid" and "department" "general_marketing"
    Then I receive a HTTP "BAD_REQUEST"