@Accountability
Feature: Get accountabilities
URI Rest: /parties/{partyType}/{partyIdentifier}/relates  GET

  As Business Manager
  In order to know hnow my departments and groups are related in Mirriad system
  I'd like to get all the relations among users, groups, departments, etc.
	
Scenario: Successful
	Given a partyType "user" named "Paul" with uuid "paul"
	Given a partyType "department" named "New Man" with uuid "newman"
	Given an accountability "works_in" between "user" "paul" and "department" "newman"
	When I call to get the relations of "user" "paul" 
	Then I receive a HTTP "OK"
	And I get the accountability "works_in" "department" "newman"

Scenario: Successful with accountability type
	Given a partyType "user" named "Paul" with uuid "paul"
	Given a partyType "department" named "New Man" with uuid "newman"
	Given an accountability "works_in" between "user" "paul" and "department" "newman"
	When I call to get the relations of "user" "paul" with accountability type "works_in"
	Then I receive a HTTP "OK"
	And I get the accountability "works_in" "department" "newman"
	
Scenario: Successful with other leg uuid
	Given a partyType "user" named "Paul" with uuid "paul"
	Given a partyType "department" named "New Man" with uuid "newman"
	Given an accountability "works_in" between "user" "paul" and "department" "newman"
	When I call to get the relations of "user" "paul" with accountability type "works_in" and other leg "department" "newman" 
	Then I receive a HTTP "OK"
	And I get the accountability "works_in" "department" "newman"
	
Scenario: Error not found
	When I call to get the relations of "user" "paul222" 
	Then I receive a HTTP "NOT_FOUND"
	
Scenario: Error not party type
	When I call to get the relations of "" "paul222" 
	Then I receive a HTTP "NOT_FOUND"
	
Scenario: Error not party uuid
	When I call to get the relations of "user" "" 
	Then I receive a HTTP "NOT_FOUND"