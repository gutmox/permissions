@Entities
Feature: Delete entities
  
  URI Rest: /entities/{entityType}/{uuid} DELETE
  
  As Business Manager
  In order to keep my data updated regarding to permissions
  I'd like to delete my domain entities stored in the permission system

  Scenario: Successful delete all of a type
    When I call to add new entity "Video" with uuid "video_uuid" and entity type "Players"
    When I call to delete all entities types"Players"
    Then I receive a HTTP "OK"
    When I call to get all entities of type "Players"
    Then I receive a HTTP "NOT_FOUND"

  Scenario: Successful delete an entity
    When I call to add new entity "MyChair" with uuid "my_chair_uuid" and entity type "Chairs"
    When I call to delete thr entity "my_chair_uuid" of type "Chairs"
    Then I receive a HTTP "OK"
    When I call to get an entity of type "Chairs" and uuid "my_chair_uuid"
    Then I receive a HTTP "NOT_FOUND"

  Scenario: Error type not found
    When I call to delete all entities types"NotExistingType"
    Then I receive a HTTP "BAD_REQUEST"

  Scenario: Error not existing Entity
    When I call to delete thr entity "not_existing_uuid" of type "Chairs"
    Then I receive a HTTP "BAD_REQUEST"
