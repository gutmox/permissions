@Accountability
Feature: Delete accountabilities
URI Rest: /parties/{partyType}/{partyIdentifier}/relates  DELETE

  As Business Manager
  In order to have my departments and groups organization updated
  I'd like to delete any kind of relations created before
	
Scenario: Successful Delete all party's accountabilities
	Given a partyType "user" named "Ringo" with uuid "ringo"
	Given a partyType "band" named "The beatles" with uuid "beatles"
	Given an accountability "played_in" between "user" "ringo" and "band" "beatles"
	When I call to delete the relations of "user" "ringo" 
	Then I receive a HTTP "OK"
	And I don't get an accountability "played_in" "band" "beatles" for party "user" "ringo"
	
Scenario: Successful Delete accountability type party's accountabilities
	Given a partyType "user" named "Ringo" with uuid "ringo"
	Given a partyType "band" named "The beatles" with uuid "beatles"
	Given an accountability "played_in" between "user" "ringo" and "band" "beatles"
	When I call to delete the "played_in" relations of "user" "ringo" 
	Then I receive a HTTP "OK"
	And I don't get an accountability "played_in" "band" "beatles" for party "user" "ringo"
	
Scenario: Successful Delete accountabilities
	Given a partyType "user" named "Ringo" with uuid "ringo"
	Given a partyType "band" named "The beatles" with uuid "beatles"
	Given an accountability "played_in" between "user" "ringo" and "band" "beatles"
	When I call to delete the "played_in" relations of "user" "ringo" on "band" "beatles"
	Then I receive a HTTP "OK"
	And I don't get an accountability "played_in" "band" "beatles" for party "user" "ringo"

Scenario: Error Not PartyType  
	When I call to delete the relations of "" "ringo" 
	Then I receive a HTTP "NOT_FOUND"
		
Scenario: Error Not PartyType and 2 legs
	When I call to delete the relations of "user" "" 
	Then I receive a HTTP "NOT_FOUND"
		
Scenario: Error Not PartyType and 2 legs
	When I call to delete the "played_in" relations of "" "ringo" on "band" "beatles"
	Then I receive a HTTP "NOT_FOUND"
	
Scenario: Error Not partyIdentifier 
	When I call to delete the "played_in" relations of "user" "" on "band" "beatles"
	Then I receive a HTTP "NOT_FOUND"
	
Scenario: Error Not right leg partyUuid 
	When I call to delete the "played_in" relations of "user" "ringo" on "band" ""
	Then I receive a HTTP "NOT_FOUND"
	