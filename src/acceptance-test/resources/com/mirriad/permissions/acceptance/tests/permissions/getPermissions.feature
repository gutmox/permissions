@Permissions
Feature: Getting Permissions
URI Rest: /permissions GET

As Business Manager
  In order to know what actions and resources are in the security system   
  I'd like to get a permission list   
  
Scenario: Successful
	Given a permission "Presentation" "View"
	When I call to get all the  permissions
    Then I receive a HTTP "OK"
    And I get a list with permissions
    
Scenario: Successful for resource
	Given a permission "Presentation" "View"
	Given a permission "Hub" "View"
	When I call to get permissions for resource "Presentation"
    Then I receive a HTTP "OK"
    And I get a list with permissions for "Presentation"
    
Scenario: Successful for permission
	Given a permission "Presentation" "View"
	Given a permission "Hub" "View"
	When I call to get permissions for resource "Presentation" and action "View" 
    Then I receive a HTTP "OK"
    And I get one permission "Presentation" "View"