@Parties
Feature: Get party types
URI Rest: /parties  GET

  As Business Manager
  In order to see all the data than my staff can see 
  I'd like to know all the existing groups or users
	
Background:
	Given a partyType "user" named "SarahConnor" with uuid "sarah_connor"
	Given   a partyType "group" named "Beyonce" with uuid "beyonce"
	Given   a partyType "department" named "Sales" with uuid "sales"
	
Scenario: Successful All
    When I call to get partyTypes
    Then I receive a HTTP "OK"
    And I receive all the partyTypes:
	  | user         |
      | group        |
      | department   |
    
Scenario: Successful by Name
	When I call to get partyTypes filtered by name "user"
    Then I receive a HTTP "OK"
    And I receive all partyTypes stored with name "user"

          