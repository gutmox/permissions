@Roles
Feature: Get parties for role
URI Rest: /roles/{roleName}/parties GET

As Business Manager
  In order to administrate the groups and people roles 
  I'd like to know what groups and people belong to what roles
  
Scenario: Successful
	Given a partyType "dude" named "Tom" with uuid "tom" assigned to roles "HubEditor" and "CampaginEditor"
	Given a partyType "dude" named "Jimmy" with uuid "jimmy" assigned to roles "HubEditor" and "CampaginEditor"
	Given a partyType "brach" named "Rounders" with uuid "rounders" assigned to roles "HubEditor" and "CampaginEditor"
	When I call to get the parties for the role "HubEditor"
    Then I receive a HTTP "OK"
    And  I receive a party list with "tom", "jimmy" and "rounders"
    
    
Scenario: Successful for party type
	Given a partyType "dude" named "Tom" with uuid "tom" assigned to roles "HubEditor" and "CampaginEditor"
	Given a partyType "dude" named "Jimmy" with uuid "jimmy" assigned to roles "HubEditor" and "CampaginEditor"
	Given a partyType "brach" named "Rounders" with uuid "rounders" assigned to roles "HubEditor" and "CampaginEditor"
	When I call to get the parties "dude" for the role "HubEditor"
    Then I receive a HTTP "OK"
    And  I receive a party list with "tom" and "jimmy"
    
Scenario: Error role not existing
	When I call to get the parties for the role "NotExistingRole"
    Then I receive a HTTP "BAD_REQUEST"

Scenario: Error partyType not existing
	When I call to get the parties "notExistingParty" for the role "CatalogueEditor"
    Then I receive a HTTP "BAD_REQUEST"
    
Scenario: Error empty role
	When I call to get the parties for the role ""
    Then I receive a HTTP "NOT_FOUND"
    