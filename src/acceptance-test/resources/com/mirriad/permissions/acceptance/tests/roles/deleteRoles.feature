@Roles
Feature: Delete roles
URI Rest: /roles/{roleName}  DELETE

  As Business Manager
  In order to keep update the roles system 
  I'd like to delete roles
  
Scenario: Successful
	Given a role "ContentOwnerEditor"
	When I call to delete a role "ContentOwnerEditor"
    Then I receive a HTTP "OK"
    And  I dont get role "ContentOwnerEditor" in a later call
    
Scenario: Error role not found
	When I call to delete a role "NotExistingRole"
    Then I receive a HTTP "BAD_REQUEST"
    
Scenario: Error delete without role
	When I call to delete a role ""
    Then I receive a HTTP "METHOD_NOT_ALLOWED"
    