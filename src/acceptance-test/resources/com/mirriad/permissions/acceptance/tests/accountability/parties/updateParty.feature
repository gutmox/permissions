@Parties
Feature: Update a party
URI Rest: /parties/{partyType}  PUT
{
   "partyName": "Sophia",
   "partyUuid": "sophia_uuid"
}

  As Business Manager
  In order to get my data updated
  I'd like to update a group or user name
  
Scenario: Successful Update user by uuid
    Given a partyType "user" named "Silvio" with uuid "silvio_uuid"
	When I call to update a party "Silvio" with uuid "silvio_uuid" and party type "user"
    Then I receive a HTTP "OK"
    
Scenario: Successful group by uuid 
    Given a partyType "group" named "Lady Gaga Managers" with uuid "lgg_uuid"
	When I call to update a party "Lady Gaga Managers" with uuid "lgg_uuid" and party type "group"
    Then I receive a HTTP "OK"

Scenario: Error with Party Uuid not existing
	When I call to update a party "Whatever" with uuid "non_existing_uuid" and party type "user"
    Then I receive a HTTP "BAD_REQUEST"

        
Scenario: Error without PartyType
    When I call to update a party without any partyTppe
    Then I receive a HTTP "METHOD_NOT_ALLOWED"
