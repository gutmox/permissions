@PartyRoles
Feature: Get roles of party
URI Rest: /parties/{partyType}/{partyUuid}/roles  GET

  As Business Manager
  In order to know what people have access to what accions to what resouces 
  I'd like to obtain the roles for a certain user or group  
  
Scenario: Successful
	Given a partyType "user" named "Ronaldo" with uuid "ronaldo" assigned to roles "CatalogueEditor" and "CampaginEditor" 
	When I call to get the "user" "ronaldo" roles
    Then I receive a HTTP "OK"
    And I get the roles "CatalogueEditor" and "CampaginEditor"
    
Scenario: Successful Not roles
	Given a partyType "user" named "Cristian" with uuid "cristian" 
	When I call to get the "user" "cristian" roles
    Then I receive a HTTP "NOT_FOUND"
    
Scenario: Successful for role
	Given a partyType "user" named "Ronaldo" with uuid "ronaldo" assigned to roles "CatalogueEditor" and "CampaginEditor" 
	When I call to get the "user" "ronaldo" role "CatalogueEditor"
    Then I receive a HTTP "OK"
    And I get the role "CatalogueEditor"

Scenario: Error not existing party
	When I call to get the "user" "not_existing" roles
    Then I receive a HTTP "BAD_REQUEST"
    
Scenario: Error not existing partyType
	When I call to get the "apple" "ronaldo" roles
    Then I receive a HTTP "BAD_REQUEST"
    
 Scenario: Error not empty party
	When I call to get the "user" "" roles
    Then I receive a HTTP "NOT_FOUND"
    
Scenario: Error not empty party type
	When I call to get the "" "ronaldo" roles
    Then I receive a HTTP "NOT_FOUND"