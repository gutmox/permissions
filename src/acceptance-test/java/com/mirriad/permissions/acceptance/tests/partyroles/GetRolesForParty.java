package com.mirriad.permissions.acceptance.tests.partyroles;

import java.util.Collection;

import javax.inject.Inject;

import org.junit.Assert;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.accountability.parties.StoreParty;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.mirriad.permissions.acceptance.tests.roles.CreateRoles;
import com.mirriad.permissions.rest.dto.RoleDTO;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GetRolesForParty {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@Inject
	private StoreParty storeParty;

	@Inject
	private CreateRoles createRoles;

	@Inject
	private AssignParty2Roles assignParty2Roles;

	@Given("^a partyType \"([^\"]*)\" named \"([^\"]*)\" with uuid \"([^\"]*)\" assigned to roles \"([^\"]*)\" and \"([^\"]*)\"$")
	public void a_partyType_named_with_uuid_assigned_to_roles_and(String partyType, String partyName, String partyUuid, String role1, String role2) throws Throwable {
			storeParty.createParty(partyName, partyUuid, partyType);
			createRoles.createRole(role1, role1);
			createRoles.createRole(role2, role2);
			assignParty2Roles.assignRole2Party(partyType, partyUuid, role1);
			assignParty2Roles.assignRole2Party(partyType, partyUuid, role2);

	}

	@When("^I call to get the \"([^\"]*)\" \"([^\"]*)\" roles$")
	public void I_call_to_get_the_roles(String partyType, String partyUuid) throws Throwable {
			getRoleForParty(partyType, partyUuid, null);
	}

	@Then("^I get the roles \"([^\"]*)\" and \"([^\"]*)\"$")
	public void I_get_the_roles_and(String role1, String role2) throws Throwable {
			Collection<RoleDTO> entities = invoked.getEntity(new GenericType<Collection<RoleDTO>>() {});
			for (RoleDTO roleDTO : entities) {
				Assert.assertTrue(roleDTO.getName().equals(role1) || roleDTO.getName().equals(role2));
			}
	}

	@When("^I call to get the \"([^\"]*)\" \"([^\"]*)\" role \"([^\"]*)\"$")
	public void I_call_to_get_the_role(String partyType, String partyUuid, String role) throws Throwable {
			getRoleForParty(partyType, partyUuid, role);
	}

	@Then("^I get the role \"([^\"]*)\"$")
	public void I_get_the_role(String role) throws Throwable {
			RoleDTO entity = invoked.getEntity(new GenericType<RoleDTO>() {});
			Assert.assertTrue(entity.getName().equals(role));
	}

	/**
   * /parties/{partyType}/{partyUuid}/roles/{roleName}
   */
	protected void getRoleForParty(String partyType, String partyUuid, String roleName) {
		String url = buildUrl(partyType, partyUuid, roleName);
		invoked = restRequestSender.invoke(RestMethod.GET, url);
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}

	private String buildUrl(String partyType, String partyUuid, String roleName) {
		String url = URL.PARTIES;
		if (partyType != null){
			url += "/" + partyType;
		}
		if (partyUuid != null){
			url += "/" + partyUuid;
		}
		url += "/roles";
		if (roleName != null){
			url += "/" + roleName;
		}
		return url;
	}

	public ClientResponse getInvoked() {
		return invoked;
	}
}
