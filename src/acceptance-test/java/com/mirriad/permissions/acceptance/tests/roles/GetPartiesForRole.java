package com.mirriad.permissions.acceptance.tests.roles;

import java.util.Collection;

import javax.inject.Inject;

import org.junit.Assert;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.mirriad.permissions.rest.dto.PartyDTO;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GetPartiesForRole {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@When("^I call to get the parties for the role \"([^\"]*)\"$")
	public void I_call_to_get_the_parties_for_the_role(String role) throws Throwable {
		getPartiesForRole(role, null);
	}

	@Then("^I receive a party list with \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void I_receive_a_party_list_with_and_for_the_role(String partyUuid1, String partyUuid2, String partyUuid3) throws Throwable {
		Collection<PartyDTO> partyList = invoked.getEntity(new GenericType<Collection<PartyDTO>>() {});
		Assert.assertEquals(3, partyList.size());
		for (PartyDTO partyDTO : partyList) {
			Assert.assertTrue(partyUuid1.equals(partyDTO.getUuid())
					|| partyUuid2.equals(partyDTO.getUuid())
					|| partyUuid3.equals(partyDTO.getUuid())
						);
		}
	}

	@When("^I call to get the parties \"([^\"]*)\" for the role \"([^\"]*)\"$")
	public void I_call_to_get_the_parties_for_the_role(String partyType, String role) throws Throwable {
		getPartiesForRole(role, partyType);
	}

	@Then("^I receive a party list with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void I_receive_a_party_list_with_and_for_the_role(String partyUuid1, String partyUuid2) throws Throwable {
		Collection<PartyDTO> partyList = invoked.getEntity(new GenericType<Collection<PartyDTO>>() {});
		Assert.assertEquals(2, partyList.size());
		for (PartyDTO partyDTO : partyList) {
			Assert.assertTrue(partyUuid1.equals(partyDTO.getUuid())
					|| partyUuid2.equals(partyDTO.getUuid())
						);
		}
	}

	protected void getPartiesForRole(String roleName, Object partyType) {
		String url = URL.ROLES;
		if (roleName != null) {
			url += "/" + roleName;
		}
		url += "/parties";
		if (partyType != null) {
			url += "/" + partyType;
		}
		invoked = restRequestSender.invoke(RestMethod.GET, url);
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}
}
