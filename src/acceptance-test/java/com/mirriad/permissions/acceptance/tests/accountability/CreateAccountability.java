package com.mirriad.permissions.acceptance.tests.accountability;

import javax.inject.Inject;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.mirriad.permissions.rest.dto.AccountabilityDTO;
import com.mirriad.permissions.rest.dto.RelatedPartyDTO;
import com.sun.jersey.api.client.ClientResponse;

import cucumber.api.java.en.When;

public class CreateAccountability {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@When("^I call add a new relation \"([^\"]*)\" between \"([^\"]*)\" \"([^\"]*)\" and \"([^\"]*)\" \"([^\"]*)\"$")
	public void I_call_add_a_new_relation_between_and(String accountabilityType,
			String partyType, String partyUuid,
			String childPartyType, String childPartyUuid) throws Throwable {
		createAccountability(partyUuid, partyType, accountabilityType, childPartyType, childPartyUuid);
	}


	protected void createAccountability(String partyUuid, String partyType, String accountabilityType, String childPartyType, String childPartyUuid) {
		AccountabilityDTO accountabilityDTO = new AccountabilityDTO();
		accountabilityDTO.setAccountabilityType(accountabilityType);
		accountabilityDTO.setChildParty(new RelatedPartyDTO(childPartyType, childPartyUuid));
		String url = URL.PARTIES;
		if (partyType != null){
			url +=  "/" + partyType;
		}
		if (partyUuid != null){
			url +=  "/" + partyUuid;
		}
		url +=  "/relates";
		invoked = restRequestSender.invoke(RestMethod.POST, url, accountabilityDTO);
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}
}
