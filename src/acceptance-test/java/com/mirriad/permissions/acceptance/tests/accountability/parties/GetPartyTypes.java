package com.mirriad.permissions.acceptance.tests.accountability.parties;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.junit.Assert;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.mirriad.permissions.rest.dto.PartyTypeDTO;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GetPartyTypes {

	private RestRequestSender restRequestSender = new RestRequestSender();

	@Inject
	private StoreParty storeParty;

	@Inject
	private ResultVerfication resultVerfication;

	private ClientResponse invoked;

	@When("^I call to get partyTypes$")
	public void I_call_to_get_partyTypes() throws Throwable {
		retrievePartyType(null);
	}

	@Then("^I receive all the partyTypes:$")
	public void I_receive_all_the_partyTypes(DataTable partyTableTable) throws Throwable {
		List<String> asList = partyTableTable.asList(String.class);
		Collection<PartyTypeDTO> entities = invoked.getEntity(new GenericType<Collection<PartyTypeDTO>>() {});
		for (String partyTypeName : asList) {
			Assert.assertTrue(entities.contains(new PartyTypeDTO(partyTypeName)));
		}
	}

	@When("^I call to get partyTypes filtered by name \"([^\"]*)\"$")
	public void I_call_to_get_partyTypes_filtered_by_name(String partyTypeName)
			throws Throwable {
		retrievePartyType(partyTypeName);
	}

	@Then("^I receive all partyTypes stored with name \"([^\"]*)\"$")
	public void I_receive_all_partyTypes_stored_with_name(String partyTypeName)
			throws Throwable {
		Collection<PartyTypeDTO> entities = invoked
				.getEntity(new GenericType<Collection<PartyTypeDTO>>() {
				});
		for (PartyTypeDTO partyTypeDTO : entities) {
			Assert.assertEquals(partyTypeName, partyTypeDTO.getName());
		}
	}

	protected void retrievePartyType(String partyTypeName) {
		String url = URL.PARTIES;
		if (partyTypeName != null) {
			url += "?partyType=" + partyTypeName;
		}
		invoked = restRequestSender.invoke(RestMethod.GET, url);
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}
}
