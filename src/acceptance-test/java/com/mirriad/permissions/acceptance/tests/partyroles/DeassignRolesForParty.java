package com.mirriad.permissions.acceptance.tests.partyroles;

import java.util.Collection;

import javax.inject.Inject;

import org.junit.Assert;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.mirriad.permissions.rest.dto.RoleDTO;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DeassignRolesForParty {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@Inject
	private GetRolesForParty getRolesForParty;

	@When("^I call to deassign all the roles for the \"([^\"]*)\" \"([^\"]*)\"$")
	public void I_call_to_deassign_all_the_roles_for_the(String partyType, String partyUuid)
			throws Throwable {
		deassignRoleForParty(partyType, partyUuid, null);
	}

	@Then("^I don't get any rol for \"([^\"]*)\" \"([^\"]*)\"$")
	public void I_don_t_get_any_rol_for(String partyType, String partyUuid) throws Throwable {
		getRolesForParty.getRoleForParty(partyType, partyUuid, null);
		resultVerfication.I_receive_a_HTTP("NOT_FOUND");
	}

	@When("^I call to deassign all the role \"([^\"]*)\" for the \"([^\"]*)\" \"([^\"]*)\"$")
	public void I_call_to_deassign_all_the_role_for_the(String role, String partyType, String partyUuid) throws Throwable {
		deassignRoleForParty(partyType, partyUuid, role);
	}

	@Then("^I get the rol \"([^\"]*)\" for \"([^\"]*)\" \"([^\"]*)\"$")
	public void I_get_the_rol_for(String role, String partyType, String partyUuid) throws Throwable {
		getRolesForParty.getRoleForParty(partyType, partyUuid, null);
		resultVerfication.I_receive_a_HTTP("OK");
		Collection<RoleDTO> entities = getRolesForParty.getInvoked().getEntity(new GenericType<Collection<RoleDTO>>() {});
		Assert.assertEquals(1, entities.size());
		for (RoleDTO roleDTO : entities) {
			Assert.assertEquals(role, roleDTO.getName());
		}
	}

	/**
	 * /parties/{partyType}/{partyUuid}/roles/{roleName}
	 */
	protected void deassignRoleForParty(String partyType, String partyUuid, String roleName) {
		String url = buildUrl(partyType, partyUuid, roleName);
		invoked = restRequestSender.invoke(RestMethod.DELETE, url);
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}

	private String buildUrl(String partyType, String partyUuid, String roleName) {
		String url = URL.PARTIES;
		if (partyType != null) {
			url += "/" + partyType;
		}
		if (partyUuid != null) {
			url += "/" + partyUuid;
		}
		url += "/roles";
		if (roleName != null) {
			url += "/" + roleName;
		}
		return url;
	}
}
