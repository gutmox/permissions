package com.mirriad.permissions.acceptance.tests.roles;

import javax.inject.Inject;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.sun.jersey.api.client.ClientResponse;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DeleteRoles {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@Inject
	private GetRoles getRoles;

	@When("^I call to delete a role \"([^\"]*)\"$")
	public void I_call_to_delete_a_role(String roleName) throws Throwable {
		deleteRole(roleName);
	}

	@Then("^I dont get role \"([^\"]*)\" in a later call$")
	public void I_dont_get_role_in_a_later_call(String roleName) throws Throwable {
		getRoles.getRole(roleName);
		resultVerfication.I_receive_a_HTTP("NOT_FOUND");
	}

	protected void deleteRole(String roleName) {
		String url = URL.ROLES;
		if (roleName != null){
			url += "/" + roleName;
		}
		invoked = restRequestSender.invoke(RestMethod.DELETE, url);
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}
}
