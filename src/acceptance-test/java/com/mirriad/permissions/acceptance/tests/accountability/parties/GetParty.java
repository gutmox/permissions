package com.mirriad.permissions.acceptance.tests.accountability.parties;

import java.util.Collection;

import javax.inject.Inject;

import org.junit.Assert;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.mirriad.permissions.rest.dto.PartyDTO;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GetParty {

	private RestRequestSender restRequestSender = new RestRequestSender();

	@Inject
	private StoreParty storeParty;

	@Inject
	private ResultVerfication resultVerfication;

	private ClientResponse invoked;

	@Given("^a partyType \"([^\"]*)\" named \"([^\"]*)\" with uuid \"([^\"]*)\"$")
	public void a_partyType_named_with_uuid(String partyType, String partyName, String partyUuid) throws Throwable {
		storeParty.createParty(partyName, partyUuid, partyType);
	}

	@When("^I call to get a party with partyType \"([^\"]*)\" by uuid \"([^\"]*)\"$")
	public void I_call_to_get_a_party_with_partyType_by_uuid(String partyType, String uuid) throws Throwable {
		retrieveParty(null, uuid, partyType);
	}

	@Then("^I receive the user \"([^\"]*)\" with uuid \"([^\"]*)\"$")
	public void I_receive_the_user_with_uuid(String userName, String userUUid) throws Throwable {
		PartyDTO party  = invoked.getEntity(new GenericType<PartyDTO>() {});
		Assert.assertEquals(userName, party.getName());
		Assert.assertEquals(userUUid, party.getUuid());
	}

	@When("^I call to get a party with partyType \"([^\"]*)\" by name \"([^\"]*)\"$")
	public void I_call_to_get_a_party_with_partyType_by_name(String partyType, String partyName) throws Throwable {
		retrieveParty(partyName, null, partyType);
	}

	@When("^I call to get all parties with partyType \"([^\"]*)\"$")
	public void I_call_to_get_all_parties_with_partyType(String partyType) throws Throwable {
		retrieveParty(null, null, partyType);
	}

	@Then("^I receive a complete list$")
	public void I_receive_a_complete_list() throws Throwable {
		Collection<PartyDTO> entities = invoked.getEntity(new GenericType<Collection<PartyDTO>>() {});
		Assert.assertNotNull(entities);
		Assert.assertTrue(entities.size() > 0);
	}

	@Then("^I receive a list of users with name \"([^\"]*)\"$")
	public void I_receive_a_list_of_users_with_name(String name) throws Throwable {
		Collection<PartyDTO> entities = invoked.getEntity(new GenericType<Collection<PartyDTO>>() {});
		for (PartyDTO partyDTO : entities) {
			Assert.assertEquals(name, partyDTO.getName());
		}
	}

	protected void retrieveParty(String partyName, String partyUuid, String partyType) {
		String url = URL.PARTIES;
		if (partyType != null){
			url += "/" + partyType;
		}
		if (partyUuid != null){
			url += "/" + partyUuid;
		}
		if (partyName != null){
			url += "?partyName=" + partyName;
		}
		invoked = restRequestSender.invoke(RestMethod.GET, url);
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}
}
