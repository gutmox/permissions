package com.mirriad.permissions.acceptance.tests.permissions;

import java.util.Collection;

import javax.inject.Inject;

import org.junit.Assert;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.mirriad.permissions.rest.dto.PermissionDTO;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GetPermissions {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@Inject
	private CreatePermissions createPermissions;

	@Given("^a permission \"([^\"]*)\" \"([^\"]*)\"$")
	public void a_permission(String resource, String action) throws Throwable {
		createPermissions.createPermission(resource, action);
	}

	@When("^I call to get all the  permissions$")
	public void I_call_to_get_all_the_permissions() throws Throwable {
		getPermissions(null, null);
	}

	@Then("^I get one permission \"([^\"]*)\" \"([^\"]*)\"$")
	public void I_get_one_permission(String resource, String action) throws Throwable {
		PermissionDTO permission = invoked.getEntity(new GenericType<PermissionDTO>() {});
		Assert.assertEquals(action, permission.getAction());
		Assert.assertEquals(resource, permission.getResource());
	}

	@Then("^I get a list with permissions for \"([^\"]*)\"$")
	public void I_get_a_list_with_permissions_for(String resource) throws Throwable {
		Collection<PermissionDTO> permissions = invoked.getEntity(new GenericType<Collection<PermissionDTO>>() {});
		for (PermissionDTO permissionDTO : permissions) {
			Assert.assertEquals(resource, permissionDTO.getResource());
		}
	}

	@Then("^I get a list with permissions$")
	public void I_get_a_list_with_permissions() throws Throwable {
			Collection<PermissionDTO> permissions = invoked.getEntity(new GenericType<Collection<PermissionDTO>>() {});
			Assert.assertTrue(permissions.size() > 0);
	}

	@When("^I call to get permissions for resource \"([^\"]*)\"$")
	public void I_call_to_get_permissions_for_resource(String resource) throws Throwable {
		getPermissions(resource, null);
	}

	@When("^I call to get permissions for resource \"([^\"]*)\" and action \"([^\"]*)\"$")
	public void I_call_to_get_permissions_for_resource_and_action(String resource, String action)
			throws Throwable {
		getPermissions(resource, action);
	}

	public void getPermissions(String resource, String action) {
		invoked = restRequestSender.invoke(RestMethod.GET, buildUrl(resource, action));
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}

	private String buildUrl(String resource, String action) {
		String url = URL.PERMISSIONS;
		if (resource != null){
			url += "/" + resource;
		}
		if (action != null){
			url += "/" + action;
		}
		return url;
	}

	public ClientResponse getInvoked() {
		return invoked;
	}
}
