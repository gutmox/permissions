package com.mirriad.permissions.acceptance.tests;

import org.junit.Assert;

import com.sun.jersey.api.client.ClientResponse.Status;

import cucumber.api.java.en.Then;

public class ResultVerfication {

	Status status;


	@Then("^I receive a HTTP \"([^\"]*)\"$")
	public void I_receive_a_HTTP(String httpStatus) throws Throwable {
		Assert.assertEquals(httpStatus, getStatus().name());
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
