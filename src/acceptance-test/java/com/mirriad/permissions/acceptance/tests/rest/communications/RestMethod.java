package com.mirriad.permissions.acceptance.tests.rest.communications;

public enum RestMethod {

	GET,
	POST,
	DELETE,
	PUT
}
