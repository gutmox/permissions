package com.mirriad.permissions.acceptance.tests.accountability;

import java.util.Collection;

import javax.inject.Inject;

import org.junit.Assert;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.mirriad.permissions.rest.dto.AccountabilityDTO;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GetAccountabilities {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@Inject
	private CreateAccountability createAccountability;

	@Given("^an accountability \"([^\"]*)\" between \"([^\"]*)\" \"([^\"]*)\" and \"([^\"]*)\" \"([^\"]*)\"$")
	public void an_accountability_between_and(String accountabilityType, String partyType, String partyUuid, String childPartyType, String childPartyUuid) throws Throwable {
		createAccountability.createAccountability(partyUuid, partyType, accountabilityType, childPartyType, childPartyUuid);
	}

	@When("^I call to get the relations of \"([^\"]*)\" \"([^\"]*)\"$")
	public void I_call_to_get_the_relations_of(String partyType, String partyUuid) throws Throwable {
		getAccountabilities(partyUuid, partyType, null, null, null);
	}

	@Then("^I get the accountability \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void I_get_the_accountability(String accountabilityType, String childPartyType, String childPartyUuid) throws Throwable {
			Collection<AccountabilityDTO> entities = invoked.getEntity(new GenericType<Collection<AccountabilityDTO>>() {});
			for (AccountabilityDTO accountabilityDTO : entities) {
				Assert.assertEquals(accountabilityType, accountabilityDTO.getAccountabilityType());
				Assert.assertEquals(childPartyType, accountabilityDTO.getChildParty().getPartyType().getName());
				Assert.assertEquals(childPartyUuid, accountabilityDTO.getChildParty().getUuid());
			}
	}

	@When("^I call to get the relations of \"([^\"]*)\" \"([^\"]*)\" with accountability type \"([^\"]*)\"$")
	public void I_call_to_get_the_relations_of_with_accountability_type(String partyType, String partyUuid, String accountabilityType) throws Throwable {
		getAccountabilities(partyUuid, partyType, accountabilityType, null, null);
	}

	@When("^I call to get the relations of \"([^\"]*)\" \"([^\"]*)\" with accountability type \"([^\"]*)\" and other leg \"([^\"]*)\" \"([^\"]*)\"$")
	public void I_call_to_get_the_relations_of_with_accountability_type_and_other_leg(String partyType, String partyUuid,
			String accountabilityType, String childPartyType, String childPartyUuid) throws Throwable {
		getAccountabilities(partyUuid, partyType, accountabilityType, childPartyType, childPartyUuid);
	}

	protected void getAccountabilities(String partyUuid, String partyType, String accountabilityType, String childPartyType, String childPartyUuid) {
		String url = URL.PARTIES;
		if (partyType != null){
			url +=  "/" + partyType;
		}
		if (partyUuid != null){
			url +=  "/" + partyUuid;
		}
		url +=  "/relates";
		if (accountabilityType != null){
			url +=  "/" + accountabilityType;
		}
		if (childPartyType != null){
			url +=  "/" + childPartyType;
		}
		if (childPartyUuid != null){
			url +=  "/" + childPartyUuid;
		}
		invoked = restRequestSender.invoke(RestMethod.GET, url);
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}
}
