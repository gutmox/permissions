package com.mirriad.permissions.acceptance.tests.entities;

import java.util.Collection;

import javax.inject.Inject;

import org.junit.Assert;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.mirriad.permissions.rest.dto.EntityTypeDTO;
import com.mirriad.permissions.rest.dto.SimpleEntityDTO;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GetEntities {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@When("^I call to get all entities types$")
	public void I_call_to_get_all_entities_types() throws Throwable {
		getEntities(null, null, null);
	}

	@Then("^I get a list of entities in which I one element \"([^\"]*)\" type;$")
	public void I_get_a_list_of_entities_in_which_I_one_element_type(String entityTypeName)
			throws Throwable {
		Collection<EntityTypeDTO> result = invoked
				.getEntity(new GenericType<Collection<EntityTypeDTO>>() {
				});
		boolean notFound = true;
		for (EntityTypeDTO entityType : result) {
			notFound = notFound && entityTypeName.equals(entityType.getName());
		}
		Assert.assertFalse(notFound);
	}

	@Then("^I get a list of entities in which I one element \"([^\"]*)\";$")
	public void I_get_a_list_of_entities_in_which_I_one_element_(String entityUuid)
			throws Throwable {
		Collection<SimpleEntityDTO> result = invoked
				.getEntity(new GenericType<Collection<SimpleEntityDTO>>() {
				});
		Assert.assertEquals(1, result.size());
		Assert.assertEquals(entityUuid, result.iterator().next().getUuid());
	}

	@When("^I call to get all entities of type \"([^\"]*)\"$")
	public void I_call_to_get_all_entities_of_type(String entityType) throws Throwable {
		getEntities(entityType, null, null);
	}

	@When("^I call to get an entity of type \"([^\"]*)\" and uuid \"([^\"]*)\"$")
	public void I_call_to_get_an_entity_of_type_and_uuid(String entityType,
			String entityUuid) throws Throwable {
		getEntities(entityType, entityUuid, null);
	}

	@Then("^I get an entity which of type \"([^\"]*)\" and uuid \"([^\"]*)\"$")
	public void I_get_an_entity_which_of_type_and_uuid(String entityType, String entityUuid)
			throws Throwable {
		SimpleEntityDTO result = invoked.getEntity(new GenericType<SimpleEntityDTO>() {
		});
		Assert.assertEquals(entityUuid, result.getUuid());
	}

	@When("^I call to get all entities types filtered by \"([^\"]*)\"$")
	public void I_call_to_get_all_entities_types_filtered_by(String name) throws Throwable {
		getEntities(null, null, name);
	}

	@Then("^I get a list of entities with only one element \"([^\"]*)\" type;$")
	public void I_get_a_list_of_entities_with_only_one_element_type(String entityType)
			throws Throwable {
		Collection<EntityTypeDTO> result = invoked
				.getEntity(new GenericType<Collection<EntityTypeDTO>>() {
				});
		Assert.assertEquals(1, result.size());
		Assert.assertEquals(entityType, result.iterator().next().getName());
	}

	@When("^I call to get all entities of type \"([^\"]*)\" filtered by name \"([^\"]*)\"$")
	public void I_call_to_get_all_entities_of_type_filtered_by_name(String entityType,
			String name) throws Throwable {
		getEntities(entityType, null, name);
	}

	public void getEntities(String entityType, String uuid, String name) {
		String url = URL.ENTITIES;
		if (entityType != null)
			url += "/" + entityType;
		if (uuid != null)
			url += "/" + uuid;
		if (name != null)
			url += "?name=" + name;
		invoked = restRequestSender.invoke(RestMethod.GET, url);
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}
}
