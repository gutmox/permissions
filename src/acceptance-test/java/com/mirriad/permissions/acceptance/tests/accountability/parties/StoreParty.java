package com.mirriad.permissions.acceptance.tests.accountability.parties;

import javax.inject.Inject;

import org.junit.Assert;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.support.SimpleCacheManager;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.mirriad.permissions.rest.dto.PartyDTO;
import com.mirriad.permissions.rest.dto.SimplePartyDTO;
import com.sun.jersey.api.client.ClientResponse;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StoreParty {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@Inject
	private SimpleCacheManager cacheManager;

	@When("^I call to add new party \"([^\"]*)\" with uuid \"([^\"]*)\" and party type \"([^\"]*)\"$")
	public void I_call_to_add_new_party_with_uuid_and_party_type(String partyName, String partyUuid, String partyType) throws Throwable {
		createParty(partyName, partyUuid, partyType);
	}

	@Then("^I get teh same uuid \"([^\"]*)\"$")
	public void I_get_teh_same_uuid(String expectedUuid) throws Throwable {
		SimplePartyDTO returnedEntity = invoked.getEntity(SimplePartyDTO.class);
		Assert.assertEquals(expectedUuid, returnedEntity.getUuid());
	}

	@When("^I call to add new party \"([^\"]*)\" without uuid and party type \"([^\"]*)\"$")
	public void I_call_to_add_new_party_without_uuid_and_party_type(String partyName, String partyType) throws Throwable {
		createParty(partyName, null, partyType);
	}

	@Then("^I get a generated uuid$")
	public void I_get_a_generated_uuid() throws Throwable {
		SimplePartyDTO returnedEntity = invoked.getEntity(SimplePartyDTO.class);
		Assert.assertNotNull(returnedEntity.getUuid());
	}

	@When("^I call to add new party without any name nor uuid and party type \"([^\"]*)\"$")
	public void I_call_to_add_new_party_without_any_name_nor_uuid_and_party_type(String partyType) throws Throwable {
	   createParty(null, null, partyType);
	}

	@When("^I call to add new party without any name, uuid \"([^\"]*)\" and party type \"([^\"]*)\"$")
	public void I_call_to_add_new_party_without_any_name_uuid_and_party_type(String uuid, String partyType) throws Throwable {
		 createParty(null, uuid, partyType);
	}

	@When("^I call to add new party without any partyTppe$")
	public void I_call_to_add_new_party_without_any_partyTppe() throws Throwable {
		createParty(null, null, "");
	}

	public void createParty(String partyName, String partyUuid, String partyType) {
		if (isNotCached(partyName, partyUuid, partyType)){
			PartyDTO partyDTO = new PartyDTO();
			partyDTO.setName(partyName);
			partyDTO.setUuid(partyUuid);
			invoked = restRequestSender.invoke(RestMethod.POST, URL.PARTIES + "/" + partyType, partyDTO);
			resultVerfication.setStatus(invoked.getClientResponseStatus());
		}
	}

	private boolean isNotCached(String partyName, String partyUuid, String partyType) {
		Cache cache = cacheManager.getCache("storedParties");
		ValueWrapper cached = cache.get(new PartyCache(partyName, partyUuid, partyType).toString());
		boolean isNotCached = (cached == null);
		cache.put(new PartyCache(partyName, partyUuid, partyType).toString(), true);
		return isNotCached;
	}

	private class PartyCache{
		private String partyName;
		private String partyUuid;
		private String partyType;
		private PartyCache(String partyName, String partyUuid, String partyType) {
			super();
			this.partyName = partyName;
			this.partyUuid = partyUuid;
			this.partyType = partyType;
		}
		@Override
		public String toString() {
			return "PartyCache [partyName=" + partyName + ", partyUuid=" + partyUuid
					+ ", partyType=" + partyType + "]";
		}
	}
}
