package com.mirriad.permissions.acceptance.tests.permissions;

import javax.inject.Inject;

import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.support.SimpleCacheManager;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.mirriad.permissions.rest.dto.PermissionDTO;
import com.sun.jersey.api.client.ClientResponse;

import cucumber.api.java.en.When;

public class CreatePermissions {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@Inject
	private SimpleCacheManager cacheManager;

	@When("^I call to create the  permission \"([^\"]*)\" \"([^\"]*)\"$")
	public void I_call_to_create_the_permission(String resource, String action)
			throws Throwable {
		createPermission(resource, action);
	}

	@When("^I call to create the  permission \"([^\"]*)\" without Resource$")
	public void I_call_to_create_the_permission_without_Resource(String action)
			throws Throwable {
		createPermission(null, action);
	}

	@When("^I call to create the  permission \"([^\"]*)\" without Action$")
	public void I_call_to_create_the_permission_without_Action(String resource)
			throws Throwable {
		createPermission(resource, null);
	}

	public void createPermission(String resource, String action) {
		PermissionDTO permission = new PermissionDTO(resource, action);
		if (isNotCached(permission)) {
			invoked = restRequestSender.invoke(RestMethod.POST, URL.PERMISSIONS, permission);
			resultVerfication.setStatus(invoked.getClientResponseStatus());
		}
	}

	private boolean isNotCached(PermissionDTO permission) {
		if (permission == null
				|| permission.getResource() == null
				|| permission.getAction() == null)
			return true;
		Cache cache = cacheManager.getCache("storedPermissions");
		ValueWrapper cached = cache.get(permission.toString());
		boolean isNotCached = (cached == null);
		cache.put(permission.toString(), true);
		return isNotCached;
	}
}
