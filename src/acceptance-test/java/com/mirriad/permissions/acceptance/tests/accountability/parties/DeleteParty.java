package com.mirriad.permissions.acceptance.tests.accountability.parties;

import javax.inject.Inject;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.sun.jersey.api.client.ClientResponse;

import cucumber.api.java.en.When;

public class DeleteParty {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@When("^I call to delete a party with uuid \"([^\"]*)\" and party type \"([^\"]*)\"$")
	public void I_call_to_delete_a_party_with_uuid_and_party_type(String partyUuid, String partyType) throws Throwable {
		deleteParty(partyUuid, partyType);
	}

	@When("^I call to delete a party without any partyTppe$")
	public void I_call_to_delete_a_party_without_any_partyTppe() throws Throwable {
		deleteParty(null, null);
	}

	protected void deleteParty(String partyUuid, String partyType) {
		String url = URL.PARTIES;
		if (partyType != null){
			url += "/" + partyType + "/" + partyUuid;
		}
		invoked = restRequestSender.invoke(RestMethod.DELETE, url);
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}
}
