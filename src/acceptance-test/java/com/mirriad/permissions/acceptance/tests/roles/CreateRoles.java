package com.mirriad.permissions.acceptance.tests.roles;

import javax.inject.Inject;

import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.support.SimpleCacheManager;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.mirriad.permissions.rest.dto.RoleDTO;
import com.sun.jersey.api.client.ClientResponse;

import cucumber.api.java.en.When;

public class CreateRoles {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@Inject
	private SimpleCacheManager cacheManager;


	@When("^I call to add new role \"([^\"]*)\" with description \"([^\"]*)\"$")
	public void I_call_to_add_new_role_with_description(String roleName, String roleDescription) throws Throwable {
			createRole(roleName, roleDescription);
	}

	@When("^I call to add new role \"([^\"]*)\" without description$")
	public void I_call_to_add_new_role_without_description(String roleName) throws Throwable {
			createRole(roleName, null);
	}

	@When("^I call to add new role without name$")
	public void I_call_to_add_new_role_without_name() throws Throwable {
		 	createRole(null, null);
	}

	public void createRole(String roleName, String roleDescription) {
		if (isNotCached(roleName)){
			invoked = restRequestSender.invoke(RestMethod.POST, URL.ROLES, new RoleDTO(roleName, roleDescription));
			resultVerfication.setStatus(invoked.getClientResponseStatus());
		}
	}

	private boolean isNotCached(String roleName) {
		if (roleName == null)
			return true;
		Cache cache = cacheManager.getCache("storedRoles");
		ValueWrapper cached = cache.get(roleName);
		boolean isNotCached = (cached == null);
		cache.put(roleName, true);
		return isNotCached;
	}
}
