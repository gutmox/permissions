package com.mirriad.permissions.acceptance.tests.entities;

import javax.inject.Inject;

import org.junit.Assert;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.support.SimpleCacheManager;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.mirriad.permissions.rest.dto.EntityDTO;
import com.mirriad.permissions.rest.dto.EntityUuidDTO;
import com.sun.jersey.api.client.ClientResponse;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StoreEntity {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@Inject
	private SimpleCacheManager cacheManager;


	@When("^I call to add new entity \"([^\"]*)\" with uuid \"([^\"]*)\" and entity type \"([^\"]*)\"$")
	public void I_call_to_add_new_entity_with_uuid_and_entity_type(String name,
			String uuid, String entityType) throws Throwable {
		createEntity(entityType, name, uuid);
	}

	@Then("^I get the same entity uuid \"([^\"]*)\"$")
	public void I_get_the_same_entity_uuid(String uuid) throws Throwable {
		EntityUuidDTO returnedEntity = invoked.getEntity(EntityUuidDTO.class);
		Assert.assertEquals(uuid, returnedEntity.getUuid());
	}

	@When("^I call to add new entity \"([^\"]*)\" without uuid and entity type \"([^\"]*)\"$")
	public void I_call_to_add_new_entity_without_uuid_and_entity_type(String name,
			String entityType) throws Throwable {
		createEntity(entityType, name, null);
	}

	@Then("^I get a generated entity uuid$")
	public void I_get_a_generated_entity_uuid() throws Throwable {
		EntityUuidDTO returnedEntity = invoked.getEntity(EntityUuidDTO.class);
		Assert.assertNotNull(returnedEntity.getUuid());
	}

	@When("^I call to add new entity without neither name nor uuid and entity type \"([^\"]*)\"$")
	public void I_call_to_add_new_entity_without_neither_name_nor_uuid_and_entity_type(
			String entityType) throws Throwable {
		createEntity(entityType, null, null);
	}

	@When("^I call to add new entity without name but with uuid \"([^\"]*)\" and entity type \"([^\"]*)\"$")
	public void I_call_to_add_new_entity_without_name_but_with_uuid_and_entity_type(
			String uuid, String entityType) throws Throwable {
		createEntity(entityType, null, uuid);
	}

	@When("^I call to add new entity without any partyTppe$")
	public void I_call_to_add_new_entity_without_any_partyTppe() throws Throwable {
		createEntity(null, null, null);
	}

	public void createEntity(String entityType, String name, String uuid) {
		if (isNotCached(entityType, name, uuid)){
			String url = URL.ENTITIES;
			if (entityType != null)
				url += "/" + entityType;
			invoked = restRequestSender.invoke(RestMethod.POST, url,
					new EntityDTO(uuid, name));
			resultVerfication.setStatus(invoked.getClientResponseStatus());
		}
	}
	private boolean isNotCached(String entityType, String name, String uuid) {
		Cache cache = cacheManager.getCache("storedEntities");
		ValueWrapper cached = cache.get(new EntityCache(entityType, name, uuid).toString());
		boolean isNotCached = (cached == null);
		cache.put(new EntityCache(entityType, name, uuid).toString(), true);
		return isNotCached;
	}

	private class EntityCache{
		private String entityType;
		private String name;
		private String uuid;
		private EntityCache(String entityType, String name, String uuid) {
			super();
			this.entityType = entityType;
			this.name = name;
			this.uuid = uuid;
		}
		@Override
		public String toString() {
			return "EntityCache [entityType=" + entityType + ", name=" + name + ", uuid=" + uuid
					+ "]";
		}
	}
}
