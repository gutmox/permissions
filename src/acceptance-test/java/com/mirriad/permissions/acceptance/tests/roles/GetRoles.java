package com.mirriad.permissions.acceptance.tests.roles;

import java.util.Collection;

import javax.inject.Inject;

import org.junit.Assert;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.mirriad.permissions.rest.dto.RoleDTO;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GetRoles {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@Inject
	private CreateRoles createRoles;

	@Given("^a role \"([^\"]*)\"$")
	public void a_role(String roleName) throws Throwable {
		createRoles.createRole(roleName, null);
	}

	@When("^I call to get a role \"([^\"]*)\"$")
	public void I_call_to_get_a_role(String roleName) throws Throwable {
		getRole(roleName);
	}

	@Then("^I receive a single role \"([^\"]*)\"$")
	public void I_receive_a_single_role(String roleName) throws Throwable {
		RoleDTO role = invoked.getEntity(new GenericType<RoleDTO>() {
		});
		Assert.assertEquals(roleName, role.getName());
	}

	@When("^I call to get all roles$")
	public void I_call_to_get_all_roles() throws Throwable {
		getRole(null);
	}

	@Then("^I receive a role list$")
	public void I_receive_a_role_list() throws Throwable {
		Collection<RoleDTO> entities = invoked
				.getEntity(new GenericType<Collection<RoleDTO>>() {
				});
		Assert.assertNotNull(entities);
		Assert.assertTrue(entities.size() > 1);
	}

	protected void getRole(String roleName) {
		String url = URL.ROLES;
		if (roleName != null) {
			url += "/" + roleName;
		}
		invoked = restRequestSender.invoke(RestMethod.GET, url);
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}

	public ClientResponse getInvoked() {
		return invoked;
	}
}
