package com.mirriad.permissions.acceptance.tests.entities;

import javax.inject.Inject;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.sun.jersey.api.client.ClientResponse;

import cucumber.api.java.en.When;

public class DeleteEntities {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@When("^I call to delete all entities types\"([^\"]*)\"$")
	public void I_call_to_delete_all_entities_types(String entityType) throws Throwable {
		deleteEntities(entityType, null);
	}

	@When("^I call to delete thr entity \"([^\"]*)\" of type \"([^\"]*)\"$")
	public void I_call_to_delete_thr_entity_of_type(String uuid, String entityType)
			throws Throwable {
		deleteEntities(entityType, uuid);
	}

	public void deleteEntities(String entityType, String uuid) {
		String url = URL.ENTITIES;
		if (entityType != null)
			url += "/" + entityType;
		if (uuid != null)
			url += "/" + uuid;
		invoked = restRequestSender.invoke(RestMethod.DELETE, url);
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}
}
