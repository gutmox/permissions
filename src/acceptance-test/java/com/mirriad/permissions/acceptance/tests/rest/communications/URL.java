package com.mirriad.permissions.acceptance.tests.rest.communications;

public interface URL {
	String PARTIES = "/parties";
	String ROLES = "/roles";
	String PERMISSIONS = "/permissions";
	String ENTITIES = "/entities";
}
