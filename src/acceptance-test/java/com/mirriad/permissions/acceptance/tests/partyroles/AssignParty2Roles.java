package com.mirriad.permissions.acceptance.tests.partyroles;

import javax.inject.Inject;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.sun.jersey.api.client.ClientResponse;

import cucumber.api.java.en.When;

public class AssignParty2Roles {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@When("^I call to assign the \"([^\"]*)\" \"([^\"]*)\" to the role \"([^\"]*)\"$")
	public void I_call_to_assign_the_to_the_role(String partyType, String partyUuid, String roleName) throws Throwable {
		assignRole2Party(partyType, partyUuid, roleName);
	}


  /**
   * /parties/{partyType}/{partyUuid}/roles/{roleName}
   */
	protected void assignRole2Party(String partyType, String partyUuid, String roleName) {
		String url = buildUrl(partyType, partyUuid, roleName);
		invoked = restRequestSender.invoke(RestMethod.PUT, url);
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}

	private String buildUrl(String partyType, String partyUuid, String roleName) {
		String url = URL.PARTIES;
		if (partyType != null){
			url += "/" + partyType;
		}
		if (partyUuid != null){
			url += "/" + partyUuid;
		}
		if (roleName != null){
			url += "/roles/" + roleName;
		}
		return url;
	}
}
