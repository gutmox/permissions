package com.mirriad.permissions.acceptance.tests.accountability;

import javax.inject.Inject;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.sun.jersey.api.client.ClientResponse;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DeleteAccountability {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@Inject
	private GetAccountabilities getAccountabilities;

	@When("^I call to delete the relations of \"([^\"]*)\" \"([^\"]*)\"$")
	public void I_call_to_delete_the_relations_of(String partyType, String partyUuid)
			throws Throwable {
		deleteAccountabilities(partyUuid, partyType, null, null, null);
	}

	@Then("^I don't get an accountability \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" for party \"([^\"]*)\" \"([^\"]*)\"$")
	public void I_don_t_get_an_accountability_for_party(String accountabilityType, String childPartyType,
			String childPartyUuid, String partyType, String partyUuid) throws Throwable {
		getAccountabilities.getAccountabilities(partyUuid, partyType, accountabilityType, childPartyType, childPartyUuid);
		resultVerfication.I_receive_a_HTTP("NOT_FOUND");
	}

	@When("^I call to delete the \"([^\"]*)\" relations of \"([^\"]*)\" \"([^\"]*)\"$")
	public void I_call_to_delete_the_relations_of(String accountabilityType, String partyType, String partyUuid)
			throws Throwable {
		deleteAccountabilities(partyUuid, partyType, accountabilityType, null, null);
	}

	@When("^I call to delete the \"([^\"]*)\" relations of \"([^\"]*)\" \"([^\"]*)\" on \"([^\"]*)\" \"([^\"]*)\"$")
	public void I_call_to_delete_the_relations_of_on(String accountabilityType, String partyType, String partyUuid,
			String childPartyType, String childPartyUuid) throws Throwable {
		deleteAccountabilities(partyUuid, partyType, accountabilityType, childPartyType, childPartyUuid);
	}

	protected void deleteAccountabilities(String partyUuid, String partyType, String accountabilityType, String childPartyType, String childPartyUuid) {
		String url = URL.PARTIES;
		if (partyType != null){
			url +=  "/" + partyType;
		}
		if (partyUuid != null){
			url +=  "/" + partyUuid;
		}
		url +=  "/relates";
		if (accountabilityType != null){
			url +=  "/" + accountabilityType;
		}
		if (childPartyType != null){
			url +=  "/" + childPartyType;
		}
		if (childPartyUuid != null){
			url +=  "/" + childPartyUuid;
		}
		invoked = restRequestSender.invoke(RestMethod.DELETE, url);
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}
}
