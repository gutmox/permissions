package com.mirriad.permissions.acceptance.tests.permissions;

import javax.inject.Inject;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.sun.jersey.api.client.ClientResponse;

import cucumber.api.java.en.When;

public class DeletePermissions {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@Inject
	private GetPermissions getPermissions;


	@When("^I call to delete the resource \"([^\"]*)\"$")
	public void I_call_to_delete_the_resource(String resource) throws Throwable {
		deletePermissions(resource, null);
	}

	@When("^I call to delete the permission \"([^\"]*)\" \"([^\"]*)\"$")
	public void I_call_to_delete_the_permission(String resource, String action) throws Throwable {
		deletePermissions(resource, action);
	}

	public void deletePermissions(String resource, String action) {
		invoked = restRequestSender.invoke(RestMethod.DELETE, buildUrl(resource, action));
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}

	private String buildUrl(String resource, String action) {
		String url = URL.PERMISSIONS;
		if (resource != null){
			url += "/" + resource;
		}
		if (action != null){
			url += "/" + action;
		}
		return url;
	}
}
