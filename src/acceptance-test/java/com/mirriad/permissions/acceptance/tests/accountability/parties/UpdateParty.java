package com.mirriad.permissions.acceptance.tests.accountability.parties;

import javax.inject.Inject;

import com.mirriad.permissions.acceptance.tests.ResultVerfication;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestMethod;
import com.mirriad.permissions.acceptance.tests.rest.communications.RestRequestSender;
import com.mirriad.permissions.acceptance.tests.rest.communications.URL;
import com.mirriad.permissions.rest.dto.PartyDTO;
import com.sun.jersey.api.client.ClientResponse;

import cucumber.api.java.en.When;

public class UpdateParty {

	private RestRequestSender restRequestSender = new RestRequestSender();

	private ClientResponse invoked;

	@Inject
	private ResultVerfication resultVerfication;

	@When("^I call to update a party \"([^\"]*)\" with uuid \"([^\"]*)\" and party type \"([^\"]*)\"$")
	public void I_call_to_update_a_party_with_uuid_and_party_type(String partyName, String partyUuid, String partyType) throws Throwable {
			updateParty(partyName, partyUuid, partyType);
	}


	@When("^I call to update a party without any partyTppe$")
	public void I_call_to_update_a_party_without_any_partyTppe() throws Throwable {
			updateParty(null,null, "");
	}

	protected void updateParty(String partyName, String partyUuid, String partyType) {
		PartyDTO partyDTO = new PartyDTO();
		partyDTO.setName(partyName);
		partyDTO.setUuid(partyUuid);
		invoked = restRequestSender.invoke(RestMethod.PUT, URL.PARTIES + "/" + partyType, partyDTO);
		resultVerfication.setStatus(invoked.getClientResponseStatus());
	}

}
