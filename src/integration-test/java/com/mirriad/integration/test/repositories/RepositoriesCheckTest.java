package com.mirriad.integration.test.repositories;

import java.util.UUID;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mirriad.permissions.dao.api.AccountabilityRepository;
import com.mirriad.permissions.dao.api.ActionRepository;
import com.mirriad.permissions.dao.api.EntityRepository;
import com.mirriad.permissions.dao.api.EntityTypeRepository;
import com.mirriad.permissions.dao.api.PartyRepository;
import com.mirriad.permissions.dao.api.PartyRolesRepository;
import com.mirriad.permissions.dao.api.PartyTypeRepository;
import com.mirriad.permissions.dao.api.PermissionsRepository;
import com.mirriad.permissions.dao.api.ResourceRepository;
import com.mirriad.permissions.dao.api.RoleRepository;
import com.mirriad.permissions.domain.Accountability;
import com.mirriad.permissions.domain.Action;
import com.mirriad.permissions.domain.Entity;
import com.mirriad.permissions.domain.EntityType;
import com.mirriad.permissions.domain.Party;
import com.mirriad.permissions.domain.PartyRole;
import com.mirriad.permissions.domain.PartyType;
import com.mirriad.permissions.domain.Permission;
import com.mirriad.permissions.domain.Resource;
import com.mirriad.permissions.domain.Role;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext.xml")
public class RepositoriesCheckTest {

	@Inject
	private AccountabilityRepository accountabilityRepository;

	@Inject
	private ActionRepository actionRepository;

	@Inject
	private PartyRepository partyRepository;

	@Inject
	private PartyRolesRepository partyRolesRepository;

	@Inject
	private PartyTypeRepository partyTypeRepository;

	@Inject
	private PermissionsRepository permissionsRepository;

	@Inject
	private ResourceRepository resourceRepository;

	@Inject
	private RoleRepository roleRepository;

	@Inject
	private EntityRepository entityRepository;

	@Inject
	private EntityTypeRepository entityTypeRepository;

	@Test
	public void shouldConnectToAccountability() {
		EndResult<Accountability> findAll = accountabilityRepository.findAll();
		Assert.assertNotNull(findAll);
	}

	@Test
	public void shouldConnectToAction() {
		EndResult<Action> findAll = actionRepository.findAll();
		Assert.assertNotNull(findAll);
	}

	@Test
	public void shouldConnectToParty() {
		EndResult<Party> findAll = partyRepository.findAll();
		Assert.assertNotNull(findAll);
	}

	@Test
	public void shouldConnectToPartyRolesRepository() {
		EndResult<PartyRole> findAll = partyRolesRepository.findAll();
		Assert.assertNotNull(findAll);
	}

	@Test
	public void shouldConnectToPartyTypeRepository() {
		EndResult<PartyType> findAll = partyTypeRepository.findAll();
		Assert.assertNotNull(findAll);
	}

	@Test
	public void shouldConnectToPermissionsRepository() {
		EndResult<Permission> findAll =  permissionsRepository.findAll();
		Assert.assertNotNull(findAll);
	}

	@Test
	public void shouldConnectToResourceRepository() {
		EndResult<Resource> findAll = resourceRepository.findAll();
		Assert.assertNotNull(findAll);
	}

	@Test
	public void shouldConnectToRoleRepository() {
		EndResult<Role> findAll = roleRepository.findAll();
		Assert.assertNotNull(findAll);
	}

	@Test
	public void shouldConnetToEntity() {
		String entityType = "entityType" + UUID.randomUUID().toString();
		String uuid = "uuid";
		Entity entity = new Entity(new EntityType(entityType), uuid, null);
		entityRepository.save(entity);
		Entity result = entityRepository.findByEntityTypeNameAndUuid(entityType, uuid);
		Assert.assertNotNull(result);
		Assert.assertEquals(uuid, result.getUuid());
		Assert.assertEquals(entityType, result.getEntityType().getName());
	}

	@Test
	public void shouldConnetToEntityType() {
		String name = "entityType" + UUID.randomUUID().toString();
		EntityType entityType = new EntityType(name);
		entityTypeRepository.save(entityType);
		EntityType result = entityTypeRepository.findByName(name);
		Assert.assertNotNull(result);
		Assert.assertEquals(name, result.getName());
	}

}
