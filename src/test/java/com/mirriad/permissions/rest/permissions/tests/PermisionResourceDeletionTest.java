package com.mirriad.permissions.rest.permissions.tests;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.exceptions.PermissionNotFound;
import com.mirriad.permissions.exceptions.ResourceNotFound;
import com.mirriad.permissions.rest.PermissionResource;
import com.mirriad.permissions.services.api.PermissionsService;

@RunWith(MockitoJUnitRunner.class)
public class PermisionResourceDeletionTest {

	@InjectMocks
	private PermissionResource permissionResource;

	@Mock
	private PermissionsService permissionsService;


	private String resourceName = "resourceName";

	private String actionName = "actionName";

	@Test
	public void shouldDeleteAllPermissionForResource() throws ResourceNotFound {
		// Fixture
		// Expectations
		permissionResource.deleteResource(resourceName);
		// Expectations
		verify(permissionsService).deleteResource(resourceName);
	}

	@Test
	public void shouldHaveResourceNameForResource() throws ResourceNotFound {
		// Fixture
		// Expectations
		Response response = permissionResource.deleteResource(null);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldExistResource() throws ResourceNotFound {
		// Fixture
		doThrow(ResourceNotFound.class).when(permissionsService).deleteResource(resourceName);
		// Expectations
		Response response = permissionResource.deleteResource(resourceName);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldDeletePermission() throws PermissionNotFound {
		// Fixture
		// Expectations
		permissionResource.deletePermission(resourceName, actionName);
		// Expectations
		verify(permissionsService).deletePermission(resourceName, actionName);
	}

	@Test
	public void shouldHavePermissionResource() throws PermissionNotFound {
		// Fixture
		// Expectations
		Response response = permissionResource.deletePermission(null, actionName);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldHavePermissionAction() throws PermissionNotFound {
		// Fixture
		// Expectations
		Response response = permissionResource.deletePermission(resourceName, null);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldExistPermission() throws PermissionNotFound {
		// Fixture
		doThrow(PermissionNotFound.class).when(permissionsService).deletePermission(resourceName, actionName);
		// Expectations
		Response response = permissionResource.deletePermission(resourceName, actionName);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
}
