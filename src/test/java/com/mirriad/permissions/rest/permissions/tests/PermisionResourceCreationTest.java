package com.mirriad.permissions.rest.permissions.tests;

import static org.mockito.Mockito.verify;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.rest.PermissionResource;
import com.mirriad.permissions.rest.dto.PermissionDTO;
import com.mirriad.permissions.services.api.PermissionsService;

@RunWith(MockitoJUnitRunner.class)
public class PermisionResourceCreationTest {

	@InjectMocks
	private PermissionResource permissionResource;

	@Mock
	private PermissionsService permissionsService;


	private String resourceName = "resourceName";

	private String actionName = "actionName";

	private PermissionDTO permission = new PermissionDTO(resourceName, actionName);

	private PermissionDTO permissionNoResource = new PermissionDTO(null, actionName);

	private PermissionDTO permissionNoAction= new PermissionDTO(resourceName, null);

	@Test
	public void shouldCreatePermission() {
		// Fixture
		// Expectations
		permissionResource.createPermission(permission);
		// Expectations
		verify(permissionsService).createPermission(permission);
	}

	@Test
	public void shouldHaveResource() {
		// Fixture
		// Expectations
		Response response = permissionResource.createPermission(permissionNoResource);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldHaveAction() {
		// Fixture
		// Expectations
		Response response = permissionResource.createPermission(permissionNoAction);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
}
