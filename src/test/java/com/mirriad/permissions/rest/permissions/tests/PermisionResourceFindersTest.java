package com.mirriad.permissions.rest.permissions.tests;

import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.HashSet;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.constants.PageSizing;
import com.mirriad.permissions.rest.PermissionResource;
import com.mirriad.permissions.rest.dto.PermissionDTO;
import com.mirriad.permissions.services.api.PermissionsService;

@RunWith(MockitoJUnitRunner.class)
public class PermisionResourceFindersTest {

	@InjectMocks
	private PermissionResource permissionResource;

	@Mock
	private PermissionsService permissionsService;

	private PermissionDTO permission = new PermissionDTO("resource", "action");

	private String resourceName = "resourceName";

	private String actionName = "actionName";
	private Integer pageSIze = PageSizing.MAX_PAGE_SIZE;
	private Integer pageIndex = PageSizing.FIRST_PAGE;

	@Test
	public void shouldGetAllPermissions() {
		// Fixture
		Collection<PermissionDTO> allPermissions = new HashSet<PermissionDTO>();
		allPermissions.add(permission);
		when(permissionsService.getPermissions(pageSIze, pageIndex)).thenReturn(allPermissions);
		// Expectations
		Response response = permissionResource.getPermissions(pageSIze, pageIndex);
		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		@SuppressWarnings("unchecked")
		Collection<PermissionDTO> result = (Collection<PermissionDTO>) response.getEntity();
		Assert.assertEquals(allPermissions, result);
	}

	@Test
	public void shouldNotFoundIfNonePermission() {
		// Fixture
		when(permissionsService.getPermissions(pageSIze, pageIndex)).thenReturn(null);
		// Expectations
		Response response = permissionResource.getPermissions(1, 1);
		// Expectations
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldGetPermissionsByResource() {
		// Fixture
		Collection<PermissionDTO> allPermissions = new HashSet<PermissionDTO>();
		allPermissions.add(permission);
		when(permissionsService.getPermissions(resourceName,pageSIze, pageIndex)).thenReturn(allPermissions);
		// Expectations
		Response response = permissionResource.getPermissions(pageSIze, pageIndex, resourceName);
		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		@SuppressWarnings("unchecked")
		Collection<PermissionDTO> result = (Collection<PermissionDTO>) response.getEntity();
		Assert.assertEquals(allPermissions, result);
	}
	@Test
	public void shouldHaveResourceNameInPermissionsByResource() {
		// Expectations
		Response response = permissionResource.getPermissions(1, 1, null);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldNotFoundIfNonePermissionByResource() {
		// Fixture
		when(permissionsService.getPermissions(resourceName,pageSIze, pageIndex)).thenReturn(null);
		// Expectations
		Response response = permissionResource.getPermissions(pageSIze, pageIndex, resourceName);
		// Expectations
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldGetAPermission() {
		// Fixture
		when(permissionsService.getPermission(resourceName, actionName )).thenReturn(permission);
		// Expectations
		Response response = permissionResource.getPermissions(resourceName, actionName );
		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		PermissionDTO result = (PermissionDTO) response.getEntity();
		Assert.assertEquals(permission, result);
	}

	@Test
	public void shouldNotFoundWhenPermissionNotExists() {
		// Fixture
		when(permissionsService.getPermission(resourceName, actionName )).thenReturn(null);
		// Expectations
		Response response = permissionResource.getPermissions(resourceName, actionName );
		// Expectations
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
}
