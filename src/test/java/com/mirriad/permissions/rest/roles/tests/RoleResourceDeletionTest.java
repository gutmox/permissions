package com.mirriad.permissions.rest.roles.tests;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.RoleResource;
import com.mirriad.permissions.services.api.RolePartiesService;
import com.mirriad.permissions.services.api.RoleService;

@RunWith(MockitoJUnitRunner.class)
public class RoleResourceDeletionTest {

	@InjectMocks
	private RoleResource roleResource;

	@Mock
	private RoleService roleService;

	@Mock
	private RolePartiesService rolePartiesService;

	private String roleName = "roleName";

	@Test
	public void shouldDeleteRole() throws RoleNotFoundException {
		// Fixture
		// Experimentation
		roleResource.deleteRole(roleName);
		// Expectations
		verify(roleService).deleteRole(roleName);
	}

	@Test
	public void shouldExistsRole() throws RoleNotFoundException {
		// Fixture
		doThrow(RoleNotFoundException.class).when(roleService).deleteRole(roleName);
		// Experimentation
		Response response = roleResource.deleteRole(roleName);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldRoleNamePassed() {
		// Experimentation
		Response response = roleResource.deleteRole(null);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
}
