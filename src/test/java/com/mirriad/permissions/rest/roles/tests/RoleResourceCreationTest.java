package com.mirriad.permissions.rest.roles.tests;

import static org.mockito.Mockito.verify;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.rest.RoleResource;
import com.mirriad.permissions.rest.dto.RoleDTO;
import com.mirriad.permissions.services.api.RolePartiesService;
import com.mirriad.permissions.services.api.RoleService;

@RunWith(MockitoJUnitRunner.class)
public class RoleResourceCreationTest {

	@InjectMocks
	private RoleResource roleResource;

	@Mock
	private RoleService roleService;

	@Mock
	private RolePartiesService rolePartiesService;

	private String roleName = "roleName";
	private RoleDTO role = new RoleDTO(roleName );

	@Test
	public void shouldCreateRoles() {
	  // Fixture
		// Experimentation
		roleResource.createRole(role);
		// Expectations
		verify(roleService).createRoleIfNeccesary(role);
	}

	@Test
	public void shouldHaveName() {
	  // Fixture
		RoleDTO role = new RoleDTO();
		// Experimentation
		Response response = roleResource.createRole(role);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
}
