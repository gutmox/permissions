package com.mirriad.permissions.rest.roles.tests;

import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.HashSet;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.constants.PageSizing;
import com.mirriad.permissions.exceptions.PartyTypeNotFoundException;
import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.RoleResource;
import com.mirriad.permissions.rest.dto.PartyDTO;
import com.mirriad.permissions.rest.dto.RoleDTO;
import com.mirriad.permissions.services.api.RolePartiesService;
import com.mirriad.permissions.services.api.RoleService;

@RunWith(MockitoJUnitRunner.class)
public class RoleResourceFindersTest {

	@InjectMocks
	private RoleResource roleResource;

	@Mock
	private RoleService roleService;

	@Mock
	private RolePartiesService rolePartiesService;

	private Integer pageSize = PageSizing.MAX_PAGE_SIZE;
	private Integer pageIndex = PageSizing.FIRST_PAGE;

	private String roleName = "roleName";
	private RoleDTO role = new RoleDTO(roleName );

	private PartyDTO party = new PartyDTO("uuid", "name");

	private String partyType = "partyType";

	@Test
	public void shouldGetRoles() {
	  // Fixture
		Collection<RoleDTO> roles = new HashSet<RoleDTO>();
		roles.add(role );
		when(roleService.getRoles(null, null)).thenReturn(roles);
		// Experimentation
		Response response = roleResource.getRoles(null, null);
		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		@SuppressWarnings("unchecked")
		Collection<RoleDTO>  result = (Collection<RoleDTO>) response.getEntity();
		Assert.assertEquals(roles, result);
	}

	@Test
	public void shouldNotFoundWhenEmptyList() {
	  // Fixture
		Collection<RoleDTO> roles = new HashSet<RoleDTO>();
		when(roleService.getRoles(null, null)).thenReturn(roles);
		// Experimentation
		Response response = roleResource.getRoles(null, null);
		// Expectations
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldGetRole() {
	  // Fixture
		when(roleService.getRole(roleName)).thenReturn(role);
		// Experimentation
		Response response = roleResource.getRole(roleName);
		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		RoleDTO  result = (RoleDTO) response.getEntity();
		Assert.assertEquals(role, result);
	}

	@Test
	public void shouldBadRequestWhenRoleNull() {
		// Experimentation
		Response response = roleResource.getRole(null);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldNotFoundWhenNotRole() {
	  // Fixture
		when(roleService.getRole(roleName)).thenReturn(null);
		// Experimentation
		Response response = roleResource.getRole(roleName);
		// Expectations
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldGetPartiesForRoles() throws RoleNotFoundException {
	  // Fixture
		Collection<PartyDTO> parties = new HashSet<PartyDTO>();
		parties.add(party);
		when(rolePartiesService.getParties(roleName,pageSize, pageIndex)).thenReturn(parties);
		// Experimentation
		Response response = roleResource.getPartiesAssociated2Role(pageSize, pageIndex, roleName);
		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		@SuppressWarnings("unchecked")
		Collection<PartyDTO>  result = (Collection<PartyDTO>) response.getEntity();
		Assert.assertEquals(parties, result);
	}

	@Test
	public void shouldHaveRoleNamePartiesForRole() throws RoleNotFoundException {
		// Experimentation
		Response response = roleResource.getPartiesAssociated2Role(null, null, null);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	@SuppressWarnings("unchecked")
	public void shouldBadRequestWhenRoleNotFound() throws RoleNotFoundException {
	  // Fixture
			when(rolePartiesService.getParties(roleName,pageSize, pageIndex)).thenThrow(RoleNotFoundException.class);
			// Experimentation
			Response response = roleResource.getPartiesAssociated2Role(pageSize, pageIndex, roleName);
			// Expectations
			Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldGetPartiesForPartyTypeAndRole() throws RoleNotFoundException, PartyTypeNotFoundException {
	  // Fixture
		Collection<PartyDTO> parties = new HashSet<PartyDTO>();
		parties.add(party);
		when(rolePartiesService.getParties(roleName, partyType, pageSize, pageIndex)).thenReturn(parties);
		// Experimentation
		Response response = roleResource.getPartiesAssociated2Role(pageSize, pageIndex, roleName, partyType);
		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		@SuppressWarnings("unchecked")
		Collection<PartyDTO>  result = (Collection<PartyDTO>) response.getEntity();
		Assert.assertEquals(parties, result);
	}

	@Test
	public void shouldHaveRoleWhenGetPartiesForPartyTypeAndRole() throws RoleNotFoundException, PartyTypeNotFoundException {
	  // Fixture
		// Experimentation
		Response response = roleResource.getPartiesAssociated2Role(null, null, null, partyType);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldHavePartyTypeWhenGetPartiesForPartyTypeAndRole() throws RoleNotFoundException, PartyTypeNotFoundException {
	  // Fixture
		// Experimentation
		Response response = roleResource.getPartiesAssociated2Role(null, null, roleName, null);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldExistsPartyTypeWhenGetPartiesForPartyTypeAndRole() throws RoleNotFoundException, PartyTypeNotFoundException {
	  // Fixture
		when(rolePartiesService.getParties(roleName, partyType,pageSize, pageIndex)).thenThrow(PartyTypeNotFoundException.class);
		// Experimentation
		Response response = roleResource.getPartiesAssociated2Role(null, null, roleName, null);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldExistsRoleWhenGetPartiesForPartyTypeAndRole() throws RoleNotFoundException, PartyTypeNotFoundException {
	  // Fixture
		when(rolePartiesService.getParties(roleName, partyType, pageSize, pageIndex)).thenThrow(RoleNotFoundException.class);
		// Experimentation
		Response response = roleResource.getPartiesAssociated2Role(null, null, roleName, null);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
}
