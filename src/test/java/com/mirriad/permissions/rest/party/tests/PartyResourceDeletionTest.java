package com.mirriad.permissions.rest.party.tests;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.rest.PartyResource;
import com.mirriad.permissions.services.api.PartyService;

@RunWith(MockitoJUnitRunner.class)
public class PartyResourceDeletionTest {

	@InjectMocks
	private PartyResource partyResource;

	@Mock
	private PartyService partyService;

	private String partyTypeName = "partyTypeName";

	private String partyUuid = "partyUuid";

	@Test
	public void shouldGetAllPartyTypes() throws PartyNotFoundException {
		// Experimentation
		partyResource.deleteParty(partyTypeName, partyUuid);
		// Expectations
		verify(partyService).delete(partyTypeName, partyUuid);
	}

	@Test
	public void shouldBadResquestWhenNotPartyUuid() throws PartyNotFoundException {
		// Experimentation
		Response response = partyResource.deleteParty(partyTypeName, null);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldBadResquestWhenNotPartyNotFound() throws PartyNotFoundException {
		//Fixture
		doThrow(PartyNotFoundException.class).when(partyService).delete(partyTypeName, partyUuid);
		// Experimentation
		Response response = partyResource.deleteParty(partyTypeName, null);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
}
