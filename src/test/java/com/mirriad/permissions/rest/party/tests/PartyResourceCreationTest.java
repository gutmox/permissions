package com.mirriad.permissions.rest.party.tests;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.rest.PartyResource;
import com.mirriad.permissions.rest.dto.PartyDTO;
import com.mirriad.permissions.rest.dto.SimplePartyDTO;
import com.mirriad.permissions.services.api.PartyService;

@RunWith(MockitoJUnitRunner.class)
public class PartyResourceCreationTest {

	@InjectMocks
	private PartyResource partyResource;

	@Mock
	private PartyService partyService;

	private String partyName = "partyName";

	private String partyTypeName = "partyTypeName";

	private PartyDTO party = new PartyDTO("uuid", partyName);

	@Test
	public void shouldCreateParty() {
		String uuid = "uuid";
		// Fixture
		when(partyService.saveIfNeccesary(partyTypeName, party)).thenReturn(uuid);
		// Experimentation
		Response response = partyResource.createParty(partyTypeName, party);
		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		SimplePartyDTO result = (SimplePartyDTO) response.getEntity();
		Assert.assertEquals(uuid, result.getUuid());
	}

	@Test
	public void shouldUpdateParty() throws PartyNotFoundException {
		// Experimentation
		partyResource.updateParty(partyTypeName, party);
		// Expectations
		verify(partyService).update(partyTypeName, party);
	}

	@Test
	public void shouldBadRequestWhenNotPartyUuidResquest() {
		PartyDTO party = new PartyDTO();
		// Experimentation
		Response response = partyResource.updateParty(partyTypeName, party);
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldBadRequestWhenNotPartyNotFoundInUpdate() throws PartyNotFoundException {
		//Fixture
		doThrow(PartyNotFoundException.class).when(partyService).update(partyTypeName, party);
		// Experimentation
		Response response = partyResource.updateParty(partyTypeName, party);
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
}
