package com.mirriad.permissions.rest.party.tests;

import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.constants.PageSizing;
import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.rest.PartyResource;
import com.mirriad.permissions.rest.dto.PartyDTO;
import com.mirriad.permissions.rest.dto.PartyTypeDTO;
import com.mirriad.permissions.services.api.PartyService;

@RunWith(MockitoJUnitRunner.class)
public class PartyResourceFindersTest {

	@InjectMocks
	private PartyResource partyResource;

	@Mock
	private PartyService partyService;

	private String partyName = "partyName";

	private PartyTypeDTO partyType = new PartyTypeDTO(partyName);

	private String partyTypeName = "partyTypeName";

	private PartyDTO party = new PartyDTO("uuid", partyName);

	private String partyUuid = "partyUuid";
	private Integer pageSIze = PageSizing.MAX_PAGE_SIZE;
	private Integer pageIndex = PageSizing.FIRST_PAGE;

	@Test
	public void shouldGetAllPartyTypes() {
		// Fixture
		Set<PartyTypeDTO> parties = new HashSet<PartyTypeDTO>();
		parties.add(partyType);
		when(partyService.findAllPartyTypes(pageSIze, pageIndex)).thenReturn(parties);
		// Experimentation
		Response response = partyResource.getPartyTypes(pageSIze, pageIndex, null);

		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		@SuppressWarnings("unchecked")
		Set<PartyTypeDTO> result = (Set<PartyTypeDTO>) response.getEntity();
		Assert.assertEquals(parties, result);
	}

	@Test
	public void shouldGetPartyTypesByTypeQuery() {
		// Fixture
		Set<PartyTypeDTO> parties = new HashSet<PartyTypeDTO>();
		parties.add(partyType);
		when(partyService.findPartyTypeByName(partyTypeName)).thenReturn(parties);
		// Experimentation
		Response response = partyResource.getPartyTypes(null, null, partyTypeName);

		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		@SuppressWarnings("unchecked")
		Set<PartyTypeDTO> result = (Set<PartyTypeDTO>) response.getEntity();
		Assert.assertEquals(parties, result);
	}

	@Test
	public void shouldResponseNotFoundEmptyListForPartyTypes() {
		// Fixture
		Set<PartyTypeDTO> parties = new HashSet<PartyTypeDTO>();
		when(partyService.findPartyTypeByName(partyTypeName)).thenReturn(parties);
		// Experimentation
		Response response = partyResource.getPartyTypes(null, null, partyTypeName);
		// Expectations
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldGetAllPartiesByType() {
		// Fixture
		Collection<PartyDTO> parties = new HashSet<PartyDTO>();
		parties.add(party);
		when(partyService.findByPartyType(partyTypeName, pageSIze, pageIndex)).thenReturn(parties);
		// Experimentation
		Response response = partyResource.getPartiesByType(partyTypeName, pageSIze, pageIndex, null);
		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		@SuppressWarnings("unchecked")
		Collection<PartyDTO> result = (Collection<PartyDTO>) response.getEntity();
		Assert.assertEquals(parties, result);
	}

	@Test
	public void shouldNotFoundWhenEmptyListForPartType() {
		// Fixture
		Collection<PartyDTO> parties = new HashSet<PartyDTO>();
		when(partyService.findByPartyType(partyTypeName, pageSIze, pageIndex)).thenReturn(parties);
		// Experimentation
		Response response = partyResource.getPartiesByType(partyTypeName, null, null, null);
		// Expectations
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldGetAllPartiesByTypeAndName() {
		// Fixture
		Collection<PartyDTO> parties = new HashSet<PartyDTO>();
		parties.add(party);
		when(partyService.findByTypeAndName(partyTypeName, partyName)).thenReturn(parties);
		// Experimentation
		Response response = partyResource.getPartiesByType(partyTypeName, null, null,
				partyName);
		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		@SuppressWarnings("unchecked")
		Collection<PartyDTO> result = (Collection<PartyDTO>) response.getEntity();
		Assert.assertEquals(parties, result);
	}

	@Test
	public void shouldNotFoundWhenEmptyListForPartTypeAndName() {
		// Fixture
		Collection<PartyDTO> parties = new HashSet<PartyDTO>();
		when(partyService.findByTypeAndName(partyTypeName, partyName)).thenReturn(parties);
		// Experimentation
		Response response = partyResource.getPartiesByType(partyTypeName, null, null,
				partyName);
		// Expectations
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldGetPartyByUuid() throws PartyNotFoundException {
		when(partyService.findByUuid(partyTypeName, partyUuid)).thenReturn(party);
		// Experimentation
		Response response = partyResource.getParty(partyTypeName, partyUuid);
		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		PartyDTO result = (PartyDTO) response.getEntity();
		Assert.assertEquals(party, result);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void shouldNotFoundWhenPartyNotFound() throws PartyNotFoundException {
		when(partyService.findByUuid(partyTypeName, partyUuid)).thenThrow(PartyNotFoundException.class);
		// Experimentation
		Response response = partyResource.getParty(partyTypeName, partyUuid);
		// Expectations
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
}
