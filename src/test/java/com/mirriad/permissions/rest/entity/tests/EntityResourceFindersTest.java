package com.mirriad.permissions.rest.entity.tests;

import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.HashSet;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.constants.PageSizing;
import com.mirriad.permissions.exceptions.EntityNotFound;
import com.mirriad.permissions.exceptions.EntityTypeNotFound;
import com.mirriad.permissions.rest.EntityResource;
import com.mirriad.permissions.rest.dto.EntityTypeDTO;
import com.mirriad.permissions.rest.dto.SimpleEntityDTO;
import com.mirriad.permissions.services.api.EntityService;

@RunWith(MockitoJUnitRunner.class)
public class EntityResourceFindersTest {

	@InjectMocks
	private EntityResource entityResource;
	@Mock
	private EntityService entityService;

	private String entityType = "entityType";
	private String uuid = "uuid";
	private String name = "name";
	private EntityTypeDTO entityTypeDTO = new EntityTypeDTO(entityType);
	private Integer pageSize = PageSizing.MAX_PAGE_SIZE;
	private Integer pageIndex = PageSizing.FIRST_PAGE;
	private SimpleEntityDTO returnedEntity = new SimpleEntityDTO(uuid, name);

	@Test
	public void shouldListTypesWhenMoreThanOne() throws EntityTypeNotFound {
		// Fixture
		Collection<EntityTypeDTO> result = new HashSet<EntityTypeDTO>();
		result.add(entityTypeDTO);
		when(entityService.getAllEntityTypes(entityType, pageSize, pageIndex)).thenReturn(result);
		// Experimentation
		Response response = entityResource.getEntityTypes(pageSize, pageIndex, entityType);
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		Assert.assertEquals(result, response.getEntity());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldTypeNotFoundWhenZeroOccurs() throws EntityTypeNotFound {
		// Fixture
		when(entityService.getAllEntityTypes(entityType, pageSize, pageIndex)).thenThrow(EntityTypeNotFound.class);
		// Experimentation
		Response response = entityResource.getEntityTypes(pageSize, pageIndex, entityType);
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldListEntitiesWhenMoreThanOne() throws EntityNotFound {
		// Fixture
		Collection<SimpleEntityDTO> result = new HashSet<SimpleEntityDTO>();
		result.add(returnedEntity );
		when(entityService.getAllEntities(entityType, null, pageSize, pageIndex)).thenReturn(result);
		// Experimentation
		Response response = entityResource.getEntities(pageSize, pageIndex, entityType, null);
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		Assert.assertEquals(result, response.getEntity());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldEntitiesNotFoundWhenZeroOccurs() throws EntityNotFound {
		// Fixture
		when(entityService.getAllEntities(entityType, null, pageSize, pageIndex)).thenThrow(EntityNotFound.class);
		// Experimentation
		Response response = entityResource.getEntities(pageSize, pageIndex, entityType, null);
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldGetEntity() throws EntityNotFound {
		// Fixture
		when(entityService.getEntity(entityType, uuid)).thenReturn(returnedEntity);
		// Experimentation
		Response response = entityResource.getEntity(entityType, uuid);
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		Assert.assertEquals(returnedEntity, response.getEntity());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldNotFOundWhenNoEntity() throws EntityNotFound {
		// Fixture
		when(entityService.getEntity(entityType, uuid)).thenThrow(EntityNotFound.class);
		// Experimentation
		Response response = entityResource.getEntity(entityType, uuid);
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

}
