package com.mirriad.permissions.rest.entity.tests;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.exceptions.EntityNotFound;
import com.mirriad.permissions.exceptions.EntityTypeNotFound;
import com.mirriad.permissions.rest.EntityResource;
import com.mirriad.permissions.services.api.EntityService;

@RunWith(MockitoJUnitRunner.class)
public class EntityResourceDeletionTest {

	@InjectMocks
	private EntityResource entityResource;
	@Mock
	private EntityService entityService;

	private String entityType = "entityType";
	private String uuid = "uuid";

	@Test
	public void shouldDeleteEntityType() throws EntityTypeNotFound {
		// Experimentation
		Response response = entityResource.deleteEntityType(entityType);
		// Expectations
		verify(entityService).deleteEntityType(entityType);
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldTypeBePreviouslyStored() throws EntityTypeNotFound {
		// Fixture
		doThrow(EntityTypeNotFound.class).when(entityService).deleteEntityType(entityType);
		// Experimentation
		Response response = entityResource.deleteEntityType(entityType);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldDeleteEntity() throws EntityNotFound {
		// Experimentation
		Response response = entityResource.deleteEntity(entityType, uuid);
		// Expectations
		verify(entityService).deleteEntity(entityType, uuid);
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldHaveEntityType() throws EntityNotFound {
		// Experimentation
		Response response = entityResource.deleteEntity(null, uuid);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldHaveEntityUuid() throws EntityNotFound {
		// Experimentation
		Response response = entityResource.deleteEntity(entityType, null);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldEntityBePreviouslyStored() throws EntityNotFound {
		// Fixture
		doThrow(EntityNotFound.class).when(entityService).deleteEntity(entityType, uuid);
		// Experimentation
		Response response = entityResource.deleteEntity(entityType, uuid);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
}
