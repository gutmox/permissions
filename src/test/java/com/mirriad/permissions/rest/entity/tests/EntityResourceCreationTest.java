package com.mirriad.permissions.rest.entity.tests;

import static org.mockito.Mockito.when;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.rest.EntityResource;
import com.mirriad.permissions.rest.dto.EntityDTO;
import com.mirriad.permissions.rest.dto.EntityUuidDTO;
import com.mirriad.permissions.services.api.EntityService;

@RunWith(MockitoJUnitRunner.class)
public class EntityResourceCreationTest {

	@InjectMocks
	private EntityResource entityResource;
	@Mock
	private EntityService entityService;

	private String entityType = "entityType";
	private String uuid = "uuid";
	private String name = "name";
	private EntityDTO entity = new EntityDTO(uuid, name);

	@Test
	public void shouldCreateEntity() {
		// Fixture
		when(entityService.createEntity(entityType, entity)).thenReturn(uuid);
		// Experimentation
		Response response = entityResource.createEntity(entityType, entity);
		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		EntityUuidDTO result = (EntityUuidDTO) response.getEntity();
		Assert.assertEquals(uuid, result.getUuid());
	}

	@Test
	public void shouldBadRequestWhenNotType() {
		// Fixture
		when(entityService.createEntity(entityType, entity)).thenReturn(uuid);
		// Experimentation
		Response response = entityResource.createEntity(null, entity);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
}
