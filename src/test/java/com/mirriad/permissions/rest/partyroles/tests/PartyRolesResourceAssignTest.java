package com.mirriad.permissions.rest.partyroles.tests;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.PartyRolesResource;
import com.mirriad.permissions.services.api.PartyRolesService;
import com.mirriad.permissions.services.impl.PartyRolesServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class PartyRolesResourceAssignTest {

	@InjectMocks
	private PartyRolesResource partyRolesResource;

	@Mock
	private PartyRolesService partyRolesService = new PartyRolesServiceImpl();

	private String partyType = "partyType";
	private String partyUuid = "partyUuid";
	private String roleName = "roleName";

	@Test
	public void shouldDeassignRoles() throws PartyNotFoundException, RoleNotFoundException {
		//Fixture
		//Expectations
		partyRolesResource.assignRole(partyType, partyUuid, roleName);
		//Expectations
		verify(partyRolesService).assignRole2Party(partyType, partyUuid, roleName);
	}

	@Test
	public void shouldBadRequestWhenPartTypeNull() throws PartyNotFoundException, RoleNotFoundException {
		//Fixture
		//Expectations
		Response response = partyRolesResource.assignRole(null, partyUuid, roleName);
		//Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldBadRequestWhenPartUuidNull() throws PartyNotFoundException, RoleNotFoundException {
		//Fixture
		//Expectations
		Response response = partyRolesResource.assignRole(partyType, null, roleName);
		//Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldBadRequestWhenRoleNull() throws PartyNotFoundException, RoleNotFoundException {
		//Fixture
		//Expectations
		Response response = partyRolesResource.assignRole(partyType, partyUuid, null);
		//Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldBadRequestWhenPartyNotFound() throws PartyNotFoundException, RoleNotFoundException {
		//Fixture
		doThrow(PartyNotFoundException.class).when(partyRolesService).assignRole2Party(partyType, partyUuid, roleName);
		//Expectations
		Response response = partyRolesResource.assignRole(partyType, partyUuid, null);
		//Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldBadRequestWhenRoleNotFound() throws PartyNotFoundException, RoleNotFoundException {
		//Fixture
		doThrow(RoleNotFoundException.class).when(partyRolesService).assignRole2Party(partyType, partyUuid, roleName);
		//Expectations
		Response response = partyRolesResource.assignRole(partyType, partyUuid, null);
		//Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
}
