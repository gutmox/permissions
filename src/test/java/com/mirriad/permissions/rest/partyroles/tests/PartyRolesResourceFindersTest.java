package com.mirriad.permissions.rest.partyroles.tests;

import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.HashSet;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.PartyRolesResource;
import com.mirriad.permissions.rest.dto.RoleDTO;
import com.mirriad.permissions.services.api.PartyRolesService;
import com.mirriad.permissions.services.impl.PartyRolesServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class PartyRolesResourceFindersTest {

	@InjectMocks
	private PartyRolesResource partyRolesResource;

	@Mock
	private PartyRolesService partyRolesService = new PartyRolesServiceImpl();

	private String partyType = "partyType";
	private String partyUuid = "partyUuid";
	private String roleName = "roleName";
	private RoleDTO role = new RoleDTO(roleName);

	@Test
	public void shouldReturnRoles() throws PartyNotFoundException, RoleNotFoundException {
		//Fixture
		when(partyRolesService.getRole(partyType, partyUuid, roleName)).thenReturn(role);

		//Experimentation
		Response response = partyRolesResource.getRole(partyType, partyUuid, roleName);

		//Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		RoleDTO result = (RoleDTO) response.getEntity();
		Assert.assertEquals(role, result);
	}

	@Test
	public void shouldbadRequestWhenPartyTypeNull() throws PartyNotFoundException, RoleNotFoundException {
		//Experimentation
		Response response = partyRolesResource.getRole(null, partyUuid, roleName);
		//Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldbadRequestWhenPartyUuidNull() throws PartyNotFoundException, RoleNotFoundException {
		//Experimentation
		Response response = partyRolesResource.getRole(partyType, null, roleName);
		//Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldbadRequestWhenRoleNull() throws PartyNotFoundException, RoleNotFoundException {
		//Experimentation
		Response response = partyRolesResource.getRole(partyType, partyUuid, null);
		//Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldReturnBadRequestWhenRoleNotFound() throws RoleNotFoundException, PartyNotFoundException {
			//Fixture
			when(partyRolesService.getRole(partyType, partyUuid, roleName)).thenThrow(RoleNotFoundException.class);

			//Experimentation
			Response response = partyRolesResource.getRole(partyType, partyUuid, roleName);
			//Expectations
			Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldReturnBadRequestWhenPartyNotFound() throws RoleNotFoundException, PartyNotFoundException {
			//Fixture
			when(partyRolesService.getRole(partyType, partyUuid, roleName)).thenThrow(PartyNotFoundException.class);

			//Experimentation
			Response response = partyRolesResource.getRole(partyType, partyUuid, roleName);
			//Expectations
			Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldReturnRolesList() throws PartyNotFoundException {
  	//Fixture
		Collection<RoleDTO> roles = new HashSet<>();
		roles.add(role);
		when(partyRolesService.getRoles(partyType, partyUuid)).thenReturn(roles);

		//Experimentation
		Response response = partyRolesResource.getRoles(partyType, partyUuid);

	  //Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		Collection<RoleDTO> result = (Collection<RoleDTO>) response.getEntity();
		Assert.assertEquals(roles, result);
	}

	@Test
	public void shouldBadRequestWhenPartyTypeNull() throws PartyNotFoundException {
  	//Fixture
		//Experimentation
		Response response = partyRolesResource.getRoles(null, partyUuid);
	  //Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldBadRequestWhenPartyUuidNull() throws PartyNotFoundException {
  	//Fixture
		//Experimentation
		Response response = partyRolesResource.getRoles(partyType, null);
	  //Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldReturnBadRequestWhenPartyNotFoundInList() throws PartyNotFoundException{
 	  //Fixture
		Collection<RoleDTO> roles = new HashSet<>();
		roles.add(role);
		when(partyRolesService.getRoles(partyType, partyUuid)).thenThrow(PartyNotFoundException.class);
		//Experimentation
		Response response = partyRolesResource.getRoles(partyType, partyUuid);
		 //Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());

	}

	@Test
	public void shouldReturnNotFoundWhenListIsEmpty() throws PartyNotFoundException {
		Collection<RoleDTO> roles = new HashSet<>();
		//Fixture
		when(partyRolesService.getRoles(partyType, partyUuid)).thenReturn(roles);

		//Experimentation
		Response response = partyRolesResource.getRoles(partyType, partyUuid);
		//Expectations
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

}
