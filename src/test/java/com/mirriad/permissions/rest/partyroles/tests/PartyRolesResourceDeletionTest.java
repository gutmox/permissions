package com.mirriad.permissions.rest.partyroles.tests;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.PartyRolesResource;
import com.mirriad.permissions.services.api.PartyRolesService;
import com.mirriad.permissions.services.impl.PartyRolesServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class PartyRolesResourceDeletionTest {

	@InjectMocks
	private PartyRolesResource partyRolesResource;

	@Mock
	private PartyRolesService partyRolesService = new PartyRolesServiceImpl();

	private String partyType = "partyType";
	private String partyUuid = "partyUuid";
	private String roleName = "roleName";

	@Test
	public void shouldDeassignRoles() throws PartyNotFoundException {
		//Fixture
		//Experimentation
		partyRolesResource.deleteRoles(partyType, partyUuid);
		//Expectations
		verify(partyRolesService).deassignAllRole2Party(partyType, partyUuid);
	}

	@Test
	public void shouldReturnBadRequestWhenPartyNotExists() throws PartyNotFoundException{
		//	Fixture
		doThrow(PartyNotFoundException.class).when(partyRolesService).deassignAllRole2Party(partyType, partyUuid);

		//Experimentation
		Response response = partyRolesResource.deleteRoles(partyType, partyUuid);
		//Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldReturnBadRequestWhenPartyTypeNull() throws PartyNotFoundException{
		//	Fixture
		//Experimentation
		Response response = partyRolesResource.deleteRoles(null, partyUuid);
		//Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldReturnBadRequestWhenPartyUuidNull() throws PartyNotFoundException{
		//	Fixture
		//Experimentation
		Response response = partyRolesResource.deleteRoles(partyType, null);
		//Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}


	@Test
	public void shouldDeassignARole() throws PartyNotFoundException, RoleNotFoundException {
		//Experimentation
		partyRolesResource.deleteRole(partyType, partyUuid, roleName);
		//Expectations
		verify(partyRolesService).deassignRole2Party(partyType, partyUuid, roleName);
	}

	@Test
	public void shouldBadRequestForPartyNotFound() throws PartyNotFoundException, RoleNotFoundException {
		// Fixture
		doThrow(PartyNotFoundException.class).when(partyRolesService).deassignRole2Party(partyType, partyUuid, roleName);
		// Experimentation
		Response response = partyRolesResource.deleteRole(partyType, partyUuid, roleName);
	  // Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldBadRequestForRoleNotFound() throws PartyNotFoundException, RoleNotFoundException {
		// Fixture
		doThrow(RoleNotFoundException.class).when(partyRolesService).deassignRole2Party(partyType, partyUuid, roleName);
		// Experimentation
		Response response = partyRolesResource.deleteRole(partyType, partyUuid, roleName);
	  // Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

}
