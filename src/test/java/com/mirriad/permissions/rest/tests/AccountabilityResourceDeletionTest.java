package com.mirriad.permissions.rest.tests;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.rest.AccountabilityResource;
import com.mirriad.permissions.services.api.AccountabilityService;

@RunWith(MockitoJUnitRunner.class)
public class AccountabilityResourceDeletionTest {

	@InjectMocks
	private AccountabilityResource accountabilityResource;

	@Mock
	private AccountabilityService accountabilityService;

	private String partyType = "partyType";

	private String partyUuid = "partyUuid";

	private String accountabilityType = "accountabilityType";

	private String otherLegPartyType = "otherLegPartyType";

	private String otherLegPartyIdentifier = "otherLegPartyIdentifier";


	@Test
	public void shouldDeleteAllAccountabilitiesOfSomeType() throws PartyNotFoundException {
		//Fixture
		// Experimentations
		accountabilityResource.deleteAccountability(partyType, partyUuid, accountabilityType );
		// Expectations
		verify(accountabilityService).deleteAllAccountabilities(partyType, partyUuid, accountabilityType);
	}

	@Test
	public void shouldBadRequestWhenPartyNotExisting() throws PartyNotFoundException {
		//Fixture
		doThrow(PartyNotFoundException.class).when(accountabilityService).deleteAllAccountabilities(partyType, partyUuid, accountabilityType);
		// Experimentations
		Response response = accountabilityResource.deleteAccountability(partyType, partyUuid, accountabilityType );
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldDeleteAllAccountabilities() throws PartyNotFoundException {
		//Fixture
		// Experimentations
		accountabilityResource.deleteAllAccountabilities(partyType, partyUuid);
		// Expectations
		verify(accountabilityService).deleteAllAccountabilities(partyType, partyUuid);
	}

	@Test
	public void shouldBadRequestWhenPartyNotExistingInPartyType() throws PartyNotFoundException {
		//Fixture
		doThrow(PartyNotFoundException.class).when(accountabilityService).deleteAllAccountabilities(partyType, partyUuid);
		// Experimentations
		Response response = accountabilityResource.deleteAllAccountabilities(partyType, partyUuid);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldDeleteAccountability() throws PartyNotFoundException {
		//Fixture
		// Experimentations
		accountabilityResource.deleteAccountability(partyType, partyUuid, accountabilityType, otherLegPartyType, otherLegPartyIdentifier);
		// Expectations
		verify(accountabilityService).deleteAllAccountabilities(partyType, partyUuid, accountabilityType, otherLegPartyIdentifier);
	}

	@Test
	public void shouldBadRequestWhenPartyNotExistingInSpecificParty() throws PartyNotFoundException {
		//Fixture
		doThrow(PartyNotFoundException.class).when(accountabilityService).deleteAllAccountabilities(partyType, partyUuid, accountabilityType, otherLegPartyIdentifier);
		// Experimentations
		Response response = accountabilityResource.deleteAccountability(partyType, partyUuid, accountabilityType, otherLegPartyType, otherLegPartyIdentifier);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
}
