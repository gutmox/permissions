package com.mirriad.permissions.rest.tests;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.rest.AccountabilityResource;
import com.mirriad.permissions.rest.dto.AccountabilityDTO;
import com.mirriad.permissions.services.api.AccountabilityService;

@RunWith(MockitoJUnitRunner.class)
public class AccountabilityResourceCreationTest {

	@InjectMocks
	private AccountabilityResource accountabilityResource;

	@Mock
	private AccountabilityService accountabilityService;

	private String partyType = "partyType";

	private String partyUuid = "partyUuid";

	private AccountabilityDTO accountability = new AccountabilityDTO();

	@Test
	public void shouldCreateAccountability() throws PartyNotFoundException {
		//Fixture
		// Experimentations
		accountabilityResource.createAccountability(partyType, partyUuid, accountability);
		// Expectations
		verify(accountabilityService).create(partyType, partyUuid, accountability);
	}

	@Test
	public void shouldNotFoundWhenPartyNotStored() throws PartyNotFoundException {
		//Fixture
		doThrow(PartyNotFoundException.class).when(accountabilityService).create(partyType, partyUuid, accountability);
		// Experimentations
		Response response = accountabilityResource.createAccountability(partyType, partyUuid, accountability);
		// Expectations
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

}
