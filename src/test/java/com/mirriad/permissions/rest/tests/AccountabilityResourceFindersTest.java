package com.mirriad.permissions.rest.tests;

import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.rest.AccountabilityResource;
import com.mirriad.permissions.rest.dto.AccountabilityDTO;
import com.mirriad.permissions.services.api.AccountabilityService;

@RunWith(MockitoJUnitRunner.class)
public class AccountabilityResourceFindersTest {

	@InjectMocks
	private AccountabilityResource accountabilityResource;

	@Mock
	private AccountabilityService accountabilityService;

	private String partyType = "partyType";

	private String partyUuid = "partyUuid";

	private AccountabilityDTO accountability = new AccountabilityDTO();

	private String accountabilityType = "accountabilityType";

	private String otherLegPartyIdentifier = "otherLegPartyIdentifier";


	@Test
	public void shouldReturnAccountabilities() {
		//Fixture
		Set<AccountabilityDTO> accountabilities = new HashSet<AccountabilityDTO>();
		accountabilities.add(accountability);
		when(accountabilityService.findAccountabilities(partyType, partyUuid)).thenReturn(accountabilities);
		// Experimentations
		Response response = accountabilityResource.getAccountabilities(partyType, partyUuid);
		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		@SuppressWarnings("unchecked")
		Set<AccountabilityDTO> result = (Set<AccountabilityDTO>) response.getEntity();
		Assert.assertEquals(accountabilities, result);
	}

	@Test
	public void shouldNotFoundWhenEmptyList() {
		//Fixture
		Set<AccountabilityDTO> accountabilities = new HashSet<AccountabilityDTO>();
		when(accountabilityService.findAccountabilities(partyType, partyUuid)).thenReturn(accountabilities);
		// Experimentations
		Response response = accountabilityResource.getAccountabilities(partyType, partyUuid);
		// Expectations
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldReturnAccountabilitiesByType() {
		//Fixture
		Set<AccountabilityDTO> accountabilities = new HashSet<AccountabilityDTO>();
		accountabilities.add(accountability);
		when(accountabilityService.findAccountabilitiesByType(partyType, partyUuid, accountabilityType)).thenReturn(accountabilities);
		// Experimentations
		Response response = accountabilityResource.getAccountabilities(partyType, partyUuid, accountabilityType);
		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		@SuppressWarnings("unchecked")
		Set<AccountabilityDTO> result = (Set<AccountabilityDTO>) response.getEntity();
		Assert.assertEquals(accountabilities, result);
	}

	@Test
	public void shouldNotFoundWhenEmptyByType() {
		//Fixture
		Set<AccountabilityDTO> accountabilities = new HashSet<AccountabilityDTO>();
		when(accountabilityService.findAccountabilitiesByType(partyType, partyUuid, accountabilityType)).thenReturn(accountabilities);
		// Experimentations
		Response response = accountabilityResource.getAccountabilities(partyType, partyUuid, accountabilityType);
		// Expectations
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void shouldReturnSpecificAccountabilityWith2Legs() {
		//Fixture
		Set<AccountabilityDTO> accountabilities = new HashSet<AccountabilityDTO>();
		accountabilities.add(accountability);
		when(accountabilityService.findAccountabilitiesByTypeAndChildParty(partyType, partyUuid, accountabilityType, otherLegPartyIdentifier)).thenReturn(accountabilities);
		// Experimentations
		Response response = accountabilityResource.getAccountability(partyType, partyUuid, accountabilityType, otherLegPartyIdentifier);
		// Expectations
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());
		@SuppressWarnings("unchecked")
		Set<AccountabilityDTO> result = (Set<AccountabilityDTO>) response.getEntity();
		Assert.assertEquals(accountabilities, result);
	}


	@Test
	public void shouldNotFoundWhenNotExistingAccountability() {
		//Fixture
		when(accountabilityService.findAccountabilitiesByTypeAndChildParty(partyType, partyUuid, accountabilityType, otherLegPartyIdentifier)).thenReturn(null);
		// Experimentations
		Response response = accountabilityResource.getAccountability(partyType, partyUuid, accountabilityType, otherLegPartyIdentifier);
		// Expectations
		Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

}
