package com.mirriad.permissions.service.partyroles.tests;

import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.neo4j.conversion.EndResult;

import com.mirriad.permissions.dao.api.PartyRepository;
import com.mirriad.permissions.dao.api.PartyRolesRepository;
import com.mirriad.permissions.domain.Party;
import com.mirriad.permissions.domain.PartyRole;
import com.mirriad.permissions.domain.Role;
import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.dto.RoleDTO;
import com.mirriad.permissions.services.api.PartyRolesService;
import com.mirriad.permissions.services.impl.PartyRolesServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class PartyRolesServiceTest {

	@InjectMocks
	private PartyRolesService partyRolesService = new PartyRolesServiceImpl();
	private String partyType = "partyType";
	private String partyUuid = "partyUuid";
	@Mock
	private PartyRolesRepository partyRolesRepository;

	@Mock
	private PartyRepository partyRepository;
	@Mock
	private EndResult<PartyRole> resultParties;
	@Mock
	private Iterator<PartyRole> iterator;
	private String uuid = "uuid";
	private String name = "name";
	private Party party = new Party(uuid, name);
	private String roleName = "roleName";
	private Role role = new Role(roleName );

	private PartyRole partyRole = new PartyRole(party, role );

	@Test
	public void shouldGetPartyRoles() throws PartyNotFoundException {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, partyUuid)).thenReturn(party);
		when(partyRolesRepository.findByPartyUuid(partyUuid)).thenReturn(resultParties);
		when(resultParties.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(partyRole);
		// Experimentation
		Collection<RoleDTO> partyRoles = partyRolesService.getRoles(partyType, partyUuid);
		// Expectations
		Assert.assertEquals(1, partyRoles.size());
		Assert.assertEquals(roleName, partyRoles.iterator().next().getName());
	}

	@Test(expected=PartyNotFoundException.class)
	public void shouldNotFindParty() throws PartyNotFoundException {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, partyUuid)).thenReturn(null);
		when(partyRolesRepository.findByPartyUuid(partyUuid)).thenReturn(resultParties);
		when(resultParties.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(partyRole);
		// Experimentation
		Collection<RoleDTO> partyRoles = partyRolesService.getRoles(partyType, partyUuid);
		// Expectations
		Assert.assertEquals(1, partyRoles.size());
		Assert.assertEquals(roleName, partyRoles.iterator().next().getName());
	}

	@Test
	public void shouldGetPartyRole() throws PartyNotFoundException, RoleNotFoundException {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, partyUuid)).thenReturn(party);
		when(partyRolesRepository.findByPartyUuidAndRoleName(partyUuid, roleName)).thenReturn(partyRole);
		// Experimentation
		RoleDTO role2 = partyRolesService.getRole(partyType, partyUuid, roleName);
		// Expectations
		Assert.assertEquals(roleName, role2.getName());
	}

	@Test(expected=PartyNotFoundException.class)
	public void shouldGetPartyNotFound() throws PartyNotFoundException, RoleNotFoundException {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, partyUuid)).thenReturn(null);
		when(partyRolesRepository.findByPartyUuidAndRoleName(partyUuid, roleName)).thenReturn(partyRole);
		// Experimentation
		RoleDTO role2 = partyRolesService.getRole(partyType, partyUuid, roleName);
		// Expectations
		Assert.assertEquals(roleName, role2.getName());
	}
}
