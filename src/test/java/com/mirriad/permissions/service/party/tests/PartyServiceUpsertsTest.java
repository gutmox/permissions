package com.mirriad.permissions.service.party.tests;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.neo4j.conversion.EndResult;

import com.mirriad.permissions.dao.api.PartyRepository;
import com.mirriad.permissions.dao.api.PartyTypeRepository;
import com.mirriad.permissions.domain.Party;
import com.mirriad.permissions.domain.PartyType;
import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.rest.dto.PartyDTO;
import com.mirriad.permissions.services.api.PartyService;
import com.mirriad.permissions.services.impl.PartyServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class PartyServiceUpsertsTest {

	@InjectMocks
	private PartyService partyService = new PartyServiceImpl();

	@Mock
	private PartyRepository partyRepository;

	@Mock
	private PartyTypeRepository partyTypeRepository;

	@Mock
	private EndResult<Party> resultParties;

	@Mock
	private Iterator<Party> iterator;

	private String uuid = "uuid";

	private String name = "name";

	private String partyType = "partyType";

	private PartyType partyTypeDomain = new PartyType(partyType);

	private Party party = new Party(partyTypeDomain , uuid, name);

	private PartyDTO partyDTO = new PartyDTO(uuid, name);

	@Mock
	private EndResult<PartyType> resultPartyTypes;

	@Mock
	private Iterator<PartyType> iteratorPartyType;

	@Test
	public void shouldPartyAlreadyStored() {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, uuid)).thenReturn(party);
		// Experimentation
		String uuid = partyService.saveIfNeccesary(partyType, partyDTO);
		// Expectations
		Assert.assertEquals(this.uuid, uuid);
	}

	@Test
	public void shouldStoreParty() {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, uuid)).thenReturn(null);
		PartyDTO partyDTOWithoutUuid = new PartyDTO(uuid, name);
		// Experimentation
		String uuid = partyService.saveIfNeccesary(partyType, partyDTOWithoutUuid);
		// Expectations
		verify(partyRepository).save(party);
		Assert.assertEquals(this.uuid, uuid);
	}

	@Test
	public void shouldReturnUuidWhenAlreadyStored() {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, uuid)).thenReturn(party);
		PartyDTO partyDTOWithoutUuid = new PartyDTO(uuid, name);
		// Experimentation
		String uuid = partyService.saveIfNeccesary(partyType, partyDTOWithoutUuid);
		// Expectations
		Assert.assertEquals(this.uuid, uuid);
	}

	@Test
	public void shouldUpdateParty() throws PartyNotFoundException {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, uuid)).thenReturn(party);
		// Experimentation
		partyService.update(partyType, partyDTO);
		// Expectations
		verify(partyRepository).save(party);
	}

	@Test(expected=PartyNotFoundException.class)
	public void shouldNotUpdateNotExistingParty() throws PartyNotFoundException {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, uuid)).thenReturn(null);
		// Experimentation
		partyService.update(partyType, partyDTO);
		// Expectations
	}
}