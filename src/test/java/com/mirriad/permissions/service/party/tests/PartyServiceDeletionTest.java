package com.mirriad.permissions.service.party.tests;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Iterator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.neo4j.conversion.EndResult;

import com.mirriad.permissions.dao.api.PartyRepository;
import com.mirriad.permissions.dao.api.PartyTypeRepository;
import com.mirriad.permissions.domain.Party;
import com.mirriad.permissions.domain.PartyType;
import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.services.api.PartyService;
import com.mirriad.permissions.services.impl.PartyServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class PartyServiceDeletionTest {

	@InjectMocks
	private PartyService partyService = new PartyServiceImpl();

	@Mock
	private PartyRepository partyRepository;

	@Mock
	private PartyTypeRepository partyTypeRepository;

	@Mock
	private EndResult<Party> resultParties;

	@Mock
	private Iterator<Party> iterator;

	private String uuid = "uuid";

	private String name = "name";

	private String partyType = "partyType";

	private PartyType partyTypeDomain = new PartyType(partyType);

	private Party party = new Party(partyTypeDomain , uuid, name);

	@Mock
	private EndResult<PartyType> resultPartyTypes;

	@Mock
	private Iterator<PartyType> iteratorPartyType;

	@Test
	public void shouldDeleteParty() throws PartyNotFoundException {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, uuid)).thenReturn(party);
		// Experimentation
		partyService.delete(partyType, uuid);
		// Expectations
		verify(partyRepository).delete(party);
	}

	@Test(expected=PartyNotFoundException.class)
	public void shouldNotDeleteOnNotExistingParty() throws PartyNotFoundException {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, uuid)).thenReturn(null);
		// Experimentation
		partyService.delete(partyType, uuid);
		// Expectations
		verify(partyRepository).delete(party);
	}
}