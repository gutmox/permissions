package com.mirriad.permissions.service.party.tests;

import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.neo4j.conversion.EndResult;

import com.mirriad.permissions.constants.PageSizing;
import com.mirriad.permissions.dao.api.PartyRepository;
import com.mirriad.permissions.dao.api.PartyTypeRepository;
import com.mirriad.permissions.dao.paging.Paging;
import com.mirriad.permissions.domain.Party;
import com.mirriad.permissions.domain.PartyType;
import com.mirriad.permissions.exceptions.PartyNotFoundException;
import com.mirriad.permissions.rest.dto.PartyDTO;
import com.mirriad.permissions.rest.dto.PartyTypeDTO;
import com.mirriad.permissions.services.api.PartyService;
import com.mirriad.permissions.services.impl.PartyServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class PartyServiceFindersTest {

	@InjectMocks
	private PartyService partyService = new PartyServiceImpl();

	@Mock
	private PartyRepository partyRepository;

	@Mock
	private PartyTypeRepository partyTypeRepository;

	@Mock
	private EndResult<Party> resultParties;

	@Mock
	private Page<Party> resultPartiesPaging;

	@Mock
	private Iterator<Party> iterator;

	private String uuid = "uuid";

	private String name = "name";

	private String partyType = "partyType";

	private Party party = new Party(uuid, name);

	@Mock
	private EndResult<PartyType> resultPartyTypes;

	@Mock
	private Iterator<PartyType> iteratorPartyType;

	@Mock
	private Paging paging;

	private PartyType partyTypeDomain = new PartyType(partyType);

	@Test
	public void shouldFindAllParties() {
		// Fixture
		when(partyRepository.findAll()).thenReturn(resultParties);
		when(resultParties.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(party);
		// Experimentation
		Set<PartyDTO> allParties = partyService.findAll();
		// Expectations
		Assert.assertEquals(1, allParties.size());
		Assert.assertEquals(name, allParties.iterator().next().getName());
		Assert.assertEquals(uuid, allParties.iterator().next().getUuid());
	}

	@Test
	public void shouldNotFindParties() {
		// Fixture
		when(partyRepository.findAll()).thenReturn(resultParties);
		when(resultParties.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(false);
		when(iterator.next()).thenReturn(party);
		// Experimentation
		Set<PartyDTO> allParties = partyService.findAll();
		// Expectations
		Assert.assertEquals(0, allParties.size());
	}

	@Test
	public void shouldFindPartyByUuid() throws PartyNotFoundException {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, uuid)).thenReturn(party);
		when(resultParties.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(party);
		// Experimentation
		PartyDTO foundParty = partyService.findByUuid(partyType, uuid);
		// Expectations
		Assert.assertEquals(name, foundParty.getName());
		Assert.assertEquals(uuid, foundParty.getUuid());
	}

	@Test(expected = PartyNotFoundException.class)
	public void shouldNotFindPartyByUuid() throws PartyNotFoundException {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, uuid)).thenReturn(null);
		when(resultParties.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(party);
		// Experimentation
		partyService.findByUuid(partyType, uuid);
	}

	@Test
	public void shouldFindOneParty() throws PartyNotFoundException {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, uuid)).thenReturn(party);
		when(resultParties.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(party);
		// Experimentation
		Party findOne = partyService.findOne(partyType, uuid);
		// Expectations
		Assert.assertEquals(party, findOne);
	}

	@Test
	public void shouldNotFindOneParty() throws PartyNotFoundException {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, uuid)).thenReturn(null);
		when(resultParties.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(party);
		// Experimentation
		Party findOne = partyService.findOne(partyType, uuid);
		// Expectations
		Assert.assertEquals(null, findOne);
	}

	@Test
	public void shouldFindOneCheckedParty() throws PartyNotFoundException {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, uuid)).thenReturn(party);
		when(resultParties.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(party);
		// Experimentation
		Party findOne = partyService.findOneChecked(partyType, uuid);
		// Expectations
		Assert.assertEquals(party, findOne);
	}

	@Test(expected = PartyNotFoundException.class)
	public void shouldNotFindOneCheckedParty() throws PartyNotFoundException {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndUuid(partyType, uuid)).thenReturn(null);
		when(resultParties.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(party);
		// Experimentation
		partyService.findOneChecked(partyType, uuid);
		// Expectations
	}

	@Test
	public void shouldFindByTypeAndName() throws PartyNotFoundException {
		// Fixture
		when(partyRepository.findByPartyTypeNameAndName(partyType, name)).thenReturn(
				resultParties);
		when(resultParties.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(party);
		// Experimentation
		Collection<PartyDTO> resultParties = partyService.findByTypeAndName(partyType, name);
		// Expectations
		Assert.assertEquals(1, resultParties.size());
		Assert.assertEquals(name, resultParties.iterator().next().getName());
		Assert.assertEquals(uuid, resultParties.iterator().next().getUuid());
	}

	@Test
	public void shouldFindByType() throws PartyNotFoundException {
		// Fixture
		when(
				partyRepository.findByPartyTypeName(partyType,
						paging.paginateAndSort(PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE)))
				.thenReturn(resultPartiesPaging);
		when(resultPartiesPaging.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(party);
		// Experimentation
		Collection<PartyDTO> resultParties = partyService.findByPartyType(partyType,
				PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE);
		// Expectations
		Assert.assertEquals(1, resultParties.size());
		Assert.assertEquals(name, resultParties.iterator().next().getName());
		Assert.assertEquals(uuid, resultParties.iterator().next().getUuid());
	}

	@Test
	public void shouldFindAllPartyTypes() throws PartyNotFoundException {
		// Fixture
		when(partyTypeRepository.findAll()).thenReturn(resultPartyTypes);
		when(resultPartyTypes.iterator()).thenReturn(iteratorPartyType);
		when(iteratorPartyType.hasNext()).thenReturn(true).thenReturn(false);
		when(iteratorPartyType.next()).thenReturn(partyTypeDomain);
		// Experimentation
		Set<PartyTypeDTO> found = partyService.findAllPartyTypes(PageSizing.MAX_PAGE_SIZE,
				PageSizing.FIRST_PAGE);
		// Expectations
		Assert.assertEquals(1, found.size());
		Assert.assertEquals(partyType, found.iterator().next().getName());
	}

	@Test
	public void shouldFindPartyTypeByName() throws PartyNotFoundException {
		// Fixture
		when(partyTypeRepository.findAllByPropertyValue("name", partyType)).thenReturn(
				resultPartyTypes);
		when(resultPartyTypes.iterator()).thenReturn(iteratorPartyType);
		when(iteratorPartyType.hasNext()).thenReturn(true).thenReturn(false);
		when(iteratorPartyType.next()).thenReturn(partyTypeDomain);
		// Experimentation
		Set<PartyTypeDTO> found = partyService.findPartyTypeByName(partyType);
		// Expectations
		Assert.assertEquals(1, found.size());
		Assert.assertEquals(partyType, found.iterator().next().getName());
	}

}
