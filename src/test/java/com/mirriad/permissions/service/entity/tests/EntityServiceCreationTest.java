package com.mirriad.permissions.service.entity.tests;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirriad.permissions.dao.api.EntityRepository;
import com.mirriad.permissions.dao.api.EntityTypeRepository;
import com.mirriad.permissions.domain.Entity;
import com.mirriad.permissions.domain.EntityType;
import com.mirriad.permissions.rest.dto.EntityDTO;
import com.mirriad.permissions.services.api.EntityService;
import com.mirriad.permissions.services.impl.EntityServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class EntityServiceCreationTest {

	@InjectMocks
	private EntityService entityService = new EntityServiceImpl();
	@Mock
	private EntityRepository entityRepository;
	@Mock
	private EntityTypeRepository entityTypeRepository;

	private String entityType = "entityType";
	private String uuid = "uuid";
	private String name = "name";
	private EntityDTO entity = new EntityDTO(uuid, name);
	private EntityType entityTypeDomain = new EntityType(entityType);
	private Entity entityDomain = new Entity(entityTypeDomain, uuid, name);

	@Test
	public void shouldReturnSameUuidIfAleradyStored() {
		// Fixture
		entityDomain.setUuid(uuid);
		when(entityRepository.findByEntityTypeNameAndUuid(entityType, uuid)).thenReturn(
				entityDomain);
		// Experimentation
		String returnedUuid = entityService.createEntity(entityType, entity);
		// Expectations
		Assert.assertEquals(uuid, returnedUuid);
	}

	@Test
	public void shouldSaveIfNotExist() {
		// Fixture
		when(entityRepository.findByEntityTypeNameAndUuid(entityType, uuid)).thenReturn(
				null);
		when(entityTypeRepository.findByName(entityType)).thenReturn(entityTypeDomain);
		// Experimentation
		String returnedUuid = entityService.createEntity(entityType, entity);
		// Expectations
		verify(entityRepository).save(entityDomain);
		Assert.assertEquals(uuid, returnedUuid);
	}

	@Test
	public void shouldGenerateUuidWhenNotComing() {
		// Fixture
		when(entityRepository.findByEntityTypeNameAndUuid(entityType, uuid)).thenReturn(
				null);
		when(entityTypeRepository.findByName(entityType)).thenReturn(entityTypeDomain);
		EntityDTO entity = new EntityDTO(null, name);
		// Experimentation
		String returnedUuid = entityService.createEntity(entityType, entity);
		// Expectations
		Entity entityDomain = new Entity(entityTypeDomain, returnedUuid, name);
		verify(entityRepository).save(entityDomain);
		Assert.assertNotNull(returnedUuid);
	}

	@Test
	public void shouldCreateTypeWhenNotExisting() {
		// Fixture
		when(entityRepository.findByEntityTypeNameAndUuid(entityType, uuid)).thenReturn(
				null);
		when(entityTypeRepository.findByName(entityType)).thenReturn(null);
		// Experimentation
		String returnedUuid = entityService.createEntity(entityType, entity);
		// Expectations
		verify(entityTypeRepository).save(entityTypeDomain);
		verify(entityRepository).save(entityDomain);
		Assert.assertNotNull(returnedUuid);
	}

}
