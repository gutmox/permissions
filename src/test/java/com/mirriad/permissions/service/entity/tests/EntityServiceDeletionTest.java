package com.mirriad.permissions.service.entity.tests;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Iterator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.neo4j.conversion.EndResult;

import com.mirriad.permissions.dao.api.EntityRepository;
import com.mirriad.permissions.dao.api.EntityTypeRepository;
import com.mirriad.permissions.domain.Entity;
import com.mirriad.permissions.domain.EntityType;
import com.mirriad.permissions.exceptions.EntityNotFound;
import com.mirriad.permissions.exceptions.EntityTypeNotFound;
import com.mirriad.permissions.rest.dto.EntityDTO;
import com.mirriad.permissions.services.api.EntityService;
import com.mirriad.permissions.services.impl.EntityServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class EntityServiceDeletionTest {

	@InjectMocks
	private EntityService entityService = new EntityServiceImpl();
	@Mock
	private EntityRepository entityRepository;
	@Mock
	private EntityTypeRepository entityTypeRepository;
	@Mock
	private EndResult<Entity> endResult;
	@Mock
	private Iterator<Entity> iterator;

	private String entityType = "entityType";
	private String uuid = "uuid";
	private String name = "name";
	private EntityDTO entity = new EntityDTO(uuid, name);
	private EntityType entityTypeDomain = new EntityType(entityType);
	private Entity entityDomain = new Entity(entityTypeDomain, uuid, name);


	@Test
	public void shouldDeleteType() throws EntityTypeNotFound {
		// Fixture
		when(entityTypeRepository.findByName(entityType)).thenReturn(entityTypeDomain);
		when(entityRepository.findByEntityTypeName(entityType)).thenReturn(endResult);
		when(endResult.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(entityDomain);
		// Experimentation
		entityService.deleteEntityType(entityType);
		// Expectations
		verify(entityTypeRepository).delete(entityTypeDomain);
		verify(entityRepository).delete(entityDomain);
	}

	@Test(expected=EntityTypeNotFound.class)
	public void shouldExistEntityType() throws EntityTypeNotFound {
		// Fixture
		when(entityTypeRepository.findByName(entityType)).thenReturn(null);
		// Experimentation
		entityService.deleteEntityType(entityType);
		// Expectations
	}

	@Test
	public void shouldDeleteEntity() throws EntityNotFound {
		// Fixture
		when(entityRepository.findByEntityTypeNameAndUuid(entityType, uuid)).thenReturn(entityDomain);
		// Experimentation
		entityService.deleteEntity(entityType, uuid);
		// Expectations
		verify(entityRepository).delete(entityDomain);
	}

	@Test(expected=EntityNotFound.class)
	public void shouldExistsEntityPreviously() throws EntityNotFound {
		// Fixture
		when(entityRepository.findByEntityTypeNameAndUuid(entityType, uuid)).thenReturn(null);
		// Experimentation
		entityService.deleteEntity(entityType, uuid);
		// Expectations
	}
}
