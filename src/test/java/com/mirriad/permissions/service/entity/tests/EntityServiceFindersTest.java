package com.mirriad.permissions.service.entity.tests;

import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mirriad.permissions.dao.api.EntityRepository;
import com.mirriad.permissions.dao.api.EntityTypeRepository;
import com.mirriad.permissions.dao.paging.Paging;
import com.mirriad.permissions.domain.Entity;
import com.mirriad.permissions.domain.EntityType;
import com.mirriad.permissions.exceptions.EntityNotFound;
import com.mirriad.permissions.exceptions.EntityTypeNotFound;
import com.mirriad.permissions.rest.dto.EntityTypeDTO;
import com.mirriad.permissions.rest.dto.SimpleEntityDTO;
import com.mirriad.permissions.services.api.EntityService;
import com.mirriad.permissions.services.impl.EntityServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class EntityServiceFindersTest {

	@InjectMocks
	private EntityService entityService = new EntityServiceImpl();
	@Mock
	private EntityRepository entityRepository;
	@Mock
	private EntityTypeRepository entityTypeRepository;
	@Mock
	private Paging paging;
	@Mock
	private Page<Entity> result;
	@Mock
	private Page<EntityType> pageType;
	@Mock
	private Pageable pageable;
	@Mock
	private Iterator<Entity> iterator;
	@Mock
	private Iterator<EntityType> iteratorType;

	private String entityType = "entityType";
	private String uuid = "uuid";
	private String name = "name";
	private EntityType entityTypeDomain = new EntityType(entityType);
	private Entity entityDomain = new Entity(entityTypeDomain, uuid, name);
	private Integer pageSize = 1;
	private Integer pageIndex = 10;



	@Test
	public void shouldGetByType() throws EntityNotFound {
		// Fixture
		when(entityRepository.findByEntityTypeName(entityType, paging.paginateAndSort(pageSize, pageIndex))).thenReturn(result);
		when(result.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(entityDomain);
		// Experimentation
		Collection<SimpleEntityDTO> allEntities = entityService.getAllEntities(entityType, null, pageSize, pageIndex);
		// Expectations
		Assert.assertEquals(1, allEntities.size());
		Assert.assertEquals(uuid, allEntities.iterator().next().getUuid());
		Assert.assertEquals(name, allEntities.iterator().next().getName());
	}

	@Test
	public void shouldGetByTypeByName() throws EntityNotFound {
		// Fixture
		when(entityRepository.findByNameAndEntityTypeName(name, entityType,paging.paginateAndSort(pageSize, pageIndex))).thenReturn(result);
		when(result.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(entityDomain);
		// Experimentation
		Collection<SimpleEntityDTO> allEntities = entityService.getAllEntities(entityType, name, pageSize, pageIndex);
		// Expectations
		Assert.assertEquals(1, allEntities.size());
		Assert.assertEquals(uuid, allEntities.iterator().next().getUuid());
		Assert.assertEquals(name, allEntities.iterator().next().getName());
	}

	@Test(expected=EntityNotFound.class)
	public void shouldGetNotFoundWhenEmptyList() throws EntityNotFound {
		// Fixture
		when(entityRepository.findByNameAndEntityTypeName(name, entityType,paging.paginateAndSort(pageSize, pageIndex))).thenReturn(result);
		when(result.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(false);
		// Experimentation
		entityService.getAllEntities(entityType, name, pageSize, pageIndex);
		// Expectations
	}

	@Test
	public void shouldGetAllTypes() throws EntityTypeNotFound {
		// Fixture
		when(paging.paginateAndSort(pageSize, pageIndex)).thenReturn(pageable);
		when(entityTypeRepository.findAll(pageable)).thenReturn(pageType);
		when(pageType.iterator()).thenReturn(iteratorType);
		when(iteratorType.hasNext()).thenReturn(true).thenReturn(true).thenReturn(false);
		when(iteratorType.next()).thenReturn(entityTypeDomain);
		// Experimentation
		Collection<EntityTypeDTO> allEntityTypes = entityService.getAllEntityTypes(null, pageSize, pageIndex);
		// Expectations
		Assert.assertEquals(1, allEntityTypes.size());
		Assert.assertEquals(entityType, allEntityTypes.iterator().next().getName());
	}

	@Test
	public void shouldGetTypeByName() throws EntityTypeNotFound {
		// Fixture
		when(entityTypeRepository.findByName(entityType)).thenReturn(entityTypeDomain);
		// Experimentation
		Collection<EntityTypeDTO> allEntityTypes = entityService.getAllEntityTypes(entityType, pageSize, pageIndex);
		// Expectations
		Assert.assertEquals(1, allEntityTypes.size());
		Assert.assertEquals(entityType, allEntityTypes.iterator().next().getName());
	}

	@Test(expected=EntityTypeNotFound.class)
	public void shouldNotFoundTypesWhenNotOccurs() throws EntityTypeNotFound {
		// Fixture
		when(paging.paginateAndSort(pageSize, pageIndex)).thenReturn(pageable);
		when(entityTypeRepository.findAll(pageable)).thenReturn(pageType);
		when(pageType.iterator()).thenReturn(iteratorType);
		when(iteratorType.hasNext()).thenReturn(false);
		// Experimentation
		entityService.getAllEntityTypes(null, pageSize, pageIndex);
	}

	@Test(expected=EntityTypeNotFound.class)
	public void shouldNotFoundTypeByName() throws EntityTypeNotFound {
		// Fixture
		when(entityTypeRepository.findByName(entityType)).thenReturn(null);
		// Experimentation
		entityService.getAllEntityTypes(entityType, pageSize, pageIndex);
		// Expectations
	}
}
