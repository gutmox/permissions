package com.mirriad.permissions.service.rolesparty.tests;

import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.neo4j.conversion.EndResult;

import com.mirriad.permissions.constants.PageSizing;
import com.mirriad.permissions.dao.api.PartyRolesRepository;
import com.mirriad.permissions.dao.api.PartyTypeRepository;
import com.mirriad.permissions.dao.api.RoleRepository;
import com.mirriad.permissions.dao.paging.Paging;
import com.mirriad.permissions.domain.Party;
import com.mirriad.permissions.domain.PartyRole;
import com.mirriad.permissions.domain.PartyType;
import com.mirriad.permissions.domain.Role;
import com.mirriad.permissions.exceptions.PartyTypeNotFoundException;
import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.dto.PartyDTO;
import com.mirriad.permissions.services.api.RolePartiesService;
import com.mirriad.permissions.services.impl.RolePartiesServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class RolesPartyServiceTest {

	@Mock
	private PartyRolesRepository partyRolesRepository;

	@InjectMocks
	private RolePartiesService rolePartiesService = new RolePartiesServiceImpl();
	private String roleName = "roleName";
	@Mock
	private EndResult<PartyRole> resultRoles;

	@Mock
	private Page<PartyRole> resultRolesPaging;

	@Mock
	private Iterator<PartyRole> iterator;
	@Mock
	private RoleRepository roleRepository;
	@Mock
	private PartyTypeRepository partyTypeRepository;

	private String uuid = "uuid";

	private String name = "name";

	private Party party = new Party(uuid, name);

	private Role role = new Role();

	private PartyRole partyRole = new PartyRole(party, role);

	private String partyTypeName = "partyTypeName";

	private PartyType partyType = new PartyType();

	@Mock
	private Paging paging;

	@Test
	public void shouldGetPartyForRole() throws RoleNotFoundException {
		// Fixture
		when(partyRolesRepository.findByRoleName(roleName,
				paging.paginateAndSort(PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE))).thenReturn(resultRolesPaging);
		when(roleRepository.findByName(roleName)).thenReturn(role);
		when(resultRolesPaging.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(partyRole);
		// Experimentation
		Collection<PartyDTO> parties = rolePartiesService.getParties(roleName, PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE);
		// Expectations
		Assert.assertEquals(1, parties.size());
		Assert.assertEquals(name, parties.iterator().next().getName());
		Assert.assertEquals(uuid, parties.iterator().next().getUuid());
	}

	@Test(expected=RoleNotFoundException.class)
	public void shouldGetExceptionIfNotgetRole() throws RoleNotFoundException {
		// Fixture
		when(partyRolesRepository.findByRoleName(roleName,
				paging.paginateAndSort(PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE))).thenReturn(resultRolesPaging);
		when(roleRepository.findByName(roleName)).thenReturn(null);
		when(resultRolesPaging.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(partyRole);
		// Experimentation
		Collection<PartyDTO> parties = rolePartiesService.getParties(roleName, PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE);
		// Expectations
		Assert.assertEquals(1, parties.size());
		Assert.assertEquals(name, parties.iterator().next().getName());
		Assert.assertEquals(uuid, parties.iterator().next().getUuid());
	}

	@Test
	public void shouldGetPartyForRoleAndPartyType() throws RoleNotFoundException, PartyTypeNotFoundException {
		// Fixture
		when(partyRolesRepository.findByRoleNameAndPartyPartyTypeName(roleName, partyTypeName,
				paging.paginateAndSort(PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE))).thenReturn(resultRolesPaging);
		when(roleRepository.findByName(roleName)).thenReturn(role);
		when(partyTypeRepository.findByName(partyTypeName)).thenReturn(partyType );
		when(resultRolesPaging.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(partyRole);
		// Experimentation
		Collection<PartyDTO> parties = rolePartiesService.getParties(roleName, partyTypeName, PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE);
		// Expectations
		Assert.assertEquals(1, parties.size());
		Assert.assertEquals(name, parties.iterator().next().getName());
		Assert.assertEquals(uuid, parties.iterator().next().getUuid());
	}

	@Test(expected=RoleNotFoundException.class)
	public void shouldGetExceptionWhenRoleNotFound() throws RoleNotFoundException, PartyTypeNotFoundException {
		// Fixture
		when(partyRolesRepository.findByRoleNameAndPartyPartyTypeName(roleName, partyTypeName,
				paging.paginateAndSort(PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE))).thenReturn(resultRolesPaging);
		when(roleRepository.findByName(roleName)).thenReturn(null);
		when(partyTypeRepository.findByName(partyTypeName)).thenReturn(partyType );
		when(resultRolesPaging.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(partyRole);
		// Experimentation
		Collection<PartyDTO> parties = rolePartiesService.getParties(roleName, partyTypeName,PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE);
		// Expectations
		Assert.assertEquals(1, parties.size());
		Assert.assertEquals(name, parties.iterator().next().getName());
		Assert.assertEquals(uuid, parties.iterator().next().getUuid());
	}

	@Test(expected=PartyTypeNotFoundException.class)
	public void shouldGetExceptionWhenPartyTypeNotFound() throws RoleNotFoundException, PartyTypeNotFoundException {
		// Fixture
		when(partyRolesRepository.findByRoleNameAndPartyPartyTypeName(roleName, partyTypeName,
				paging.paginateAndSort(PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE))).thenReturn(resultRolesPaging);
		when(roleRepository.findByName(roleName)).thenReturn(role);
		when(partyTypeRepository.findByName(partyTypeName)).thenReturn(null);
		when(resultRolesPaging.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(partyRole);
		// Experimentation
		Collection<PartyDTO> parties = rolePartiesService.getParties(roleName, partyTypeName, PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE);
		// Expectations
		Assert.assertEquals(1, parties.size());
		Assert.assertEquals(name, parties.iterator().next().getName());
		Assert.assertEquals(uuid, parties.iterator().next().getUuid());
	}
}
