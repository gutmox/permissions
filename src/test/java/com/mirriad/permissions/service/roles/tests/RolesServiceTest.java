package com.mirriad.permissions.service.roles.tests;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.neo4j.conversion.EndResult;

import com.mirriad.permissions.constants.PageSizing;
import com.mirriad.permissions.dao.api.RoleRepository;
import com.mirriad.permissions.dao.paging.Paging;
import com.mirriad.permissions.domain.Role;
import com.mirriad.permissions.exceptions.RoleNotFoundException;
import com.mirriad.permissions.rest.dto.RoleDTO;
import com.mirriad.permissions.services.api.RoleService;
import com.mirriad.permissions.services.impl.RoleServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class RolesServiceTest {

	@InjectMocks
	private RoleService roleService = new RoleServiceImpl();

	@Mock
	private RoleRepository roleRepository;

	@Mock
	private EndResult<Role> resultRoles;

	@Mock
	private Page<Role> resultRolesPaging;

	@Mock
	private Iterator<Role> iterator;

	private String roleName = "roleName";

	private Role role = new Role(roleName);

	private String description = "description";

	@Mock
	private Paging paging;

	@Test
	public void shouldReturnAllRoles() {
		// Fixture
		when(roleRepository.findAll(
				paging.paginateAndSort(PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE))).thenReturn(resultRolesPaging);
		when(resultRolesPaging.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(role);
		// Experimentation
		Collection<RoleDTO> result = roleService.getRoles(PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE);
		// Expectations
		Assert.assertEquals(1, result.size());
		Assert.assertEquals(roleName, result.iterator().next().getName());
	}

	@Test
	public void shouldReturnEmptyList() {
		// Fixture
		when(roleRepository.findAll(
				paging.paginateAndSort(PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE))).thenReturn(resultRolesPaging);
		when(resultRolesPaging.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(false);
		// Experimentation
		Collection<RoleDTO> result = roleService.getRoles(PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE);
		// Expectations
		Assert.assertEquals(0, result.size());
	}

	@Test
	public void shouldReturnARole() {
		// Fixture
		role.setDescription(description);
		when(roleRepository.findByName(roleName)).thenReturn(role);
		// Experimentation
		RoleDTO roleDTO = roleService.getRole(roleName);
		// Expectations
		Assert.assertEquals(roleName, roleDTO.getName());
		Assert.assertEquals(description, roleDTO.getDescription());
	}

	@Test
	public void shouldReturnNull() {
		// Fixture
		when(roleRepository.findByName(roleName)).thenReturn(null);
		// Experimentation
		RoleDTO roleDTO = roleService.getRole(roleName);
		// Expectations
		Assert.assertEquals(null, roleDTO);
	}

	@Test
	public void shouldDeleteARole() throws RoleNotFoundException {
		// Fixture
		role.setDescription(description);
		when(roleRepository.findByName(roleName)).thenReturn(role);
		// Experimentation
		roleService.deleteRole(roleName);
		// Expectations
		verify(roleRepository).delete(role);
	}

	@Test(expected=RoleNotFoundException.class)
	public void shouldNotDeleteWithoutRole() throws RoleNotFoundException {
		// Fixture
		when(roleRepository.findByName(roleName)).thenReturn(null);
		// Experimentation
		roleService.deleteRole(roleName);
		// Expectations
	}

	@Test
	public void shouldCreate() throws RoleNotFoundException {
		// Fixture
		role.setDescription(description);
		when(roleRepository.findByName(roleName)).thenReturn(null);
		RoleDTO roleDTO = new RoleDTO(roleName, description);
		// Experimentation
		roleService.createRoleIfNeccesary(roleDTO);
		// Expectations
		verify(roleRepository).save(role);
	}
}
