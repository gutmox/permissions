package com.mirriad.permissions.service.permissions.tests;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.neo4j.conversion.EndResult;

import com.mirriad.permissions.constants.PageSizing;
import com.mirriad.permissions.dao.api.ActionRepository;
import com.mirriad.permissions.dao.api.PermissionsRepository;
import com.mirriad.permissions.dao.api.ResourceRepository;
import com.mirriad.permissions.dao.paging.Paging;
import com.mirriad.permissions.domain.Action;
import com.mirriad.permissions.domain.Permission;
import com.mirriad.permissions.domain.Resource;
import com.mirriad.permissions.exceptions.PermissionNotFound;
import com.mirriad.permissions.exceptions.ResourceNotFound;
import com.mirriad.permissions.rest.dto.PermissionDTO;
import com.mirriad.permissions.services.api.PermissionsService;
import com.mirriad.permissions.services.impl.PermissionsServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class PermissionServiceTest {

	@InjectMocks
	private PermissionsService permissionsService = new PermissionsServiceImpl();

	@Mock
	private PermissionsRepository permissionsRepository;

	@Mock
	private EndResult<Permission> resultPermissions;

	@Mock
	private Page<Permission> resultPermissionsPaging;

	@Mock
	private Iterator<Permission> iterator;
	private String resourceName = "resource";
	private String actionName = "action";
	private Resource resource = new Resource(resourceName);
	private Action action = new Action(actionName);
	private Permission permission = new Permission(resource, action);

	private PermissionDTO permissionDTO = new PermissionDTO(resourceName, actionName);

	@Mock
	private ResourceRepository resourceRepository;

	@Mock
	private ActionRepository actionRepository;

	@Mock
	private Paging paging;

	@Test
	public void shouldGetAllPermissions() {
		// Fixture
		when(permissionsRepository.findAllOrdered(paging.paginateAndSort(PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE, "resource.name"))).thenReturn(resultPermissionsPaging);
		when(resultPermissionsPaging.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(permission);
		// Experimentation
		Collection<PermissionDTO> permissions = permissionsService.getPermissions(PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE);
		// Expectations
		Assert.assertEquals(1, permissions.size());
		Assert.assertEquals(resourceName, permissions.iterator().next().getResource());
		Assert.assertEquals(actionName, permissions.iterator().next().getAction());
	}

	@Test
	public void shouldGetPermissionsByResourceName() {
		// Fixture
		when(permissionsRepository.findByResourceName(resourceName, paging.paginateAndSort(PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE))).thenReturn(resultPermissionsPaging);
		when(resultPermissionsPaging.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(permission);
		// Experimentation
		Collection<PermissionDTO> permissions = permissionsService.getPermissions(resourceName,PageSizing.MAX_PAGE_SIZE, PageSizing.FIRST_PAGE);
		// Expectations
		Assert.assertEquals(1, permissions.size());
		Assert.assertEquals(resourceName, permissions.iterator().next().getResource());
		Assert.assertEquals(actionName, permissions.iterator().next().getAction());
	}

	@Test
	public void shouldGetPermissionsByResourceAndActionName() {
		// Fixture
		when(permissionsRepository.findByResourceNameAndActionName(resourceName, actionName)).thenReturn(permission);
		when(resultPermissions.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(permission);
		// Experimentation
		PermissionDTO permission2 = permissionsService.getPermission(resourceName, actionName);
		// Expectations
		Assert.assertEquals(resourceName, permission2.getResource());
		Assert.assertEquals(actionName, permission2.getAction());
	}

	@Test
	public void shouldCreatePermission() {
		// Fixture
		when(permissionsRepository.findByResourceNameAndActionName(resourceName, actionName)).thenReturn(null);
		when(resourceRepository.findByName(resourceName)).thenReturn(resource);
		when(actionRepository.findByName(actionName)).thenReturn(action);
		when(resultPermissions.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(permission);
		// Experimentation
		permissionsService.createPermission(permissionDTO);
		// Expectations
		verify(permissionsRepository).save(permission);
	}

	@Test
	public void shouldDeleteResource() throws ResourceNotFound {
		// Fixture
		when(resourceRepository.findByName(resourceName)).thenReturn(resource);
		when(permissionsRepository.findByResourceName(resourceName)).thenReturn(resultPermissions);
		when(resultPermissions.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(permission);
		// Experimentation
		permissionsService.deleteResource(resourceName);
		// Expectations
		verify(permissionsRepository).delete(permission);
		verify(resourceRepository).delete(resource);
	}

	@Test(expected=ResourceNotFound.class)
	public void shouldThrowExceptionWhenResourceNotFound() throws ResourceNotFound {
		// Fixture
		when(resourceRepository.findByName(resourceName)).thenReturn(null);
		when(permissionsRepository.findByResourceName(resourceName)).thenReturn(resultPermissions);
		when(resultPermissions.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(permission);
		// Experimentation
		permissionsService.deleteResource(resourceName);
		// Expectations
		verify(permissionsRepository).delete(permission);
		verify(resourceRepository).delete(resource);
	}

	@Test
	public void shouldDeletePermission() throws PermissionNotFound {
		// Fixture
		when(permissionsRepository.findByResourceNameAndActionName(resourceName, actionName)).thenReturn(permission);
		// Experimentation
		permissionsService.deletePermission(resourceName, actionName);
		// Expectations
		verify(permissionsRepository).delete(permission);
	}

	@Test(expected=PermissionNotFound.class)
	public void shouldThrowExceptionWhenPermissionNotFound() throws PermissionNotFound {
		// Fixture
		when(permissionsRepository.findByResourceNameAndActionName(resourceName, actionName)).thenReturn(null);
		// Experimentation
		permissionsService.deletePermission(resourceName, actionName);
		// Expectations
		verify(permissionsRepository).delete(permission);
	}
}
