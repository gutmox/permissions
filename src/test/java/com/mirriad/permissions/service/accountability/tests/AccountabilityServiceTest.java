package com.mirriad.permissions.service.accountability.tests;

import static org.mockito.Mockito.when;

import java.util.Iterator;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.neo4j.conversion.EndResult;

import com.mirriad.permissions.dao.api.AccountabilityRepository;
import com.mirriad.permissions.domain.Accountability;
import com.mirriad.permissions.domain.Party;
import com.mirriad.permissions.domain.PartyType;
import com.mirriad.permissions.rest.dto.AccountabilityDTO;
import com.mirriad.permissions.services.api.AccountabilityService;
import com.mirriad.permissions.services.impl.AccountabilityServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class AccountabilityServiceTest {

	@InjectMocks
	private AccountabilityService accountabilityService = new AccountabilityServiceImpl();

	@Mock
	private AccountabilityRepository accountabilityRepository;

	@Mock
	private EndResult<Accountability> resultAccountability;

	@Mock
	private Iterator<Accountability> iteratorAccountability;

	private String partyType = "partyType";
	private String partyUuid = "partyUuid";
	private String accountabilityType = "accountabilityType";
	private Accountability accountability = new Accountability();
	private String uuid = "uuid";
	private String name = "name";
	private PartyType partyTypeDomain = new PartyType(partyType);
	Party childParty = new Party(partyTypeDomain, uuid, name);
	Party party = new Party(partyTypeDomain, uuid, name);

	@Test
	public void shouldFindAccountablitiesByPartyUuid() {
		// Fixture
		accountability.setAccountabilityType(accountabilityType);
		accountability.setParentParty(party);
		accountability.setChildParty(childParty);
		when(accountabilityRepository.getByTypeAndUuid(partyType, partyUuid)).thenReturn(
				resultAccountability);
		when(resultAccountability.iterator()).thenReturn(iteratorAccountability);
		when(iteratorAccountability.hasNext()).thenReturn(true).thenReturn(false);
		when(iteratorAccountability.next()).thenReturn(accountability);
		// Experimentation
		Set<AccountabilityDTO> found = accountabilityService.findAccountabilities(partyType,
				partyUuid);
		// Expectations
		Assert.assertEquals(1, found.size());
		Assert.assertEquals(accountabilityType, found.iterator().next()
				.getAccountabilityType());
	}

	@Test
	public void shouldFindAccountablitiesByType() {
		// Fixture
		accountability.setAccountabilityType(accountabilityType);
		accountability.setParentParty(party);
		accountability.setChildParty(childParty);
		when(
				accountabilityRepository.getByAccountabilityType(accountabilityType, partyType,
						partyUuid)).thenReturn(resultAccountability);
		when(resultAccountability.iterator()).thenReturn(iteratorAccountability);
		when(iteratorAccountability.hasNext()).thenReturn(true).thenReturn(false);
		when(iteratorAccountability.next()).thenReturn(accountability);
		// Experimentation
		Set<AccountabilityDTO> found = accountabilityService.findAccountabilitiesByType(
				partyType, partyUuid, accountabilityType);
		// Expectations
		Assert.assertEquals(1, found.size());
		Assert.assertEquals(accountabilityType, found.iterator().next()
				.getAccountabilityType());
	}

	@Test
	public void shouldFindAccountablitiesByTwoLegs() {
		// Fixture
		accountability.setAccountabilityType(accountabilityType);
		accountability.setParentParty(party);
		accountability.setChildParty(childParty);
		when(
				accountabilityRepository.getByAccountabilityType(accountabilityType, partyType,
						partyUuid, uuid)).thenReturn(resultAccountability);
		when(resultAccountability.iterator()).thenReturn(iteratorAccountability);
		when(iteratorAccountability.hasNext()).thenReturn(true).thenReturn(false);
		when(iteratorAccountability.next()).thenReturn(accountability);
		// Experimentation
		Set<AccountabilityDTO> found = accountabilityService
				.findAccountabilitiesByTypeAndChildParty(partyType, partyUuid,
						accountabilityType, uuid);
		// Expectations
		Assert.assertEquals(1, found.size());
		Assert.assertEquals(accountabilityType, found.iterator().next()
				.getAccountabilityType());
	}

}
